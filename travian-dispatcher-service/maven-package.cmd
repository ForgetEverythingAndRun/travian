@rem -T 1C = parallel building based on dependency graph tree where we use 1 thread per processor core
@rem -o = --offline,
@rem -Dmaven.test.skip = skipp all tests
@rem -am = incremental update of modules instead of clean
@rem -P %1 = building profile with name %1
@set defaultProfile=akacyka

@IF [%1] == [] (

mvn package -am -o -Dmaven.test.skip -T 1C

) else (

mvn package -am -o -Dmaven.test.skip -T 1C -P %1

)