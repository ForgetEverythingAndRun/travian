package by.travian.util;

import com.google.common.base.Charsets;
//import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;

//import static org.junit.Assert.assertEquals;

public class TestProxy {

   /* @Test
    public void testProxyConnection() throws IOException {
        final String URI = "https://api.myip.com/";//https://stackoverflow.com";
        assertEquals(HttpURLConnection.HTTP_OK, urlConnection(URI));
        setUpProxy();
        setDefaultAuthenticator();
        assertEquals(HttpURLConnection.HTTP_OK, urlConnection(URI));
    }*/

    public int urlConnection(String strUrl) throws IOException {
        URL url = new URL(strUrl);
        HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
        urlConn.connect();
        byte[] b = new byte[2048];
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataInputStream responseStream = null;
        responseStream = new DataInputStream((FilterInputStream) urlConn.getContent());
        int c = 0;
        while ((c = responseStream.read(b, 0, b.length)) != -1) bos.write(b, 0, c);
        String return_ = new String(bos.toByteArray(), Charsets.UTF_8);
        System.out.println(return_);
        //System.out.println(getClientIpAddress(urlConn));
        //urlConn.disconnect();
        return urlConn.getResponseCode();
    }

    public static void setUpProxy() {
        System.setProperty("jdk.http.auth.tunneling.disabledSchemes", "");//jdk8 is required
        System.setProperty("https.proxyHost", "server1-fr.freevpn.me");
        System.setProperty("https.proxyPort", "443");
        System.setProperty("https.proxyUser", "freevpn.me");
        System.setProperty("https.proxyPassword", "ZdgIRETPPjHA");//new=JyT8v9p6 old=ZdgIRETPPjHA


    }

    public void setDefaultAuthenticator() {
        final String proxyUser = System.getProperty("http.proxyUser");
        final String proxyPassword = System.getProperty("http.proxyPassword");
        if (proxyUser != null && proxyPassword != null) {
            Authenticator.setDefault(
                    new Authenticator() {
                        public PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(
                                    proxyUser, proxyPassword.toCharArray());
                        }
                    }
            );

        }
    }

    private static final String[] IP_HEADER_CANDIDATES = {
            "X-Forwarded-For",
            "Proxy-Client-IP",
            "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "REMOTE_ADDR"};

    public static String getClientIpAddress(HttpURLConnection urlConn) {
        for (String header : IP_HEADER_CANDIDATES) {
            String ip = urlConn.getHeaderField(header);
            if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
                return ip;
            }
        }
        return "NULL";
    }
}
