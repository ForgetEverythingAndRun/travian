package by.travian.constant;

import by.travian.bean.entity.building.Slot;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
public class TravianConstant {

    @Value("${host}")
    public void setHost(String host) {
        TravianConstant.Common.HOST = host;
    }

    public static class Common {
        public static String HOST = "tse3t3x.international.travian.com"; //as default
        public static final String HTTPS_SCHEME = "https";
        public static final String HTTPS_PORT = "443";
        public static final String BROWSER_USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0";

        public static final String VILLAGE_MAIN_PAGE_PATH = "/dorf1.php";
        public static final String VILLAGE_CENTER_PATH = "/dorf2.php";
        public static final String BUILDING_PATH = "/build.php";

        public static final String HERO_ADVENTURE_STATUS_PATH = "/hero/adventures"; //  /hero    /hero.php
        public static final String HERO_ADVENTURE_SEND_PATH = "/start_adventure.php";
        public static final String HERO_RESOURCE_PRODUCTION_PATH = "/api/v1/hero/attributes";

        public static final String MARKET_PATH = "/api/v1/marketplace/prepare";//"/api/v1/ajax/prepareMarketplace";

        public static final String TRAVIAN_PLUS_PATH = "api/v1/payment-wizard";//1) old "/api/v1/ajax/paymentWizard";2) new //api/v1/payment-wizard

        public static final String VILLAGE_ID_URL_PARAM = "newdid";
        public static final String SLOT_ID_URL_PARAM = "id";
        public static final String BUILDING_ID_URL_PARAM = "gid";
        public static final String GET_URL_PARAM_PATH_SEPARATOR = "?";
        public static final String GET_URL_PARAM_SEPARATOR = "&";
        public static final String GET_URL_PARAM_VALUE_SEPARATOR = "=";

        public final static String HOST_HEADER = "Host";
        public final static String REFERER_HEADER = "Referer";
        public final static String USER_AGENT_HEADER = "User-Agent";

        public static String LOGIN_FORM = "login";//"<form name=\"login\" method=\"POST\" action=\"login.php\">";
    }

    /*public static class Reports {
        public static final String REPORTS_URL = Common.HTTPS_SCHEME + Common.HOST + "/berichte.php";
        public static final String URL_PARAM_TAB_KEY = "t";
        public static final String URL_PARAM_TAB_ALL_VALUE = "0";
        public static final String URL_PARAM_PAGE = "page";

        public static final String GET_URL_PARAM_SEPARATOR = "&";
        public static final String GET_URL_PARAM_VALUE_SEPARATOR = "=";
    }*/

    public static class OffenseSend {
        public final static String TIMESTAMP_ATTACK_KEY = "timestamp";
        public final static String C_KEY = "c";
        public final static String X_KEY = "x";
        public final static String Y_KEY = "y";
        public final static String DNAME_KEY = "dname";
        public final static String TIMESTAMP_CHECKSUM_ATTACK_KEY = "checksum";//timestamp_checksum
        public final static String CURRENT_DID = "currentDid";
        public final static String B_KEY = "b";
        public final static String B_VALUE = "1";
        public final static String S1_KEY = "s1";
        public final static String S1_VALUE = "ok";
        public final static String EMPTY = "";
    }

    public static class OffenseApprove {
        public final static String REDEPLOY_HERO_KEY = "redeployHero";
        public final static String SEND_REALLY_KEY = "sendReally";
        public final static String SEND_REALLY_VALUE = "0"; //find more info about it
        public final static String TROOPS_SENT_KEY = "troopsSent";
        public final static String SPY_KEY = "troops[0][spy]";//"spy";
        public final static String TROOPS_SENT_VALUE = "1";
        public final static String SEND_TROOPS_BUILDING_ID_KEY = "id";
        //if village has another building id for send troop then it must be modified
        public final static String SEND_TROOPS_BUILDING_ID_VALUE = "39";
        public final static String B_KEY = "b";
        public final static String B_VALUE = "2";
        public final static String A_KEY = "a";
        public final static String KID_KEY = "kid";
        public final static String EMPTY = "";
    }

    public static class Login {
        //warning:  + "/dorf1.php"; is not working as login page any more! (required re-direct)
        public static final String LOGIN_PATH = "/login.php";
        public final static String NAME_KEY = "name";
        public final static String PASSWORD_KEY = "password";
        public final static String ENTER_BUTTON_KEY = "s1";
        //TODO: check encoding of it before use "Войти" (BE CARE!!!)
        public final static String ENTER_BUTTON_VALUE = "Войти";
        public final static String SCREEN_WIDTH_KEY = "w";
        public final static String SCREEN_WIDTH_VALUE = "1920:1080";
        public final static String TIMESTAMP_LOGIN_KEY = "login";
        public final static String TIMESTAMP_LOGIN_VALUE = (System.currentTimeMillis() / 1000) + "";
        public final static String LOW_RES_KEY = "lowRes";
        public final static String LOW_RES_VALUE = "0";
    }

    public static class Cookie {
        public static final String SET_COOKIE = "Set-Cookie";
        public static final String COOKIE_VALUE_DELIMITER = ";";
        public static final String PATH = "path";
        public static final String EXPIRES = "expires";
        public static final String DATE_FORMAT = "EEE, dd-MMM-yyyy hh:mm:ss z";
        public static final String SET_COOKIE_SEPARATOR = "; ";
        public static final String COOKIE = "Cookie";

        public static final char NAME_VALUE_SEPARATOR = '=';
        public static final char DOT = '.';
    }

    public static class BuildingConstant {
        public static final Slot WALL_SLOT = new Slot(40);
    }
}
