package by.travian.service;

import by.travian.bean.entity.OffenseEntity;
import by.travian.bean.task.Task;

import java.util.List;

public interface TravianOffenseService {
    void attack(OffenseEntity entity);

    void attack(List<OffenseEntity> list);

    void fastAttack(OffenseEntity entity);

    /**
     * sequence of attack (@currentDid should be the same for all tasks of the list)
     */
    void fastAttack(List<OffenseEntity> list);

}
