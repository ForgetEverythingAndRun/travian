package by.travian.service;

import by.travian.bean.Nation;
import by.travian.bean.container.VillageContainer;
import by.travian.bean.entity.OffenseEntity;
import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.LoginEntity;
import by.travian.bean.entity.MarketEntity;
import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.building.Level;
import by.travian.bean.entity.building.Slot;
import by.travian.bean.entity.village.Village;
import by.travian.bean.container.StaticResourceContainer;
import by.travian.bean.task.Task;
import by.travian.bean.task.handler.ContextHandler;
import by.travian.bean.troop.TroopAlias;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public interface TravianService {

    void updateVillages(String html);

    void notifyWhenEnoughResources(Village village, StaticResourceContainer actual);

    void trackResources(Village village, StaticResourceContainer resources);

    void loginTaskExecute(Task<LoginEntity> task);

    void trackTravianPlusStatus();

    Map<Integer, List<Level>> getBuildingLevelMap();

    void sleep(long timeout, TimeUnit unit);

    void sleepLogMessage(long timeout, TimeUnit unit);

    void sleepLogMessage(long timeout, TimeUnit unit, String message);

    ContextHandler getContextHandler();

    void login();

    void marketTasksExecute(List<Task<MarketEntity>> list);

    void buildingTasksExecute(List<Task<BuildingEntity>> list);


    void offenseTasksExecute(List<Task<OffenseEntity>> list);



    void autoSendHeroAdventure();

    VillageContainer getVillageContainerManager();

    void getVillages();

    boolean getVillageResourceBuildings(Village village);

    boolean getVillageCenterBuildings(Village village);

    boolean getVillageBuildingsResourceOrCenterByBuildingOwner(Village village, int buildingId);

    boolean getVillageResources(Village village);

    boolean getVillageTroops(Village village);

    boolean isBuildingQueueReady(Village village, boolean isResourceBuilding);

    Map.Entry<Slot, Building> centerBuildingUpgradeIfAvailable(Village village, Map.Entry<Slot, Building> entry);

    Map.Entry<Slot, Building> resourceBuildingUpgradeIfAvailable(Village village, Map.Entry<Slot, Building> entry);

    void resourceBuildingUpgradeAnyIfAvailable(Village village);

    void troopsSingleTypeAutoBuyMaxValue(TroopAlias.RequestTroopAlias alias, Nation nation, String villageId);

    void track(String villageId);

    void discordServiceStart();
}
