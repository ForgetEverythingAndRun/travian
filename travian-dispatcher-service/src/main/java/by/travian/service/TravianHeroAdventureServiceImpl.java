package by.travian.service;

import by.travian.bean.container.HeroAdventureContainer;
import by.travian.bean.entity.HeroAdventureEntity;
import by.travian.configuration.TravianConfiguration;
import by.travian.executor.HttpExecutor;
import by.travian.request.TravianHeroAdventureSendRequest;
import by.travian.request.TravianHeroAdventureStatusRequest;
import by.travian.util.ResponseParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class TravianHeroAdventureServiceImpl implements TravianHeroAdventureService {
    public final static Logger logger = LoggerFactory.getLogger(TravianHeroAdventureServiceImpl.class);

    private HttpExecutor httpExecutor;
    private ResponseParser responseParser;
    private TravianConfiguration travianConfiguration;
    private HeroAdventureContainer heroAdventureContainer;

    @Lazy
    @Autowired
    public TravianHeroAdventureServiceImpl(HttpExecutor httpExecutor, ResponseParser responseParser, TravianConfiguration travianConfiguration) {
        this.httpExecutor = httpExecutor;
        this.responseParser = responseParser;
        this.travianConfiguration = travianConfiguration;
    }

    @Override
    public void autoSendHeroAdventure() {
        TravianHeroAdventureStatusRequest heroAdventureStatusRequest = new TravianHeroAdventureStatusRequest();
        ResponseEntity<String> responseEntity = httpExecutor.execute(heroAdventureStatusRequest);
        String responseEntityBody = responseEntity.getBody();
        if (StringUtils.isBlank(responseEntityBody)) {
            logger.error("Can't find hero adventure list. Response is empty");
            return;
        }
        try {
            heroAdventureContainer = responseParser.getHeroAdventures(responseEntityBody);

            HeroAdventureContainer.HeroAdventureComparatorType type = travianConfiguration.getHeroAdventuresPriority();
            HeroAdventureContainer.HeroAdventureSortOrder order = travianConfiguration.getHeroAdventuresSortOrder();
            while (heroAdventureContainer.size() > 0) {
                HeroAdventureEntity heroAdventureEntity = heroAdventureContainer.getAdventuresSortedBy(type, order).get(0);
                if (heroAdventureContainer.isHeroAtHome()) {
                    logger.debug(String.format("Available adventures: %d", heroAdventureContainer.size()));
                    TravianHeroAdventureSendRequest heroAdventureSendRequest = new TravianHeroAdventureSendRequest(heroAdventureEntity);
                    responseEntity = httpExecutor.execute(heroAdventureSendRequest);
                    responseEntityBody = responseEntity.getBody();
                    if (StringUtils.isBlank(responseEntityBody)) {
                        logger.error("Can't update hero adventure status. Response is empty");
                        return;
                    }
                    logger.debug("Hero was sent to adventure.");
                    //heroStatus50 = go to, heroStatus9 = go back
                } else {

                    long wait = heroAdventureContainer.getWaitHeroTimestamp() - System.currentTimeMillis();
                    if (wait > 0) {
                        try {
                            Thread.sleep(wait);
                            responseEntity = httpExecutor.execute(heroAdventureStatusRequest);
                            responseEntityBody = responseEntity.getBody();
                            if (StringUtils.isBlank(responseEntityBody)) {
                                logger.error("Can't find hero adventure list. Response is empty");
                                return;
                            }
                            heroAdventureContainer = responseParser.getHeroAdventures(responseEntityBody);
                            logger.debug(String.format("Available adventures: %d", heroAdventureContainer.size()));
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                responseParser.updateHeroAdventureStatus(heroAdventureContainer, responseEntityBody);
            }
        } catch (Exception e) {
            logger.error("", e);
            return;
        }
    }

}
