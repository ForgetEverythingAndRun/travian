package by.travian.service;

import by.travian.bean.container.VillageContainer;
import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.MarketEntity;
import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.building.BuildingTabs;
import by.travian.bean.entity.building.Slot;
import by.travian.bean.entity.village.Village;
import by.travian.bean.container.VillageResourceContainer;
import by.travian.bean.task.Task;
import by.travian.bean.task.manager.MarketTaskManagerImpl;
import by.travian.executor.HttpExecutor;
import by.travian.request.*;
import by.travian.util.DefaultParamParser;
import by.travian.util.ResponseParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class TravianMarketServiceImpl implements TravianMarketService {
    public final static Logger logger = LoggerFactory.getLogger(TravianMarketService.class);
    public final static int MARKET_BUILDING_ID = 17;

    private HttpExecutor httpExecutor;
    private VillageContainer villageContainer;
    private ResponseParser responseParser;

    @Lazy
    @Autowired
    public TravianMarketServiceImpl(HttpExecutor httpExecutor, VillageContainer villageContainer, ResponseParser responseParser) {
        this.httpExecutor = httpExecutor;
        this.villageContainer = villageContainer;
        this.responseParser = responseParser;
    }

    @Override
    public boolean resourcesSend(MarketEntity entity) {
        Village village = villageContainer.getVillageById(entity.getVillageId());
        if (village == null) {
            logger.error("Can't find village with id = " + entity.getVillageId());
            return false;
        }
        Slot marketSlot = village.getBuildingSlotContainer().getFirstKeyByMatchedValue(Arrays.asList(Integer.toString(MARKET_BUILDING_ID)));
        if (marketSlot == null) {
            logger.info("Can't find market in village: " + village);
            return false;
        }
        entity.setSlotId(marketSlot.getId());

        BuildingEntity buildingEntity = new BuildingEntity(new Building(MARKET_BUILDING_ID), marketSlot, village.getVillageId());
        TravianBuildingRequest villageMarketRequest = new TravianBuildingRequest(buildingEntity, BuildingTabs.MarketTab.SEND);
        ResponseEntity<String> villageCenterResponse = httpExecutor.execute(villageMarketRequest);

        String villageMarket = villageCenterResponse.getBody();
        if (StringUtils.isBlank(villageMarket)) {
            return false;
        }

        VillageResourceContainer container = responseParser.getVillageResources(villageMarket);//debug here
        village.setResources(container);

        int availableMerchants = responseParser.getAvailableMerchants(villageMarket);
        int merchantCapacity = responseParser.getMerchantCapacity(villageMarket);
        MarketEntity sendEntity = updateMarketEntity(container, entity);

        String token = DefaultParamParser.findToken(villageMarket);
        TravianRequest villageRequest = new TravianPrepareMarketRequest(sendEntity, token);
        ResponseEntity<String> response = httpExecutor.execute(villageRequest);

        String responseBody = response.getBody();
        if (StringUtils.isBlank(responseBody)) {
            return false;
        }
        try {
            responseBody = new JSONObject(responseBody).get("formular").toString();
        } catch (JSONException e) {
            if (!isCarryCapacityEnough(sendEntity, availableMerchants, merchantCapacity)) {
                logger.debug("Skipped. No merchants available. Market task " + sendEntity);
            } else {
                logger.debug("Skipped. Cause of changing village context. Market task " + sendEntity);
            }
            return false;
        }

        String dname = DefaultParamParser.findInputWithNameAndId("dname", responseBody);
        String kid = DefaultParamParser.findInputWithNameAndId("kid", responseBody);
        String sz = DefaultParamParser.findInputWithNameAndId("sz", responseBody);
        String c = DefaultParamParser.findInputWithNameAndId("c", responseBody);
        sendEntity.setDname(dname);
        sendEntity.setKid(kid);
        sendEntity.setSz(sz);
        sendEntity.setC(c);

        TravianMarketRequest request = new TravianMarketRequest(sendEntity, token);

        response = httpExecutor.execute(request);
        responseBody = response.getBody();
        if (StringUtils.isBlank(responseBody)) {
            return false;
        }
        logger.info("Сompleted. Market task: " + entity);
        return true;
    }

    protected boolean isCarryCapacityEnough(MarketEntity marketEntity, int availableMerchants, int merchantCapacity) {
        int requiredCapacity = marketEntity.getWood() + marketEntity.getClay() + marketEntity.getIron() + marketEntity.getCrop();
        return merchantCapacity * availableMerchants >= requiredCapacity;
    }

    protected MarketEntity updateMarketEntity(VillageResourceContainer container, MarketEntity original) {
        //create new object otherwise it sends wrong amount of resources after not enough resources case
        MarketEntity entity = original.toBuilder().build();
        if (entity.getWood() > container.getWood().getAmount()) {
            entity.setWood(container.getWood().getAmount());
        }
        if (entity.getClay() > container.getClay().getAmount()) {
            entity.setClay(container.getClay().getAmount());
        }
        if (entity.getIron() > container.getIron().getAmount()) {
            entity.setIron(container.getIron().getAmount());
        }
        if (entity.getCrop() > container.getCrop().getAmount()) {
            entity.setCrop(container.getCrop().getAmount());
        }
        return entity;
    }

}
