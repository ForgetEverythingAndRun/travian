package by.travian.service;

import by.travian.bean.Nation;
import by.travian.bean.troop.TroopAlias;

@Deprecated
public interface TravianRecruitmentService {

    void troopsSingleTypeAutoBuyMaxValue(TroopAlias.RequestTroopAlias alias, Nation nation, String villageId);
}
