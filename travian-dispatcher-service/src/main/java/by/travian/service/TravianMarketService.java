package by.travian.service;

import by.travian.bean.entity.MarketEntity;
import by.travian.bean.task.Task;

import java.util.List;

public interface TravianMarketService {
    boolean resourcesSend(MarketEntity entity);
}
