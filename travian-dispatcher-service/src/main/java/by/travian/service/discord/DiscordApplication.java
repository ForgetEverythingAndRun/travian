package by.travian.service.discord;

import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.village.Village;
import by.travian.bean.movements.MoveEvent;
import by.travian.bean.container.MovementContainer;
import by.travian.configuration.BeanNameConst;
import by.travian.configuration.TravianConfiguration;
import by.travian.constant.TravianConstant;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.channel.Channel;
import org.javacord.api.entity.channel.ChannelType;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class DiscordApplication {
    public final Logger logger = LoggerFactory.getLogger(DiscordApplication.class);
    private final TravianConfiguration travianConfiguration;
    private final AtomicBoolean isShutdown;
    public final AtomicBoolean isPaused;
    private final BlockingQueue<EmbedBuilder> messageQueue;
    private RetryTemplate retryTemplate;

    @Lazy
    @Autowired
    public DiscordApplication(TravianConfiguration travianConfiguration, RetryTemplate retryTemplate) {
        this.travianConfiguration = travianConfiguration;
        this.isShutdown = travianConfiguration.isShutdown();
        this.isPaused = travianConfiguration.isPaused();
        this.retryTemplate = retryTemplate;
        this.messageQueue = new LinkedBlockingQueue<>(travianConfiguration.getTrackEnemyVillageIds().size() + 1);
    }

    public void push(MovementContainer movementContainer) {
        for (Map.Entry<String, Deque<MoveEvent>> entry : movementContainer.getMap().entrySet()) {
            String villageId = entry.getKey();
            Deque<MoveEvent> deque = entry.getValue();
            int total = deque.size();
            if (total > 0) {
                List<MoveEvent> events = Arrays.asList(deque.toArray(new MoveEvent[0]));

                int news = 0;
                List<MoveEvent> list = new ArrayList<>();
                for (MoveEvent event : events) {
                    if (event.isNew()) {
                        list.add(event);
                        news++;
                        event.setNew(false);
                    }
                }
                if (news > 0) {
                    Collections.sort(events, (o1, o2) -> {
                        return (int) (o1.getTimestamp() - o2.getTimestamp());
                    });
                    MoveEvent nearestEvent = events.get(0);
                    push(total, news, nearestEvent);
                }
            }
        }
    }

    protected void push(int total, int news, MoveEvent nearestEvent) {
        Thread thread = new Thread(() -> {
            try {
                messageQueue.put(interpretAsDiscordMessage(total, news, nearestEvent));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    public void start() {
        Thread thread = new Thread(run());
        thread.setDaemon(true);
        thread.start();
    }

    protected EmbedBuilder interpretAsDiscordMessage(int total, int news, MoveEvent event) {
        String accountName = "Account: __**" + travianConfiguration.getLogin() + "**__";
        Village villageUnderAttack = event.getVillage();
        String village = "**Village:** " + villageUnderAttack.getDname();
        String link = linkBuilder(villageUnderAttack.getLocation().getX(), villageUnderAttack.getLocation().getY()).build().toUriString();

        String totalWaves = "**Total waves:** " + total + " (+" + news + ")";
        //String forceWaves = "**Force waves:** " + 0;
        //String raidWaves = "**Raid waves:** " + 0;

        String nearestAttackArrivalTime = "**Nearest attack (local):** " + event.getDetails();
        String freeCrop = "**Free crop:** " + villageUnderAttack.getResources().getFreeCrop();
        String granaryCapacity = "**Granary:** " + villageUnderAttack.getResources().getCrop().getAmount() + "/" + villageUnderAttack.getResources().getGranaryCapacity();
        Building b = villageUnderAttack.getBuildingSlotContainer().get(TravianConstant.BuildingConstant.WALL_SLOT);
        Integer value = b != null ? b.getLevel().getNumber() : 0;
        String wall = "**Wall:** " + value + " lvl";

        StringBuilder sb = new StringBuilder();
        sb.append("\n\n").append(village).append("\n\n");
        sb.append(totalWaves).append("\n\n");
        sb.append(nearestAttackArrivalTime).append("\n\n");
        sb.append(granaryCapacity).append("\n\n");
        sb.append(freeCrop).append("\n\n");
        sb.append(wall).append("\n\n");

        return new EmbedBuilder().setColor(travianConfiguration.getDiscordChannelMessageColor())
                .setTitle(accountName).setDescription(link + sb.toString());
    }

    protected Runnable run() {
        return new Runnable() {
            @Override
            public void run() {
                String token = travianConfiguration.getDiscordToken();
                /*retryTemplate.execute(ctx -> {
                    return null;
                });*/

                DiscordApi api = new DiscordApiBuilder().setToken(token).login().join();
                logger.debug("You can invite the bot by using the following url: " + api.createBotInvite());
                api.addMessageCreateListener(event -> {
                    if (event.getMessageAuthor().isBotUser()) {
                        logger.debug("Ignore message of bot user.");
                        return;
                    }
                    if (event.getMessageContent().equalsIgnoreCase("!whoami")) {
                        event.getChannel().sendMessage("I'm " + travianConfiguration.getLogin() + " bot!");
                    } else if (event.getMessageContent().equalsIgnoreCase("!ping")) {
                        event.getChannel().sendMessage("pong!");
                    } else if (event.getMessageContent().equalsIgnoreCase("!commands")) {
                        event.getChannel().sendMessage("Available commands: !ping, !whoami, !pause [accountName]");
                    } else if (event.getMessageContent().startsWith("!pause")) {
                        String command = event.getMessageContent();
                        String[] commands = command.split(" ");
                        if (commands.length > 1) {
                            String accountName = commands[1];
                            if (!accountName.isBlank() && accountName.equalsIgnoreCase(travianConfiguration.getLogin())) {
                                if (isPaused.get()) {
                                    isPaused.set(false);
                                    event.getChannel().sendMessage("Bot(" + travianConfiguration.getLogin() + ") is working!");
                                    logger.debug("Bot(" + travianConfiguration.getLogin() + ") is working! Use !pause [accountName] to stop work.");
                                    synchronized (isPaused) {
                                        isPaused.notifyAll();
                                    }
                                } else {
                                    isPaused.set(true);
                                    event.getChannel().sendMessage("Bot(" + travianConfiguration.getLogin() + ") is stopped!");
                                    logger.debug("Bot(" + travianConfiguration.getLogin() + ") is stopped! Use !pause [accountName] once again to continue work.");
                                }
                            } else {
                                logger.debug("Account name '" + accountName + "' is invalid or empty. Please check and try again.");
                            }
                        } else {
                            event.getChannel().sendMessage("Pause on/off: !pause [AccountName]");
                        }
                    }
                });

                while (!isShutdown.get()) {
                    try {
                        EmbedBuilder embedBuilder = messageQueue.take();
                        for (Channel channel : api.getChannels()) {
                            if (channel.getType() == ChannelType.SERVER_TEXT_CHANNEL) {
                                ServerTextChannel textChannel = (ServerTextChannel) channel.asTextChannel().get();
                                if (textChannel.getName().equalsIgnoreCase(travianConfiguration.getDiscordTextChannelName())) {
                                    textChannel.sendMessage(embedBuilder);
                                }
                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    protected UriComponentsBuilder linkBuilder(int x, int y) {
        return UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path("/position_details.php")
                .queryParam("x", x)
                .queryParam("y", y);
    }

}
