package by.travian.service;

import by.travian.bean.container.VillageContainer;
import by.travian.bean.entity.OffenseEntity;
import by.travian.bean.troop.TroopAlias;
import by.travian.bean.troop.Troops;
import by.travian.error.SendTroopsException;
import by.travian.error.TravianException;
import by.travian.executor.HttpExecutor;
import by.travian.request.TravianApproveOffenseRequest;
import by.travian.request.TravianChooseOffenseRequest;
import by.travian.request.TravianPrepareOffenseRequest;
import by.travian.request.TravianRequest;
import by.travian.util.DefaultParamParser;
import by.travian.util.XPathHelper;
import com.google.common.base.CharMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import java.util.List;

import static by.travian.util.XPathHelper.compileXpathExpression;

@Service
public class TravianOffenseServiceImpl implements TravianOffenseService {
    public final static Logger logger = LoggerFactory.getLogger(TravianOffenseServiceImpl.class);

    private HttpExecutor httpExecutor;
    private VillageContainer villageContainer;


    //take source of html response in debug mode instead of copping it (Ctrl + U) from browser because there is a huge difference cause of js!
    //element <input> has a type and element <a> has a value in another case it's disable or empty
    private static final XPathExpression AVAILABLE_TROOPS_XPATH = compileXpathExpression("//table[@id='troops']//tr/td[input and a]");
    private static final XPathExpression TROOP_TYPE_XPATH = compileXpathExpression("input");//[starts-with(@name,'t')]
    private static final XPathExpression TROOP_VALUE_XPATH = compileXpathExpression("a");

    @Lazy
    @Autowired
    public TravianOffenseServiceImpl(HttpExecutor httpExecutor, VillageContainer villageContainer) {
        this.httpExecutor = httpExecutor;
        this.villageContainer = villageContainer;
    }

    @Override
    public void fastAttack(List<OffenseEntity> list) {
        for (OffenseEntity entity : list) {
            try {
                prepareAttack(entity);
                chooseAttack(entity);
            } catch (TravianException e) {
                logger.error(entity.toString(), e);
            }
        }
        //Prepare entities and sent after that. Required to decrease delay between waves.
        for (OffenseEntity entity : list) {
            try {
                approveAttack(entity);
                logger.debug(String.format("Task completed: %s", entity.toString()));
            } catch (TravianException e) {
                logger.error(String.format("Failed task: %s", entity.toString()), e);
            }
        }
    }

    @Override
    public void fastAttack(OffenseEntity entity) {
        try {
            prepareAttack(entity);
            chooseAttack(entity);
            approveAttack(entity);
            logger.debug(String.format("Sent: %s", entity.toString()));
        } catch (TravianException e) {
            logger.error(String.format("Failed task: %s", entity.toString()), e);
        }
    }

    @Override
    public void attack(OffenseEntity entity) {
        try {
            String htmlResponse = prepareAttack(entity);

            Troops villageTroops = villageContainer.getVillageById(entity.getVillageId()).getTroops();
            villageTroops.setTroops(availableTroops(htmlResponse).getTroops());
            if (!villageTroops.hasEnoughTroops(entity.getTroopsContainer())) {
                return;
            }
            htmlResponse = chooseAttack(entity);
            htmlResponse = approveAttack(entity);
        } catch (TravianException e) {
            logger.error(String.format("Failed task: %s", entity.toString()), e);
        }

    }

    @Override
    public void attack(List<OffenseEntity> list) {
        for (OffenseEntity entity : list) {
            try {
                String htmlResponse = prepareAttack(entity);
                Troops villageTroops = villageContainer.getVillageById(entity.getVillageId()).getTroops();
                villageTroops.setTroops(availableTroops(htmlResponse).getTroops());
                if (!villageTroops.hasEnoughTroops(entity.getTroopsContainer())) {
                    continue;
                }
                chooseAttack(entity);
                approveAttack(entity);
            } catch (TravianException e) {
                logger.error(String.format("Failed task: %s", entity.toString()), e);
            }
        }
    }

    protected String prepareAttack(OffenseEntity entity) {
        TravianRequest attack = new TravianPrepareOffenseRequest(entity);

        ResponseEntity<String> response = httpExecutor.execute(attack);
        String htmlbody = response.getBody();

        String timestamp = DefaultParamParser.findTimestamp(htmlbody);
        String checksum = DefaultParamParser.findChecksum(htmlbody);

        entity.setTimestamp(timestamp);
        entity.setChecksum(checksum);
        return htmlbody;
    }

    protected String chooseAttack(OffenseEntity entity) {
        TravianRequest attack = new TravianChooseOffenseRequest(entity);
        ResponseEntity<String> response = httpExecutor.execute(attack);
        String htmlbody = response.getBody();

        String a = DefaultParamParser.findA(htmlbody);
        String kid = DefaultParamParser.findKid(htmlbody);

        entity.setA(a);
        entity.setKid(kid);
        return htmlbody;
    }

    protected String approveAttack(OffenseEntity entity) {
        TravianRequest attack = new TravianApproveOffenseRequest(entity);
        ResponseEntity<String> response = httpExecutor.execute(attack);
        return response.getBody();
    }

    private static Troops availableTroops(String responseHtml) {
        Troops availableTroops = new Troops();
        Document doc = XPathHelper.responseAsDocument(responseHtml);
        try {
            NodeList troops = (NodeList) AVAILABLE_TROOPS_XPATH.evaluate(doc, XPathConstants.NODESET);
            logger.info(String.format("Available troop types: %d", troops.getLength()));
            for (int i = 0; i < troops.getLength(); i++) {
                Node troop = troops.item(i);
                Node troopItemType = (Node) TROOP_TYPE_XPATH.evaluate(troop, XPathConstants.NODE);
                Node troopItemValue = (Node) TROOP_VALUE_XPATH.evaluate(troop, XPathConstants.NODE);
                try {
                    String type = CharMatcher.INVISIBLE.removeFrom(troopItemType.getAttributes().getNamedItem("name").getTextContent());
                    // Added wrapper
                    type = type.replace("troops[0][", "").replace("]", "");
                    int value = Integer.parseInt(CharMatcher.INVISIBLE.removeFrom(troopItemValue.getTextContent()));
                    availableTroops.getTroops().put(TroopAlias.RequestTroopAlias.createAliasByString(type), value);
                    logger.info(String.format("Update troop:  %s -> %s", type, value));
                } catch (NumberFormatException e) {
                    logger.error("", e);
                    throw new SendTroopsException(SendTroopsException.TROOPS_PARSE_ERROR);
                }
            }
        } catch (XPathExpressionException e) {
            logger.error("", e);
        }
        return availableTroops;
    }

}
