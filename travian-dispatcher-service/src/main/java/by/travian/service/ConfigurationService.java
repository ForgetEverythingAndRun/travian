package by.travian.service;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConfigurationService {

    private static String PROPERTY_DELIMITER = ".";
    private static long PROPERTY_REFRESH_DELAY = 1000;//default 5000 ms
    private Map<String, Map<String,String>> resourceProps = new ConcurrentHashMap<>();

    //@ConditionalOnResource
    //matchIfMissing - specify if the condition should match if the property is not set.
    // –spring.config.location=file://{path to file}.
    @Bean
    @ConditionalOnProperty(name = "spring.config.location", matchIfMissing = false)
    public PropertiesConfiguration propertiesConfiguration(@Value("${spring.config.location}") String path) throws Exception {
        String filePath = new File(path.substring("file:".length())).getCanonicalPath();
        PropertiesConfiguration configuration = new PropertiesConfiguration(
                new File(filePath));
        FileChangedReloadingStrategy fcrs = new FileChangedReloadingStrategy();
        fcrs.setRefreshDelay(PROPERTY_REFRESH_DELAY);
        configuration.setReloadingStrategy(fcrs);
        return configuration;
    }


    /*
    	@Bean
    public ConfigurationService configurationService() {
    	CfgFactory.init("/als_config.xml");
    	return CfgFactory.getConfiguration();
    }



    private String getProperty(String key) {
        if(cfgService.contains(key)) {
            return cfgService.getString(key);
        }

        return null;
    }
     */
}
