package by.travian.service;

import by.travian.bean.Nation;
import by.travian.bean.container.VillageContainer;
import by.travian.bean.entity.village.Village;
import by.travian.bean.task.BuildingTask;
import by.travian.bean.task.handler.ContextHandler;
import by.travian.bean.troop.TroopAlias;
import by.travian.configuration.BeanNameConst;
import by.travian.configuration.TravianConfiguration;
import by.travian.util.FileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Service
public class TravianServiceFacade {
    private static final Logger logger = LoggerFactory.getLogger(TravianServiceFacade.class);

    private ContextHandler contextHandler;
    private TravianService travianService;
    private VillageContainer villageContainer;
    private AtomicBoolean isShutdown;
    private TravianConfiguration travianConfiguration;

    @Autowired
    public TravianServiceFacade(ContextHandler contextHandler,TravianService travianService, VillageContainer villageContainer, @Qualifier(BeanNameConst.IS_SHUTDOWN) AtomicBoolean isShutdown, TravianConfiguration travianConfiguration) {
        this.contextHandler = contextHandler;
        this.travianService = travianService;
        this.villageContainer = villageContainer;
        this.isShutdown = isShutdown;
        this.travianConfiguration = travianConfiguration;
    }

    public void execute(ApplicationArguments args) {
        logger.info("Your application started with option names : {}", args.getOptionNames());
        travianService.login();
        travianService.getVillages();

        contextHandler.handle();

        travianService.trackTravianPlusStatus();
        villageContainer.getVillages().forEach(village -> {
            travianService.getVillageResourceBuildings(village);
            travianService.getVillageCenterBuildings(village);
        });
        if (travianConfiguration.isHeroAdventureEnabled()) {
            travianService.autoSendHeroAdventure();
        }
        if (travianConfiguration.isMarketTasksEnabled()) {
            travianService.marketTasksExecute(new ArrayList<>(FileReader.readMarketTasks()));
        }
        if (travianConfiguration.isRaidEnabled()) {
            travianService.offenseTasksExecute(new ArrayList<>(FileReader.read(FileReader.RAID_TARGET_FILE, travianConfiguration)));
        }
        if (travianConfiguration.isBuildingTasksEnabled()) {
            travianService.buildingTasksExecute(new ArrayList<>(travianConfiguration.createBuildingTasks()));
        }
        if (travianConfiguration.isTrackEnemyEnabled()) {
            travianService.discordServiceStart();
        }
        while (!isShutdown.get()) {
            if (travianConfiguration.isAutoBuildingEnabled()) {
                autoBuilding();
            }
            if (travianConfiguration.isMilitaryBuyModeEnabled()) {
                recruitment();
            }
            if (travianConfiguration.isTrackEnemyEnabled()) {
                trackEnemy();
            }
            travianService.sleep(travianConfiguration.getLoopTimeout(), travianConfiguration.getLoopTimeUnit());
        }
        logger.info("Application is shutting down");
    }

    protected void trackEnemy() {
        if (travianConfiguration.getTrackEnemyVillageIds().size() > 0) {
            for (String villageId : travianConfiguration.getTrackEnemyVillageIds()) {
                villageId = villageId.trim();
                Village village = villageContainer.getVillageById(villageId);
                if (village != null) {
                    contextHandler.createDaemonThreadWithName("trackEnemy", () -> {
                        logger.trace("Call handleContextAccessAndRun for " + village.getVillageId() + " trackEnemy");
                        contextHandler.handleContextAccessAndRun(village.getVillageId(), () -> {
                            travianService.track(village.getVillageId());
                        });
                    }).start();
                }
            }
        }
    }

    protected void autoBuilding() {
        for (String villageId : travianConfiguration.getAutoBuildingVillageIds()) {
            villageId = villageId.trim();
            Village village = villageContainer.getVillageById(villageId);
            if (village != null) {
                contextHandler.createDaemonThreadWithName("autoBuilding", () -> {
                    logger.trace("Call handleContextAccessAndRun for " + village.getVillageId() + " autoBuilding");
                    contextHandler.handleContextAccessAndRun(village.getVillageId(), () -> {
                        travianService.getVillageResources(village);
                        travianService.resourceBuildingUpgradeAnyIfAvailable(village);
                    });
                }).start();
            }
        }
    }

    protected void recruitment() {
        for (Map.Entry<String, String> entry : travianConfiguration.getMilitaryBuyVillageIds().entrySet()) {
            String villageId = entry.getKey();
            String militaryBuyTroopType = entry.getValue();

            Village village = villageContainer.getVillageById(villageId);
            if (village != null) {
                contextHandler.createDaemonThreadWithName("recruitment", () -> {
                    logger.trace("Call handleContextAccessAndRun for " + village.getVillageId() + " recruitment");
                    contextHandler.handleContextAccessAndRun(village.getVillageId(), () -> {
                        Nation nation =  travianConfiguration.getNation(villageId);
                        TroopAlias.RequestTroopAlias troopType = TroopAlias.RequestTroopAlias.valueOf(militaryBuyTroopType.toUpperCase());
                        travianService.troopsSingleTypeAutoBuyMaxValue(troopType, nation, village.getVillageId());
                    });
                }).start();
            }
        }
    }
}
