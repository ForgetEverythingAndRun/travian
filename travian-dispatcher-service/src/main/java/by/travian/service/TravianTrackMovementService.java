package by.travian.service;

public interface TravianTrackMovementService {
    void track(String villageId);
}
