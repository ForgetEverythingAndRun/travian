package by.travian.service;

import by.travian.bean.entity.LoginEntity;

public interface TravianLoginService {
    boolean login(String user, String password);

    boolean login(LoginEntity entity);

    boolean isAuth();
}
