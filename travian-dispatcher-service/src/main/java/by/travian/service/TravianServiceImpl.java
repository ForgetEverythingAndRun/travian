package by.travian.service;

import by.travian.bean.Nation;
import by.travian.bean.container.StaticResourceContainer;
import by.travian.bean.container.VillageContainer;
import by.travian.bean.entity.*;
import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.building.Level;
import by.travian.bean.entity.building.Slot;
import by.travian.bean.entity.village.Village;
import by.travian.bean.task.Task;
import by.travian.bean.task.handler.ContextHandler;
import by.travian.bean.task.manager.BuildingTaskManagerImpl;
import by.travian.bean.task.manager.LoginTaskManagerImpl;
import by.travian.bean.task.manager.MarketTaskManagerImpl;
import by.travian.bean.task.manager.OffenseTaskManagerImpl;
import by.travian.bean.troop.TroopAlias;
import by.travian.common.command.RallyPointTrackEnemyCommand;
import by.travian.common.command.RecruitUnitCommand;
import by.travian.common.command.factory.CommandFactory;
import by.travian.common.context.TravianCommandContext;
import by.travian.configuration.BeanNameConst;
import by.travian.configuration.TravianConfiguration;
import by.travian.service.discord.DiscordApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static by.travian.service.TravianTrackMovementServiceImpl.RALLY_POINT_BUILDING_ID;
import static by.travian.service.TravianTrackMovementServiceImpl.RALLY_POINT_BUILDING_SLOT_ID;

@Service
public class TravianServiceImpl implements TravianService {
    public static final Logger logger = LoggerFactory.getLogger(TravianServiceImpl.class);

    /*private ResponseParser responseParser;
    private HttpExecutor httpExecutor;
    private TravianOffenseService travianOffenseService;
    private TravianMarketService travianMarketService;
    private TravianTrackMovementServiceImpl travianTrackMovementService;*/

    private ContextHandler contextHandler;
    private CommandFactory commandFactory;
    private TravianConfiguration travianConfiguration;
    private TravianLoginService travianLoginService;
    private TravianVillageService travianVillageService;
    private TravianHeroAdventureService travianHeroAdventureService;
    private DiscordApplication discordApplication;
    private TravianPlusService travianPlusService;
    private Map<Integer, List<Level>> buildingLevelMap;
    private SimpleDateFormat simpleDateFormat;
    private AtomicBoolean isAuth;
    private TravianResourceTrackService travianResourceTrackService;

    private OffenseTaskManagerImpl offenseTaskManager;
    private LoginTaskManagerImpl loginTaskManager;
    private MarketTaskManagerImpl marketTaskManager;
    private BuildingTaskManagerImpl buildingTaskManager;

    /*HttpExecutor httpExecutor,
    ResponseParser responseParser,
    TravianOffenseService travianOffenseService,
    TravianMarketService travianMarketService,
    TravianTrackMovementServiceImpl travianTrackMovementService,

        this.travianOffenseService = travianOffenseService;
        this.httpExecutor = httpExecutor;
        this.responseParser = responseParser;
        this.travianMarketService = travianMarketService;
        this.travianTrackMovementService = travianTrackMovementService;*/
    @Lazy
    @Autowired
    public TravianServiceImpl(ContextHandler contextHandler,
                              CommandFactory commandFactory,
                              TravianConfiguration travianConfiguration,
                              TravianLoginService travianLoginService,
                              TravianVillageService travianVillageService,
                              TravianHeroAdventureService travianHeroAdventureService,
                              DiscordApplication discordApplication,
                              TravianPlusService travianPlusService,
                              Map<Integer, List<Level>> buildingLevelMap,
                              SimpleDateFormat simpleDateFormat,
                              @Qualifier(BeanNameConst.IS_AUTHENTICATED) AtomicBoolean isAuth,
                              TravianResourceTrackService travianResourceTrackService,
                              OffenseTaskManagerImpl offenseTaskManager,
                              LoginTaskManagerImpl loginTaskManager,
                              MarketTaskManagerImpl marketTaskManager,
                              BuildingTaskManagerImpl buildingTaskManager) {
        this.contextHandler = contextHandler;
        this.commandFactory = commandFactory;
        this.travianConfiguration = travianConfiguration;
        this.travianLoginService = travianLoginService;
        this.travianVillageService = travianVillageService;
        this.travianHeroAdventureService = travianHeroAdventureService;
        this.discordApplication = discordApplication;
        this.travianPlusService = travianPlusService;
        this.buildingLevelMap = buildingLevelMap;
        this.simpleDateFormat = simpleDateFormat;
        this.isAuth = isAuth;
        this.travianResourceTrackService = travianResourceTrackService;
        this.offenseTaskManager = offenseTaskManager;
        this.loginTaskManager = loginTaskManager;
        this.marketTaskManager = marketTaskManager;
        this.buildingTaskManager = buildingTaskManager;
    }

    @Override
    public void updateVillages(String html) {
        travianVillageService.updateVillages(html);
    }

    @Override
    public void notifyWhenEnoughResources(Village village, StaticResourceContainer actual) {
        travianResourceTrackService.notifyWhenEnoughResources(village, actual);
    }

    @Override
    public void trackResources(Village village, StaticResourceContainer resources) {
        travianResourceTrackService.trackResources(village, resources);
    }

    @Override
    public void trackTravianPlusStatus() {
        travianPlusService.start();
    }

    @Override
    public Map<Integer, List<Level>> getBuildingLevelMap() {
        return buildingLevelMap;
    }

    @Override
    public VillageContainer getVillageContainerManager() {
        return travianVillageService.getVillageContainerManager();
    }

    @Override
    public void sleep(long timeout, TimeUnit unit) {
        try {
            long millisec = unit.toMillis(timeout);
            if (!Thread.currentThread().getName().equalsIgnoreCase("main")) {
                sleepLogMessage(timeout, unit);
            }
            Thread.sleep(millisec);
        } catch (InterruptedException e) {
            logger.error("", e);
        }
    }

    public void sleepLogMessage(long timeout, TimeUnit unit, String message) {
        long millisec = unit.toMillis(timeout);
        message = message.trim() + " ";
        logger.info(message + "Sleep time: ~" + unit.toMinutes(timeout) + " minute(s) till " + simpleDateFormat.format(System.currentTimeMillis() + millisec));
    }

    @Override
    public ContextHandler getContextHandler() {
        return contextHandler;
    }

    public void sleepLogMessage(long timeout, TimeUnit unit) {
        sleepLogMessage(timeout, unit, "");
    }


    @Override
    public void marketTasksExecute(List<Task<MarketEntity>> list) {
        marketTaskManager.execute(list);
    }

    @Override
    public void buildingTasksExecute(List<Task<BuildingEntity>> list) {
        buildingTaskManager.execute(list);
    }

    @Override
    public void offenseTasksExecute(List<Task<OffenseEntity>> list) {
        offenseTaskManager.execute(list);
    }

    @Override
    public void autoSendHeroAdventure() {
        long timeout = travianConfiguration.getHeroAdventuresTimeout();
        TimeUnit timeUnit = travianConfiguration.getHeroAdventuresTimeUnit();
        Thread thread = new Thread(() -> {
            while (true) {
                travianHeroAdventureService.autoSendHeroAdventure();
                try {
                    //timeUnit.sleep(timeout);
                    synchronized (travianConfiguration.hasHeroAdventure()) {
                        sleepLogMessage(timeout, timeUnit, " Wait till hero notified about next adventure. ");
                        travianConfiguration.hasHeroAdventure().wait(timeUnit.toMillis(timeout));
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    @Override
    public void login() {
        travianLoginService.login(travianConfiguration.getLogin(), travianConfiguration.getPassword());
        logger.info(String.format("isAuthenticated: %b", isAuth.get()));
    }


    @Override
    public void loginTaskExecute(Task<LoginEntity> task) {
        loginTaskManager.execute(Collections.singletonList(task));
    }


    @Override
    public void getVillages() {
        travianVillageService.getVillages();
    }

    @Override
    public boolean getVillageResourceBuildings(Village village) {
        return travianVillageService.getVillageResourceBuildings(village);
    }

    @Override
    public boolean getVillageCenterBuildings(Village village) {
        return travianVillageService.getVillageCenterBuildings(village);
    }

    @Override
    public boolean getVillageBuildingsResourceOrCenterByBuildingOwner(Village village, int buildingId) {
        return travianVillageService.getVillageBuildingsResourceOrCenterByBuildingOwner(village, buildingId);
    }

    @Override
    public boolean getVillageResources(Village village) {
        return travianVillageService.getVillageResources(village);
    }

    @Override
    public boolean getVillageTroops(Village village) {
        return travianVillageService.getVillageTroops(village);
    }

    @Override
    public boolean isBuildingQueueReady(Village village, boolean isResourceBuilding) {
        return travianVillageService.isBuildingQueueReady(village, isResourceBuilding);
    }

    @Override
    public Map.Entry<Slot, Building> centerBuildingUpgradeIfAvailable(Village village, Map.Entry<Slot, Building> entry) {
        return travianVillageService.centerBuildingUpgradeIfAvailable(village, entry);
    }

    @Override
    public Map.Entry<Slot, Building> resourceBuildingUpgradeIfAvailable(Village village, Map.Entry<Slot, Building> entry) {
        return travianVillageService.resourceBuildingUpgradeIfAvailable(village, entry);
    }

    @Override
    public void resourceBuildingUpgradeAnyIfAvailable(Village village) {
        travianVillageService.resourceBuildingUpgradeAnyIfAvailable(village);
    }

    @Override
    public void troopsSingleTypeAutoBuyMaxValue(TroopAlias.RequestTroopAlias alias, Nation nation, String villageId) {
        RecruitmentEntity entity = RecruitmentEntity.builder(villageId, alias, nation, 9999).build(); //pull it from config file
        RecruitUnitCommand recruitUnitCommand = commandFactory.createRecruitUnitCommand();
        recruitUnitCommand.setCommandContext(new TravianCommandContext(entity));
        recruitUnitCommand.execute();
    }

    @Override
    public void track(String villageId) {
        BuildingEntity buildingEntity = new BuildingEntity(new Building(RALLY_POINT_BUILDING_ID), new Slot(RALLY_POINT_BUILDING_SLOT_ID), villageId);
        RallyPointTrackEnemyCommand command = commandFactory.createRallyPointTrackEnemyCommand();
        command.setCommandContext(new TravianCommandContext(buildingEntity));
        command.execute();
    }


    @Override
    public void discordServiceStart() {
        discordApplication.start();
    }
}
