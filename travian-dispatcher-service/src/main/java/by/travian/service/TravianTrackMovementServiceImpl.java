package by.travian.service;

import by.travian.bean.container.VillageContainer;
import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.building.BuildingTabs;
import by.travian.bean.entity.building.Slot;
import by.travian.bean.entity.village.Village;
import by.travian.bean.container.MovementContainer;
import by.travian.bean.container.VillageResourceContainer;
import by.travian.configuration.TravianConfiguration;
import by.travian.executor.HttpExecutor;
import by.travian.request.TravianBuildingRequest;
import by.travian.service.discord.DiscordApplication;
import by.travian.util.ResponseParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class TravianTrackMovementServiceImpl implements TravianTrackMovementService {
    public final static Logger logger = LoggerFactory.getLogger(TravianTrackMovementServiceImpl.class);

    protected final static int RALLY_POINT_BUILDING_ID = 16;
    protected final static int RALLY_POINT_BUILDING_SLOT_ID = 39;

    private final HttpExecutor httpExecutor;
    private final ResponseParser responseParser;
    private final TravianConfiguration travianConfiguration;
    private final VillageContainer villageContainer;
    private final MovementContainer movementContainer;//should be a part of village container
    private final DiscordApplication discordApplication;

    @Lazy
    @Autowired
    public TravianTrackMovementServiceImpl(HttpExecutor httpExecutor, ResponseParser responseParser, TravianConfiguration travianConfiguration, VillageContainer villageContainer, MovementContainer movementContainer, DiscordApplication discordApplication) {
        this.httpExecutor = httpExecutor;
        this.responseParser = responseParser;
        this.travianConfiguration = travianConfiguration;
        this.villageContainer = villageContainer;
        this.movementContainer = movementContainer;
        this.discordApplication = discordApplication;
    }

    @Override
    public void track(String villageId) {
        Village village = villageContainer.getVillageById(villageId);

        BuildingEntity buildingEntity = new BuildingEntity(new Building(RALLY_POINT_BUILDING_ID), new Slot(RALLY_POINT_BUILDING_SLOT_ID), village.getVillageId());
        TravianBuildingRequest openRallyPointViewTab = new TravianBuildingRequest(buildingEntity, BuildingTabs.RallyPointTab.VIEW);
        ResponseEntity<String> response = httpExecutor.execute(openRallyPointViewTab);

        String responseBody = response.getBody();
        if (StringUtils.isBlank(responseBody)) {
            return;
        }
        VillageResourceContainer resourceContainer = responseParser.getVillageResources(response.getBody());
        village.setResources(resourceContainer);

        try {
            logger.debug("Track enemy movements village id: " + villageId);
            responseParser.updateEnemyMovementContainer(movementContainer, village, responseBody);
            discordApplication.push(movementContainer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
