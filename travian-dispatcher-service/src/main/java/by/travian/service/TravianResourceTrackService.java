package by.travian.service;

import by.travian.bean.entity.village.Village;
import by.travian.bean.container.StaticResourceContainer;

public interface TravianResourceTrackService {
    void notifyWhenEnoughResources(Village village, StaticResourceContainer actual);

    void trackResources(Village village, StaticResourceContainer resources);
}
