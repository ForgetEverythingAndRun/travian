package by.travian.service;


import by.travian.bean.container.VillageContainer;
import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.building.Slot;
import by.travian.bean.entity.village.Village;
import by.travian.bean.task.Task;
import by.travian.error.BuildingExtraDependencyException;

import java.util.List;
import java.util.Map;

public interface TravianVillageService {
    boolean isBuildingQueueReady(Village village, boolean isResourceBuilding);

    VillageContainer getVillageContainerManager();

    void getVillages();

    void updateVillages(String html);

    boolean getVillageResourceBuildings(Village village);

    boolean getVillageCenterBuildings(Village village);

    boolean getVillageBuildingsResourceOrCenterByBuildingOwner(Village village, int buildingId);

    boolean getVillageResources(Village village);

    boolean getVillageTroops(Village village);

    Map.Entry<Slot, Building> centerBuildingUpgradeIfAvailable(Village village, Map.Entry<Slot, Building> entry);

    Map.Entry<Slot, Building> resourceBuildingUpgradeIfAvailable(Village village, Map.Entry<Slot, Building> entry);

    void resourceBuildingUpgradeAnyIfAvailable(Village village);

    Map.Entry<Slot, Building> findVillageBuildingAndSlotAfterCreation(BuildingEntity entity);

    void makeBuildingTask(BuildingEntity task) throws BuildingExtraDependencyException;

}
