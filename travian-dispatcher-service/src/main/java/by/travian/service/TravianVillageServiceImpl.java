package by.travian.service;

import by.travian.bean.Nation;
import by.travian.bean.container.BuildingSlotContainer;
import by.travian.bean.container.TravianPlusContainer;
import by.travian.bean.container.VillageContainer;
import by.travian.bean.container.VillageResourceContainer;
import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.building.BuildingTabs;
import by.travian.bean.entity.building.Level;
import by.travian.bean.entity.building.Slot;
import by.travian.bean.entity.village.Village;
import by.travian.bean.task.handler.ContextHandler;
import by.travian.configuration.BeanNameConst;
import by.travian.configuration.TravianConfiguration;
import by.travian.error.BuildingExtraDependencyException;
import by.travian.error.VillageContainerUpdateException;
import by.travian.executor.HttpExecutor;
import by.travian.request.*;
import by.travian.util.ResponseParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class TravianVillageServiceImpl implements TravianVillageService {

    public final static Logger logger = LoggerFactory.getLogger(TravianVillageServiceImpl.class);

    private ContextHandler contextHandler;
    private HttpExecutor httpExecutor;
    private VillageContainer villageContainer;

    private ResourceBundle resourceBundle;//remove it?
    private Map<Integer, List<Level>> buildingLevelMap;//remove it?

    private ResponseParser responseParser;
    private TravianPlusContainer travianPlusContainer;
    private TravianConfiguration travianConfiguration;

    @Lazy
    @Autowired
    public TravianVillageServiceImpl(ContextHandler contextHandler, HttpExecutor httpExecutor, VillageContainer villageContainer, @Qualifier(BeanNameConst.BUILDINGS_RESOURCE_BUNDLE) ResourceBundle resourceBundle, @Qualifier(BeanNameConst.BUILDING_LEVEL_MAP) Map<Integer, List<Level>> buildingLevelMap, ResponseParser responseParser, TravianPlusContainer travianPlusContainer, TravianConfiguration travianConfiguration) {
        this.contextHandler = contextHandler;
        this.httpExecutor = httpExecutor;
        this.villageContainer = villageContainer;
        this.resourceBundle = resourceBundle;
        this.buildingLevelMap = buildingLevelMap;
        this.responseParser = responseParser;
        this.travianPlusContainer = travianPlusContainer;
        this.travianConfiguration = travianConfiguration;
    }

    @Override
    public VillageContainer getVillageContainerManager() {
        return villageContainer;
    }

    @Override
    public Map.Entry<Slot, Building> findVillageBuildingAndSlotAfterCreation(BuildingEntity entity) {
        Village village = villageContainer.getVillageById(entity.getVillageId());
        if (village == null) {
            logger.error("Can't find village with id: " + entity.getVillageId());
            return null;
        }
        Slot slot = new Slot(entity.getSlotId());
        Building building = new Building(entity.getId());
        Map.Entry<Slot, Building> entry = village.getBuildingSlotContainer().getBuildingSlotEntry(slot, building);
        if (entry.getValue() == null) {
            logger.error("Incorrect entity: " + entity + " It cant be null.");
            return null;
        }
        if (building.isResourceBuilding()) {
            return resourceBuildingUpgradeIfAvailable(village, entry);
        } else {
            return centerBuildingUpgradeIfAvailable(village, entry);
        }
    }

    @Override
    public void makeBuildingTask(BuildingEntity entity) throws BuildingExtraDependencyException {
        Village village = getVillageContainerManager().getVillageById(entity.getVillageId());
        Map.Entry<Slot, Building> result = findVillageBuildingAndSlotAfterCreation(entity);
        getVillageBuildingsResourceOrCenterByBuildingOwner(village, entity.getId());
        if (result != null) {
            Building building = result.getValue();
            if (building == null) {
                throw new BuildingExtraDependencyException("Village: " + village + " Building has extra requirements! Check: bId=" + entity.getId() + " Slot: " + result.getKey().getId());
            }
            logger.info("Complete: " + (building != null ? building.getLevel().getNumber() : null) + " of " + entity.getUntilLvlRepeat() + ". Village: " + village + ". Slot: " + result.getKey().getId() + " " + building);
        }
    }


    @Override
    public void getVillages() {
        TravianRequest villageRequest = new TravianDefaultRequest();
        ResponseEntity<String> response = httpExecutor.execute(villageRequest);

        String resource = response.getBody();
        if (StringUtils.isBlank(resource)) {
            return;
        }
        updateVillages(resource);

    }

    @Override
    public void updateVillages(String html) {
        try {
            List<Village> updatedVillageList = responseParser.getOwnVillages(html);
            if (updatedVillageList != null && !updatedVillageList.isEmpty()) {
                for (Village village : updatedVillageList) {
                    if (!villageContainer.contains(village)) {
                        villageContainer.add(village);
                        contextHandler.registerContext(village.getVillageId());
                        logger.debug("Added: " + village.toString());
                    }
                }

                Iterator<Village> iterator = villageContainer.getVillages().iterator();
                while (iterator.hasNext()) {
                    Village village = iterator.next();
                    if (!updatedVillageList.contains(village)) {
                        iterator.remove();
                        contextHandler.removeContext(village.getVillageId());
                        logger.debug("Removed: " + village.toString());
                    }
                }
            }
        } catch (VillageContainerUpdateException e) {
            logger.error("", e);
        }
    }

    /**
     * update resource building list, dashboard queue, resources
     */
    @Override
    public boolean getVillageResourceBuildings(Village village) {
        TravianRequest villageRequest = new TravianVillageRequest(village);
        ResponseEntity<String> response = httpExecutor.execute(villageRequest);

        String villageResource = response.getBody();
        if (StringUtils.isBlank(villageResource)) {
            return false;
        }

        responseParser.getVillageResourceBuildings(villageResource).forEach((k, v) -> {
            BuildingSlotContainer buildingSlotContainer = village.getBuildingSlotContainer();
            buildingSlotContainer.put(k, v);
            //logger.info(k.toString() + " " + v.toString());
        });

        village.setBuildingQueueTimeWait(System.currentTimeMillis() + responseParser.villageBuildingDashboardWaitTimestamp(villageResource));

        VillageResourceContainer resourceContainer = responseParser.getVillageResources(response.getBody());
        village.setResources(resourceContainer);

        return true;
    }

    /**
     * update center building list, dashboard queue, resources
     */
    public boolean getVillageCenterBuildings(Village village) {
        TravianRequest villageCenterRequest = new TravianVillageCenterRequest(village);
        ResponseEntity<String> villageCenterResponse = httpExecutor.execute(villageCenterRequest);

        String villageCenterResource = villageCenterResponse.getBody();

        if (StringUtils.isBlank(villageCenterResource)) {
            return false;
        }

        responseParser.getVillageCenterBuildings(villageCenterResource).forEach((k, v) -> {
            BuildingSlotContainer buildingSlotContainer = village.getBuildingSlotContainer();
            buildingSlotContainer.put(k, v);
            //logger.info(k.toString() + " " + v.toString());
        });

        village.setBuildingQueueTimeWait(System.currentTimeMillis() + responseParser.villageBuildingDashboardWaitTimestamp(villageCenterResource));

        VillageResourceContainer resourceContainer = responseParser.getVillageResources(villageCenterResource);
        village.setResources(resourceContainer);

        return true;
    }

    /**
     * update center or resource building list, dashboard queue and resources based on params
     *
     * @param village
     * @param buildingId
     * @return
     */
    @Override
    public boolean getVillageBuildingsResourceOrCenterByBuildingOwner(Village village, int buildingId) {
        return Building.isResourceBuilding(buildingId) ? getVillageResourceBuildings(village) : getVillageCenterBuildings(village);
    }

    @Override
    public boolean getVillageResources(Village village) {
        TravianRequest villageRequest = new TravianVillageRequest(village);
        ResponseEntity<String> response = httpExecutor.execute(villageRequest);

        String villageResource = response.getBody();
        if (StringUtils.isBlank(villageResource)) {
            return false;
        }

        VillageResourceContainer resourceContainer = responseParser.getVillageResources(response.getBody());//debug here
        village.setResources(resourceContainer);
        logger.trace(village.getResources().toString());

        return true;
    }

    @Override
    public boolean getVillageTroops(Village village) {
        TravianRequest villageRequest = new TravianVillageRequest(village);
        ResponseEntity<String> response = httpExecutor.execute(villageRequest);

        String villageResource = response.getBody();
        if (StringUtils.isBlank(villageResource)) {
            return false;
        }

        village.setTroops(responseParser.getVillageTroops(villageResource));
        logger.trace(village.getTroops().toString());

        return true;
    }

    /**
     * @param village
     * @param entry
     * @return pair of slot & building or null when upgrade is failed
     */
    @Override
    public Map.Entry<Slot, Building> centerBuildingUpgradeIfAvailable(Village village, Map.Entry<Slot, Building> entry) {
        BuildingEntity entity = new BuildingEntity(entry.getValue(), entry.getKey(), village.getVillageId());
        BuildingTabs.BuildingTabValue tab = BuildingTabs.createManagementTabByCenterBuildingId(entity.getId());
        TravianRequest request = new TravianBuildingRequest(entity, tab);
        ResponseEntity<String> response = httpExecutor.execute(request);

        if (StringUtils.isBlank(response.getBody())) {
            logger.trace("Response is empty.");
            return null;
        }
        String queryParamC = responseParser.findQueryParamC(response.getBody());
        if (StringUtils.isBlank(queryParamC)) {
            logger.trace("Query param 'C' is required for building request");
            return null;
        }
        request = new TravianUpgradeCenterBuildingRequest(entity, queryParamC);
        response = httpExecutor.execute(request);
        if (StringUtils.isBlank(response.getBody())) {
            logger.trace("Response is empty.");
            return null;
        }
        responseParser.updateDashboardAndResourcesAndBuildingsByBuildingOwner(village, Building.isResourceBuilding(entity.getId()), response.getBody());
        /*VillageResourceContainer resourceContainer = responseParser.getVillageResources(response.getBody());
        village.setResources(resourceContainer);
        logger.trace("Village: " + village + " center building list update:");
        logger.trace("[Begin]-------------------------");
        responseParser.getVillageCenterBuildings(response.getBody()).forEach((k, v) -> {
            BuildingSlotContainer buildingSlotContainer = village.getBuildingSlotContainer();
            buildingSlotContainer.put(k, v);
            logger.trace(k.toString() + " " + v.toString());
        });
        logger.trace("[End]-------------------------");*/
        return new AbstractMap.SimpleEntry<>(entry.getKey(), village.getBuildingSlotContainer().get(entry.getKey()));
    }

    @Override
    public Map.Entry<Slot, Building> resourceBuildingUpgradeIfAvailable(Village village, Map.Entry<Slot, Building> entry) {
        BuildingEntity entity = new BuildingEntity(entry.getValue(), entry.getKey(), village.getVillageId());
        TravianRequest request = new TravianBuildingRequest(entity, null);
        ResponseEntity<String> response = httpExecutor.execute(request);

        if (StringUtils.isBlank(response.getBody())) {
            return null;
        }
        String queryParamC = responseParser.findQueryParamC(response.getBody());
        if (StringUtils.isBlank(queryParamC)) {
            return null;
        }

        request = new TravianUpgradeResourceBuildingRequest(entity, queryParamC);
        response = httpExecutor.execute(request);

        if (StringUtils.isBlank(response.getBody())) {
            return null;
        }
        responseParser.updateDashboardAndResourcesAndBuildingsByBuildingOwner(village, Building.isResourceBuilding(entity.getId()), response.getBody());
        /*VillageResourceContainer resourceContainer = responseParser.getVillageResources(response.getBody());
        village.setResources(resourceContainer);
        logger.trace("Village: " + village + " resource building list update:");
        logger.trace("[Begin]-------------------------");
        responseParser.getVillageResourceBuildings(response.getBody()).forEach((k, v) -> {
            BuildingSlotContainer buildingSlotContainer = village.getBuildingSlotContainer();
            buildingSlotContainer.put(k, v);
            logger.trace(k.toString() + " " + v.toString());
        });
        logger.trace("[End]-------------------------");*/
        return new AbstractMap.SimpleEntry<>(entry.getKey(), village.getBuildingSlotContainer().get(entry.getKey()));
    }

    @Override
    public void resourceBuildingUpgradeAnyIfAvailable(Village village) {
        boolean cropUpgradeEnabled = travianConfiguration.isAutoBuildingCropEnabled();

        VillageResourceContainer resourceContainer = village.getResources();
        int resourceIdInPriority = resourceContainer.getDeficitResourceId();
        List<Map.Entry<Slot, Building>> list = village.getBuildingSlotContainer().getAvailableResourceBuildingsSortedByLvl();

        if (!cropUpgradeEnabled) {
            list = list.stream().filter((entry) -> {
                return !Integer.valueOf(4).equals(entry.getValue().getId());
            }).collect(Collectors.toList());
        }

        list = list.stream().sorted((o1, o2) -> {
            int priority1 = o1.getValue().getId() == resourceIdInPriority ? -1 : 0;
            int priority2 = o2.getValue().getId() == resourceIdInPriority ? -1 : 0;
            //move resource with priority at the beginning
            return priority1 - priority2;
        }).collect(Collectors.toList());

        for (Map.Entry<Slot, Building> entry : list) {
            if (!isBuildingQueueReady(village, true)) {
                return;
            }
            if (resourceBuildingUpgradeIfAvailable(village, entry) != null) {
                logger.info("Auto-building(resources). Status: OK " + village + " " + entry.getValue() + " slot: " + entry.getKey().getId());
            }
        }
    }

    @Override
    public boolean isBuildingQueueReady(Village village, boolean isResourceBuilding) {
        TravianRequest villageRequest = new TravianVillageRequest(village); //dorf1.php
        ResponseEntity<String> response = httpExecutor.execute(villageRequest);

        if (StringUtils.isBlank(response.getBody())) {
            logger.debug("Can't check building queue because response is empty/null");
            return false;
        }
        responseParser.updateDashboardAndResourcesAndBuildingsByBuildingOwner(village, isResourceBuilding, response.getBody());

        JSONArray jsonArray = responseParser.villageBuildingItemsInQueue(response.getBody());
        int buildingsInQueue = jsonArray.length();
        int travianPlusCapacity = (travianPlusContainer.isTravianPlusActive() ? 1 : 0);
        int maxBuildingQueueCapacity = Nation.ROMANS == travianConfiguration.getNation(village.getVillageId()) ? 2 : 1;
        logger.trace("Dashboard queue length: " + buildingsInQueue + " max capacity: " + (maxBuildingQueueCapacity + travianPlusCapacity) + " json: " + jsonArray);
        if (buildingsInQueue < (maxBuildingQueueCapacity + travianPlusCapacity)) {
            int resourceQueue = 0;
            int centerQueue = 0;
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject obj = jsonArray.getJSONObject(i);
                    if (Building.isResourceBuilding(obj.getInt("gid"))) {
                        resourceQueue++;
                    } else {
                        centerQueue++;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (isResourceBuilding) {
                return resourceQueue < (1 + travianPlusCapacity);
            } else {
                return centerQueue < (1 + travianPlusCapacity);
            }
        }
        return false;
    }
}
