package by.travian.service;

import by.travian.bean.container.TravianPlusContainer;
import by.travian.configuration.TravianConfiguration;
import by.travian.executor.HttpExecutor;
import by.travian.request.TravianDefaultRequest;
import by.travian.request.TravianPlusRequest;
import by.travian.request.TravianRequest;
import by.travian.util.DefaultParamParser;
import by.travian.util.ResponseParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import static java.lang.Thread.State.RUNNABLE;

@Service
public class TravianPlusServiceImpl implements TravianPlusService {
    public final static Logger logger = LoggerFactory.getLogger(TravianPlusServiceImpl.class);
    private final static String THREAD_NAME = "TravianPlus";

    private HttpExecutor httpExecutor;
    private ResponseParser responseParser;
    private TravianPlusContainer travianPlusContainer;
    private TravianConfiguration travianConfiguration;
    private Thread thread = createTravianPlusThread();

    @Lazy
    @Autowired
    public TravianPlusServiceImpl(HttpExecutor httpExecutor, ResponseParser responseParser, TravianPlusContainer travianPlusContainer, TravianConfiguration travianConfiguration) {
        this.httpExecutor = httpExecutor;
        this.responseParser = responseParser;
        this.travianPlusContainer = travianPlusContainer;
        this.travianConfiguration = travianConfiguration;
    }

    //Create method re-start

    @Override
    public void start() {
        if (thread.getState() == Thread.State.NEW) {
            thread.start();
        }else{
            logger.info("Start was denied. Travian Plus Service has already started.");
        }
    }

    private Thread createTravianPlusThread() {
        Thread monitorTravianPlusStatus = new Thread(travianPlusContainerUpdate());
        monitorTravianPlusStatus.setName(THREAD_NAME);
        monitorTravianPlusStatus.setDaemon(true);
        return monitorTravianPlusStatus;
    }

    private Runnable travianPlusContainerUpdate() {
        return new Runnable() {
            @Override
            public void run() {
                while (true) {
                    String token = getToken();
                    if (token != null) {
                        TravianPlusRequest travianPlusRequest = new TravianPlusRequest(token);
                        ResponseEntity<String> response = httpExecutor.execute(travianPlusRequest);

                        String responseBody = response.getBody();
                        if (StringUtils.isBlank(responseBody)) {
                            logger.debug("Response body is empty.");
                            return;
                        }
                        try {
                            responseBody = new JSONObject(responseBody).get("html").toString();
                            responseParser.updateTravianPlus(travianPlusContainer, responseBody);
                            logger.debug("Travian plus active: " + travianPlusContainer.isTravianPlusActive());
                        } catch (JSONException e) {
                            logger.debug("", e);
                        }
                    } else {
                        logger.error("Can't receive a token.");
                    }
                    try {
                        if (travianPlusContainer.isTravianPlusActive()) {
                            Thread.sleep(travianPlusContainer.getTimestamp() - System.currentTimeMillis());
                        } else {
                            travianConfiguration.getTravianPlusTrackTimeUnit().sleep(travianConfiguration.getTravianPlusTrackTimeout());
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    //TODO: replace by GetBaseAuthTokenCommand
    private String getToken() {
        TravianRequest villageRequest = new TravianDefaultRequest();
        ResponseEntity<String> response = httpExecutor.execute(villageRequest);

        String body = response.getBody();
        if (StringUtils.isBlank(body)) {
            logger.debug("Response body is empty.");
            return null;
        }

        String token = DefaultParamParser.findToken(body);
        return token;
    }

}
