package by.travian.service;

import by.travian.bean.entity.village.Village;
import by.travian.bean.container.StaticResourceContainer;
import by.travian.error.TravianException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class TravianResourceTrackServiceIml implements TravianResourceTrackService {
    public final static Logger logger = LoggerFactory.getLogger(TravianResourceTrackServiceIml.class);

    private Map<Village, List<StaticResourceContainer>> map = new ConcurrentHashMap<>();

    public synchronized void trackResources(Village village, StaticResourceContainer resources) {
        if (village != null && resources != null) {
            List<StaticResourceContainer> containers = map.get(village);
            if (containers == null) {
                containers = new ArrayList<>();
                map.put(village, containers);
            }
            containers.add(resources);
        } else {
            throw new TravianException("Key: 'village' or value: 'resources' can't be null.");
        }
    }

    public synchronized void notifyWhenEnoughResources(Village village, StaticResourceContainer actual) {
        List<StaticResourceContainer> resources = get(village);
        Iterator<StaticResourceContainer> i = resources.iterator();
        while (i.hasNext()) {
            StaticResourceContainer expected = i.next();
            if (isEnoughResources(actual, expected)) {
                synchronized (expected) {
                    expected.notifyAll();
                    i.remove();
                }
                logger.info("Resource notification for village:" + village.getVillageId(), "");
            }
        }
    }

    protected boolean isEnoughResources(StaticResourceContainer actual, StaticResourceContainer expected) {
        return actual.getWood().getAmount() >= expected.getWood().getAmount()
                && actual.getClay().getAmount() >= expected.getClay().getAmount()
                && actual.getIron().getAmount() >= expected.getIron().getAmount()
                && actual.getCrop().getAmount() >= expected.getCrop().getAmount();
    }

    protected synchronized List<StaticResourceContainer> get(Village village) {
        if (village != null) {
            List<StaticResourceContainer> containers = map.get(village);
            if (containers == null) {
                containers = new ArrayList<>();
            }
            return containers;
        } else {
            throw new TravianException("Key: 'village' can't be null.");
        }
    }
}
