package by.travian.service;

import by.travian.bean.entity.LoginEntity;
import by.travian.configuration.BeanNameConst;
import by.travian.constant.TravianConstant;
import by.travian.executor.HttpExecutor;
import by.travian.request.TravianLoginRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class TravianLoginServiceImpl implements TravianLoginService {
    private final HttpExecutor httpExecutor;
    private final AtomicBoolean isAuth;

    @Lazy
    @Autowired
    public TravianLoginServiceImpl(HttpExecutor httpExecutor, @Qualifier(BeanNameConst.IS_AUTHENTICATED) AtomicBoolean isAuth) {
        this.httpExecutor = httpExecutor;
        this.isAuth = isAuth;
    }

    @Override
    public boolean login(String user, String password) {
        return login(new LoginEntity(user, password));
    }

    @Override
    public boolean login(LoginEntity entity) {
        TravianLoginRequest request = new TravianLoginRequest(entity);
        ResponseEntity<String> response = httpExecutor.execute(request);
        isAuth.set(isAuth(response.getBody()));
        return isAuth.get();
    }

    @Override
    public boolean isAuth() {
        return isAuth.get();
    }

    public boolean isAuth(String responseBody) {
        return !StringUtils.isBlank(responseBody) && !responseBody.contains(TravianConstant.Common.LOGIN_FORM);
    }

}
