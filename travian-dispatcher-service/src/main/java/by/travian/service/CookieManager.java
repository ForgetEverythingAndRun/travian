package by.travian.service;

import by.travian.constant.TravianConstant;
import by.travian.error.CookieException;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class CookieManager {
    public final static Logger logger = LoggerFactory.getLogger(CookieManager.class);
    private List<String> keyExceptionList = new ArrayList<String>(Arrays.asList("httponly"));
    private static final String LOCALE_KEY = "travian-language";

    private Map store;

    private Locale locale = new Locale("en", "US");//new Locale("ru", "RU")

    private DateFormat dateFormat;

    public CookieManager() {
        store = new HashMap();
        dateFormat = new SimpleDateFormat(TravianConstant.Cookie.DATE_FORMAT);
        //TODO: set timezone
    }

    public Locale getLocale() {
        /*URL url = null;
        try {
            url = new URL(UriComponentsBuilder.newInstance()
                    .scheme(TravianConstant.Common.HTTPS_SCHEME)
                    .host(TravianConstant.Common.HOST).toUriString());
            String domain = getDomainFromHost(url.getHost());
            //Map domainStore =store.get(domain) //Locale.forLanguageTag(.get(LOCALE_KEY));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }*/
        return locale;
    }


    public void storeCookies(String domain, HttpHeaders headers) throws CookieException {
        //TODO: re-write extraction of cookies and url host's info
        //String domain = getDomainFromHost(conn.getURL().getHost());
        Map domainStore;
        if (store.containsKey(domain)) {
            domainStore = (Map) store.get(domain);
        } else {
            domainStore = new HashMap();
            store.put(domain, domainStore);
        }

        headers.forEach((headerName, headerValue) -> {
            if (headerName.equalsIgnoreCase(TravianConstant.Cookie.SET_COOKIE)) {
                Map cookie = new HashMap();
                StringTokenizer st = new StringTokenizer(String.join(TravianConstant.Cookie.COOKIE_VALUE_DELIMITER, headerValue), TravianConstant.Cookie.COOKIE_VALUE_DELIMITER);

                if (st.hasMoreTokens()) {
                    String token = st.nextToken();
                    if (isHttpOnly(token)) {
                        return;
                    }
                    logger.trace(token);
                    String name = token.substring(0, token.indexOf(TravianConstant.Cookie.NAME_VALUE_SEPARATOR));
                    String value = token.substring(token.indexOf(TravianConstant.Cookie.NAME_VALUE_SEPARATOR) + 1, token.length());
                    domainStore.put(name, cookie);
                    cookie.put(name, value);
                }

                while (st.hasMoreTokens()) {
                    String token = st.nextToken();
                    if (isHttpOnly(token)) {
                        continue;
                    }
                    String name = token.substring(0, token.indexOf(TravianConstant.Cookie.NAME_VALUE_SEPARATOR));
                    String value = token.substring(token.indexOf(TravianConstant.Cookie.NAME_VALUE_SEPARATOR) + 1, token.length());
                    logger.trace(token);
                    cookie.put(name.toLowerCase(), value);
                }
            }
        });
    }

    private boolean isHttpOnly(String token) {
        return keyExceptionList.contains(token.trim().toLowerCase());
    }

    @SneakyThrows
    public String getCookies(String uri) throws CookieException {
        URL url = new URL(uri);
        String domain = getDomainFromHost(url.getHost());
        String path = url.getPath();

        Map domainStore = (Map) store.get(domain);
        if (domainStore == null) return "";

        StringBuffer cookieStringBuffer = new StringBuffer();

        Iterator cookieNames = domainStore.keySet().iterator();
        while (cookieNames.hasNext()) {
            String cookieName = (String) cookieNames.next();
            Map cookie = (Map) domainStore.get(cookieName);
            if (comparePaths((String) cookie.get(TravianConstant.Cookie.PATH), path) && isNotExpired((String) cookie.get(TravianConstant.Cookie.EXPIRES))) {
                cookieStringBuffer.append(cookieName);
                cookieStringBuffer.append("=");
                cookieStringBuffer.append((String) cookie.get(cookieName));
                if (cookieNames.hasNext()) cookieStringBuffer.append(TravianConstant.Cookie.SET_COOKIE_SEPARATOR);
            }
        }
        logger.trace(cookieStringBuffer.toString());
        return cookieStringBuffer.toString();
    }

    private String getDomainFromHost(String host) {
        if (host.indexOf(TravianConstant.Cookie.DOT) != host.lastIndexOf(TravianConstant.Cookie.DOT)) {
            return host.substring(host.indexOf(TravianConstant.Cookie.DOT) + 1);
        } else {
            return host;
        }
    }

    private boolean isNotExpired(String cookieExpires) {
        if (cookieExpires == null) return true;
        Date now = new Date();
        try {
            return (now.compareTo(dateFormat.parse(cookieExpires))) <= 0;
        } catch (java.text.ParseException pe) {
            pe.printStackTrace();
            return false;
        }
    }

    private boolean comparePaths(String cookiePath, String targetPath) {
        if (cookiePath == null) {
            return true;
        } else if (cookiePath.equals("/")) {
            return true;
        } else if (targetPath.regionMatches(0, cookiePath, 0, cookiePath.length())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return store.toString();
    }
}
