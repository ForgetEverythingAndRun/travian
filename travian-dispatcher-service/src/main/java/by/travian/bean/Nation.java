package by.travian.bean;

import by.travian.bean.troop.*;

public enum Nation {
    GAULS {
        @Override
        public GaulsTroop[] getNationTroopValues() {
            return GaulsTroop.values();
        }

        @Override
        public TroopAlias.RequestTroopAlias getNationSpyAlias() {
            return TroopAlias.RequestTroopAlias.T3;
        }
    }, ROMANS {
        @Override
        public RomansTroop[] getNationTroopValues() {
            return RomansTroop.values();
        }

        @Override
        public TroopAlias.RequestTroopAlias getNationSpyAlias() {
            return TroopAlias.RequestTroopAlias.T4;
        }
    }, GERMANS {
        @Override
        public GermansTroop[] getNationTroopValues() {
            return GermansTroop.values();
        }

        @Override
        public TroopAlias.RequestTroopAlias getNationSpyAlias() {
            return TroopAlias.RequestTroopAlias.T4;
        }
    }, EGYPTIANS {
        @Override
        public EgyptiansTroop[] getNationTroopValues() {
            return EgyptiansTroop.values();
        }

        @Override
        public TroopAlias.RequestTroopAlias getNationSpyAlias() {
            return TroopAlias.RequestTroopAlias.T4;
        }
    }, HUNS {
        @Override
        public HunsTroop[] getNationTroopValues() {
            return HunsTroop.values();
        }

        @Override
        public TroopAlias.RequestTroopAlias getNationSpyAlias() {
            return TroopAlias.RequestTroopAlias.T3;
        }
    };

    public abstract NationTroop[] getNationTroopValues();

    public abstract TroopAlias.RequestTroopAlias getNationSpyAlias();


    public NationTroop getNationTroopByAlias(TroopAlias.RequestTroopAlias alias) {
        for (NationTroop type : getNationTroopValues()) {
            if (type.getAlias() == alias) {
                return type;
            }
        }
        throw new IllegalArgumentException("Invalid alias: " + alias);
    }

}