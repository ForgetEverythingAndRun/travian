package by.travian.bean.container;

import by.travian.bean.entity.HeroAdventureEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class HeroAdventureContainer extends ArrayList<HeroAdventureEntity> {
    private HeroAdventureComparatorType type = HeroAdventureComparatorType.NONE;
    private HeroAdventureSortOrder order = HeroAdventureSortOrder.ASCENDING;
    private AtomicLong waitHeroTimestamp = new AtomicLong(0);
    private volatile boolean isHeroAtHome;


    public long getWaitHeroTimestamp() {
        return waitHeroTimestamp.get();
    }

    public void setWaitHeroTimestamp(long waitHeroTimestamp) {
        this.waitHeroTimestamp.getAndSet(waitHeroTimestamp);
    }

    public boolean isHeroAtHome() {
        return isHeroAtHome;
    }

    public void setHeroAtHome(boolean heroAtHome) {
        isHeroAtHome = heroAtHome;
    }

    public List<HeroAdventureEntity> getAdventuresSortedBy(HeroAdventureComparatorType type, HeroAdventureSortOrder order) {
        if (type != null && order != null) {
            Comparator<HeroAdventureEntity> comparator = type.getComparator();
            if (comparator != null) {
                return this.stream().sorted(order.isAscending ? comparator : comparator.reversed()).collect(Collectors.toList());
            }
        }
        return this;
    }

    public enum HeroAdventureSortOrder {
        ASCENDING(true), DESCENDING(false);

        protected boolean isAscending;

        HeroAdventureSortOrder(boolean isAscending) {
            this.isAscending = isAscending;
        }

        public boolean isAscending() {
            return isAscending;
        }
    }

    public enum HeroAdventureComparatorType {
        DIFFICULTY(new HeroAdventureDifficultyComparator()), TIME(new HeroAdventureTimeComparator()), NONE(null);

        protected Comparator<HeroAdventureEntity> comparator;

        HeroAdventureComparatorType(Comparator<HeroAdventureEntity> comparator) {
            this.comparator = comparator;
        }

        public Comparator<HeroAdventureEntity> getComparator() {
            return comparator;
        }
    }

    public static class HeroAdventureDifficultyComparator implements Comparator<HeroAdventureEntity> {
        @Override
        public int compare(HeroAdventureEntity a1, HeroAdventureEntity a2) {
            //hard = 0, normal > 0 (1,2,3)
            return (a2.getDifficulty() - a1.getDifficulty());
        }
    }


    public static class HeroAdventureTimeComparator implements Comparator<HeroAdventureEntity> {
        @Override
        public int compare(HeroAdventureEntity a1, HeroAdventureEntity a2) {
            return ((int) (a1.getTime() - a2.getTime()));
        }
    }
}
