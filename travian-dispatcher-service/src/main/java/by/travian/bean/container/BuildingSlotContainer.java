package by.travian.bean.container;

import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.building.Slot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.NotSupportedException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class BuildingSlotContainer extends ConcurrentHashMap<Slot, Building> {
    public final static Logger logger = LoggerFactory.getLogger(BuildingSlotContainer.class);

    @Override
    public Building get(Object key) {
        if (key instanceof Slot) {
            return super.get(key);
        }
        throw new NoSuchElementException();
    }

    public List<Map.Entry<Slot, Building>> getAvailableResourceBuildingsSortedByLvlAndDeficit() {
        List<Map.Entry<Slot, Building>> list = new ArrayList<>();
        for (Map.Entry<Slot, Building> e : this.entrySet()) {
            if (e.getValue().getId() <= 4) {// four types of fields: wood, clay, iron, crop
                list.add(e);
            }
        }
        //Collections.sort(list, new BuildingEntryLevelComparator());

        Comparator<Map.Entry<Slot, Building>> comparator = new BuildingEntryLevelComparator();
        list.stream().sorted(comparator).collect(Collectors.toList());
        return list;
    }

    public List<Map.Entry<Slot, Building>> getAvailableResourceBuildingsSortedByLvl() {
        List<Map.Entry<Slot, Building>> list = new ArrayList<>();
        for (Map.Entry<Slot, Building> e : this.entrySet()) {
            if (e.getValue().getId() <= 4) {// four types of fields: wood, clay, iron, crop
                list.add(e);
            }
        }
        Collections.sort(list, new BuildingEntryLevelComparator());
        return list;
    }

    /*
        when we have few building with the same id then need choose building with the lowest lvl for upgrade
     */
    public List<Map.Entry<Slot, Building>> getAvailableCenterBuildingsSortedByLvl() {
        List<Map.Entry<Slot, Building>> list = new ArrayList<>();
        for (Map.Entry<Slot, Building> e : this.entrySet()) {
            if (e.getValue().getId() > 4) {
                list.add(e);
            }
        }
        Collections.sort(list, new BuildingEntryLevelComparator());
        return list;
    }

    public Slot getCenterBuildingEmptySlot() {
        Slot slot = null;
        for (int i = 19; i < 40; i++) {
            slot = new Slot(i);
            if (!this.containsKey(slot)) {
                return slot;
            }
        }
        return null;
    }


    public Slot getFirstKeyByMatchedValue(List<String> keys) {
        Slot firstSlotId = null;
        for (String id : keys) {
            for (Map.Entry<Slot, Building> e : this.entrySet()) {
                if (e.getValue().getId().toString().equals(id)) {
                    firstSlotId = e.getKey();
                }
            }
        }
        return firstSlotId;
    }

    public Map.Entry<Slot, Building> getFirstEntryByMatchedValue(List<String> keys) {
        Map.Entry<Slot, Building> entry = null;
        for (String id : keys) {
            for (Map.Entry<Slot, Building> e : this.entrySet()) {
                if (e.getValue().getId().toString().equals(id)) {
                    entry = new AbstractMap.SimpleEntry<>(e.getKey(), e.getValue());
                }
            }
        }
        return entry;
    }

    public static class BuildingEntryLevelComparator implements Comparator<Map.Entry<Slot, Building>> {
        @Override
        public int compare(Entry<Slot, Building> b1, Entry<Slot, Building> b2) {
            return b1.getValue().getLevel().getNumber().compareTo(b2.getValue().getLevel().getNumber());
        }
    }

    /**
     * @param slot is location of building which can be equals 0 for getting default value or existing one
     * @param buildingParam is building id
     * @return pair of slot and building or null when slot already has another building id
     * */

    public Map.Entry<Slot, Building> getBuildingSlotEntry(Slot slot, Building buildingParam) {
        Map.Entry<Slot, Building> result = null;
        Building newBuilding = new Building(buildingParam, true);
        if (slot.getId() > 0) {
            // resource building always returns object because nested slots are bind on resource fields
            // center building returns object when construction is already built
            Building building = get(slot);
            if (building == null) {// new center building when slot is defined
                result = new AbstractMap.SimpleEntry<>(slot, newBuilding);
            } else if (building.getId().equals(buildingParam.getId())) {
                result = new AbstractMap.SimpleEntry<>(slot, building);
            } else {
                logger.error("Slot: " + slot.getId() + " Expected buildingId: " + buildingParam.getId() + " Actual buildingId: " + building.getId());
            }
        } else {//slot equals 0 case then get default
            if (buildingParam.isResourceBuilding()) {
                Map.Entry<Slot, Building> resourceBuildingSlotWithMinBuildingLvl = getAvailableResourceBuildingsSortedByLvl().stream().
                        filter(entry -> buildingParam.getId().equals(entry.getValue().getId()))
                        .findFirst().get();
                result = resourceBuildingSlotWithMinBuildingLvl;
            } else {
                Map.Entry<Slot, Building> centerBuildingEmptySlot = new AbstractMap.SimpleEntry<>(getCenterBuildingEmptySlot(), newBuilding);
                Map.Entry<Slot, Building> centerBuildingSlotWithMinBuildingLvl = getAvailableCenterBuildingsSortedByLvl().stream().
                        filter(entry -> buildingParam.getId().equals(entry.getValue().getId()))
                        .findFirst()
                        .orElse(centerBuildingEmptySlot);
                result = centerBuildingSlotWithMinBuildingLvl;
            }
        }

        return result;
    }

}

