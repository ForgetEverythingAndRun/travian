package by.travian.bean.container;

import by.travian.bean.resource.VillageResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Администратор on 29.10.2017.
 */
public class VillageResourceContainer extends ResourceContainer<VillageResource> {
    public final static Logger logger = LoggerFactory.getLogger(VillageResourceContainer.class);

    protected int warehouseCapacity;
    protected int granaryCapacity;
    protected int freeCrop;

    public VillageResourceContainer() {
        //buildings_en_US.properties
        //building id of resource generation: 1
        setWood(new VillageResource(1));
        setClay(new VillageResource(2));
        setIron(new VillageResource(3));
        setCrop(new VillageResource(4));
    }

    public int getWarehouseCapacity() {
        return warehouseCapacity;
    }

    public void setWarehouseCapacity(int warehouseCapacity) {
        if (warehouseCapacity > 0) {
            this.warehouseCapacity = warehouseCapacity;
        } else {
            logger.error("Error: Current warehouse capacity is " + this.warehouseCapacity + ". Setted value is " + warehouseCapacity);
        }
    }

    public int getGranaryCapacity() {
        return granaryCapacity;
    }

    public void setGranaryCapacity(int granaryCapacity) {
        if (granaryCapacity > 0) {
            this.granaryCapacity = granaryCapacity;
        } else {
            logger.error("Error: Current granary capacity is " + this.granaryCapacity + ". Setted value is " + granaryCapacity);
        }
    }

    public int getFreeCrop() {
        return freeCrop;
    }

    public void setFreeCrop(int freeCrop) {
        this.freeCrop = freeCrop;
    }

    public int getDeficitResourceId() {
        List<VillageResource> list = Arrays.asList(getWood(), getClay(), getIron(), getCrop());

        VillageResource min = list
                .stream()
                .min((o1, o2) -> {
                    int capacity1 = getStorageCapacityByResourceId(o1.getId());
                    int capacity2 = getStorageCapacityByResourceId(o2.getId());
                    int result1 = o1.getAmount() * 100 / capacity1;
                    int result2 = o2.getAmount() * 100 / capacity2;
                    return result1 - result2;
                })
                .orElseThrow(NoSuchElementException::new);
        return min.getId();
    }

    protected int getStorageCapacityByResourceId(int id) {
        return id == 4 ? granaryCapacity : warehouseCapacity;
    }

    /*public int getDeficitResourceId() {
        List<VillageResource> list = Arrays.asList(getWood(), getClay(), getIron(), getCrop());

        VillageResource min = list
                .stream()
                .min(Comparator.comparing(VillageResource::getAmount))
                .orElseThrow(NoSuchElementException::new);
        return min.getId();
    }*/

    /*
        Collections.sort(list, new Comparator<VillageResource>(){
        @Override
        public int compare(VillageResource o1, VillageResource o2) {
            return o1.getAmount() - o2.getAmount();
        }});
        return list.get(0).getId();
    */

    public static void main(String[] args) {
        //System.out.println(Arrays.asList("123".split(";")));
        //System.out.println("expected: 40 min actual: " + TimeUnit.MILLISECONDS.toMinutes(VillageResourceContainer.getRequiredTimeToProduceResource(7655, new VillageResource(3, 158, 11200))));

        //Iron: 9749 production: 17500 required: 21810
        //System.out.println("expected: 33 min actual: " + TimeUnit.MILLISECONDS.toMinutes(VillageResourceContainer.getRequiredTimeToProduceResource(21810, new VillageResource(3, 9749, 21700))));

        /*List<Long> list = Arrays.asList(
                VillageResourceContainer.getRequiredTimeToProduceResource(16615, new VillageResource(1, 43504, 17500)),
                VillageResourceContainer.getRequiredTimeToProduceResource(14540, new VillageResource(2, 32329, 17500)),
                VillageResourceContainer.getRequiredTimeToProduceResource(21810, new VillageResource(3, 9749, 21700)),
                VillageResourceContainer.getRequiredTimeToProduceResource(18690, new VillageResource(4, 131499, 17500))
        );
        Long resourceProduceMaxWaitTime = list
                .stream()
                .mapToLong(v -> v)
                .max().getAsLong();

        System.out.println(TimeUnit.MILLISECONDS.toMinutes(resourceProduceMaxWaitTime));*/
    }

    /* it works (verified):  */
    public static long getRequiredTimeToProduceResource(int needResourceAmount, VillageResource resource) {
        int haveResource = resource.getAmount();
        long produceResourceTime = 0;
        if (haveResource < needResourceAmount) {
            int needed = needResourceAmount - haveResource;
            produceResourceTime = Math.round(((double) needed / (double) resource.getProduction()) * (double) TimeUnit.HOURS.toMillis(1));
        }
        return produceResourceTime;
    }

    public void updateResources(VillageResourceContainer container) {
        this.updateResources(container.getWood().getAmount()
                , container.getClay().getAmount()
                , container.getIron().getAmount()
                , container.getCrop().getAmount()
                , container.getWood().getProduction()
                , container.getClay().getProduction()
                , container.getIron().getProduction()
                , container.getCrop().getProduction());
        this.setWarehouseCapacity(container.getWarehouseCapacity());
        this.setGranaryCapacity(container.getGranaryCapacity());
        this.setFreeCrop(container.getFreeCrop());
    }

    public void updateResources(int wood, int clay, int iron, int crop, int prodWood, int prodClay, int prodIron, int prodCrop) {
        getWood().setAmount(wood);
        getClay().setAmount(clay);
        getIron().setAmount(iron);
        getCrop().setAmount(crop);
        getWood().setProduction(prodWood);
        getClay().setProduction(prodClay);
        getIron().setProduction(prodIron);
        getCrop().setProduction(prodCrop);
    }

    public void updateResources(int wood, int clay, int iron, int crop) {
        getWood().setAmount(wood);
        getClay().setAmount(clay);
        getIron().setAmount(iron);
        getCrop().setAmount(crop);
    }

    public void updateResourcesBasedOnProductionPerTime() {
        updateResouceAmountBasedOnProductionPerTime(getWood());
        updateResouceAmountBasedOnProductionPerTime(getClay());
        updateResouceAmountBasedOnProductionPerTime(getIron());
        updateResouceAmountBasedOnProductionPerTime(getCrop());
    }

    private void updateResouceAmountBasedOnProductionPerTime(VillageResource resource) {
        long currentTime = System.currentTimeMillis();
        long timeFromLastUpdate = currentTime - resource.getLastUpdate();
        long producedResourceUnitPerTime = getUnitResourceProduceTime(resource);
        int resourceAmount = 0;
        if (timeFromLastUpdate > producedResourceUnitPerTime) {
            resourceAmount = (int) (timeFromLastUpdate / producedResourceUnitPerTime);
            resource.setAmount(resource.getAmount() + resourceAmount);
        }
    }

    private long getUnitResourceProduceTime(VillageResource resource) {
        int unitResourceProduction = resource.getProduction();
        //how much time need to produce resource unit
        return TimeUnit.HOURS.toMillis(1) / unitResourceProduction;
    }

    @Override
    public String toString() {
        return "{" +
                "warehouseCapacity=" + warehouseCapacity +
                ", granaryCapacity=" + granaryCapacity +
                ", freeCrop=" + freeCrop +
                '}';
    }
}
