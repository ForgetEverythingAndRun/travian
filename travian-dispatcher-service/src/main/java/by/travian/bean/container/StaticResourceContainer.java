package by.travian.bean.container;

import by.travian.bean.resource.Resource;

/**
 * Created by Администратор on 09.12.2017.
 */
public class StaticResourceContainer extends ResourceContainer<Resource> {
    private int total;

    public StaticResourceContainer() {
        setWood(new Resource(Resource.ResourceType.WOOD, 0));
        setClay(new Resource(Resource.ResourceType.CLAY, 0));
        setIron(new Resource(Resource.ResourceType.IRON, 0));
        setCrop(new Resource(Resource.ResourceType.CROP, 0));
    }

    public StaticResourceContainer(int wood, int clay, int iron, int crop) {
        setWood(new Resource(Resource.ResourceType.WOOD, wood));
        setClay(new Resource(Resource.ResourceType.CLAY, clay));
        setIron(new Resource(Resource.ResourceType.IRON, iron));
        setCrop(new Resource(Resource.ResourceType.CROP, crop));
    }

    private void updateStoredResourcesAmount() {
        total = (getWood() != null ? getWood().getAmount() : 0) + (getClay() != null ? getClay().getAmount() : 0)
                + (getIron() != null ? getIron().getAmount() : 0) + (getCrop() != null ? getCrop().getAmount() : 0);
    }

    @Override
    public void setWood(Resource wood) {
        super.setWood(wood);
        updateStoredResourcesAmount();
    }

    @Override
    public void setClay(Resource clay) {
        super.setClay(clay);
        updateStoredResourcesAmount();
    }

    @Override
    public void setIron(Resource iron) {
        super.setIron(iron);
        updateStoredResourcesAmount();
    }

    @Override
    public void setCrop(Resource crop) {
        super.setCrop(crop);
        updateStoredResourcesAmount();
    }

    public int getTotal() {
        return total;
    }

}
