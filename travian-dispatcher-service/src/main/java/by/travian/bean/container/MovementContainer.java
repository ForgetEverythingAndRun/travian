package by.travian.bean.container;

import by.travian.bean.movements.MoveEvent;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;


@Data
@NoArgsConstructor
public class MovementContainer {
    public final Logger logger = LoggerFactory.getLogger(MovementContainer.class);

    private Map<String, Deque<MoveEvent>> map = new ConcurrentHashMap<>();

    public MovementContainer(Map<String, Deque<MoveEvent>> map) {
        this.map = map;
    }

    public boolean put(String villageId, MoveEvent event) {
        boolean isAdded = false;
        if (event != null && !villageId.isBlank()) {
            if (!map.containsKey(villageId)) {
                Deque<MoveEvent> deque = new ConcurrentLinkedDeque<MoveEvent>();
                map.put(villageId, deque);
            }
            Deque<MoveEvent> deque = map.get(villageId);
            if (!deque.contains(event)) {
                deque.add(event);
                isAdded = true;
                logger.debug("New attack wave is detected! Arrival time: ", event.getDetails());
            }
            cleanOutdatedEvents(deque);
        }
        return isAdded;
    }

    public Deque<MoveEvent> get(String villageId) {
        Deque<MoveEvent> events = map.get(villageId);
        cleanOutdatedEvents(events);
        return events;
    }

    private void cleanOutdatedEvents(Deque<MoveEvent> deque) {
        if (deque.size() == 0) {
            return;
        }
        boolean delete = false;
        long difference;
        int removed = 0;
        Date now = new Date();
        do {
            MoveEvent e = deque.getLast();
            difference = now.getTime() - e.getTimestamp();
            delete = difference > 0;
            if (delete) {
                deque.removeLast();
                removed++;
            }
        } while (delete = difference > 0);

        if (delete) {
            logger.debug("Removed: " + removed + " Size of the queue: ", deque.size());
        }
    }

}
