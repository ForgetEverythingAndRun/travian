package by.travian.bean.container;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

@NoArgsConstructor
@AllArgsConstructor
public class TravianPlusContainer {
    private AtomicLong timestamp = new AtomicLong(0);

    public boolean isTravianPlusActive() {
        return timestamp.get() - System.currentTimeMillis() > TimeUnit.SECONDS.toMillis(1);
    }

    public long getTimestamp() {
        return timestamp.get();
    }

    public void setTimestamp(long timestamp) {
        if (timestamp < 0) {
            this.timestamp.getAndSet(0);
        } else {
            this.timestamp.getAndSet(timestamp);
        }
    }
}
