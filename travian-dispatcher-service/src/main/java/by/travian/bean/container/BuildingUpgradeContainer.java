package by.travian.bean.container;

import by.travian.bean.Nation;
import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.building.Slot;
import by.travian.error.TravianException;
import io.vavr.Tuple3;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class BuildingUpgradeContainer {
    private AtomicLong buildingQueueTimeWait = new AtomicLong(0);
    private List<Tuple3<Integer, Integer, Integer>> buildings = new ArrayList<>();

    public long getBuildingQueueTimeWait() {
        return buildingQueueTimeWait.get();
    }

    //@param2 responseParser.villageBuildingItemsInQueue(response.getBody());
    public synchronized void setBuildingQueueTimeWait(long buildingQueueTimeWait, JSONArray jsonArray) throws TravianException {
        this.buildingQueueTimeWait.getAndSet(buildingQueueTimeWait);
        buildings = new ArrayList<>();
        if (jsonArray != null) {
            int buildingsInQueue = jsonArray.length();
            if (buildingsInQueue > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        int level = obj.getInt("stufe");
                        int buildingId = obj.getInt("gid");
                        int slot = obj.getInt("aid");
                        Tuple3<Integer, Integer, Integer> tuple3 = new Tuple3<>(buildingId, level, slot);
                        buildings.add(tuple3);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
