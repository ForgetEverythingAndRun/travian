package by.travian.bean.container;

import by.travian.bean.resource.Resource;

/**
 * Created by Администратор on 29.10.2017.
 */
public class ResourceContainer<T extends Resource> {
    private T wood;
    private T clay;
    private T iron;
    private T crop;

    public T getWood() {
        return wood;
    }

    public void setWood(T wood) {
        this.wood = wood;
    }

    public T getClay() {
        return clay;
    }

    public void setClay(T clay) {
        this.clay = clay;
    }

    public T getIron() {
        return iron;
    }

    public void setIron(T iron) {
        this.iron = iron;
    }

    public T getCrop() {
        return crop;
    }

    public void setCrop(T crop) {
        this.crop = crop;
    }

    @Override
    public String toString() {
        return "Resources{" +
                "wood = " + wood +
                ", clay = " + clay +
                ", iron = " + iron +
                ", crop = " + crop +
                '}';
    }
}
