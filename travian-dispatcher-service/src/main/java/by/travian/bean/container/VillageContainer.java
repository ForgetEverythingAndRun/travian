package by.travian.bean.container;

import by.travian.bean.entity.village.Village;

import java.util.LinkedHashSet;
import java.util.Set;

public class VillageContainer extends LinkedHashSet<Village> {

    public Village getVillageById(String villageId) {
        if (villageId != null) {
            for (Village village : this) {
                if (villageId.equals(village.getVillageId())) {
                    return village;
                }
            }
        }
        return null;
    }

    public Set<Village> getVillages() {
        return this;
    }

    public Village getVillageByXY(int x, int y) {
        for (Village village : this) {
            if (x == village.getLocation().getX() && y == village.getLocation().getY()) {
                return village;
            }
        }
        return null;
    }
}
