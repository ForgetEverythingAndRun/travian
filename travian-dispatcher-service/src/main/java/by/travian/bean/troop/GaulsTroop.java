package by.travian.bean.troop;

import by.travian.bean.Nation;

import java.util.List;

/**
 * Created by Администратор on 01.10.2017.
 */
public enum GaulsTroop implements NationTroop {
    PHALANX(RequestTroopAlias.T1, UnitBuilding.getInfantryBuildingId()),

    SWORDSMEN(RequestTroopAlias.T2, UnitBuilding.getInfantryBuildingId()),

    PATHFINDER_SCOUT(RequestTroopAlias.T3, UnitBuilding.getCavalryBuildingId()),

    THUNDER(RequestTroopAlias.T4, UnitBuilding.getCavalryBuildingId()),

    DRUID(RequestTroopAlias.T5, UnitBuilding.getCavalryBuildingId()),

    HAEDUAN(RequestTroopAlias.T6, UnitBuilding.getCavalryBuildingId()),

    RAM(RequestTroopAlias.T7, UnitBuilding.getMachineBuildingId()),

    CATAPULT(RequestTroopAlias.T8, UnitBuilding.getMachineBuildingId()),

    CHIEFTAIN(RequestTroopAlias.T9, UnitBuilding.getSettlerAndInvaderBuildingId()),

    SETTLER(RequestTroopAlias.T10, UnitBuilding.getSettlerAndInvaderBuildingId());

    private RequestTroopAlias alias;
    private List<String> unitCreationBuildingIds;

    GaulsTroop(RequestTroopAlias alias, List<String> unitCreationBuildingIds) {
        this.alias = alias;
        this.unitCreationBuildingIds = unitCreationBuildingIds;
    }

    public RequestTroopAlias getAlias() {
        return alias;
    }

    public void setAlias(RequestTroopAlias alias) {
        this.alias = alias;
    }

    @Override
    public List<String> getUnitBuildingIds() {
        return unitCreationBuildingIds;
    }

    public void setUnitCreationBuildingIds(List<String> unitCreationBuildingIds) {
        this.unitCreationBuildingIds = unitCreationBuildingIds;
    }

    public Nation getNation() {
        return Nation.GAULS;
    }

}
