package by.travian.bean.troop;

import by.travian.bean.Nation;

public interface NationTroop extends TroopAlias, UnitBuilding {
    Nation getNation();
}
