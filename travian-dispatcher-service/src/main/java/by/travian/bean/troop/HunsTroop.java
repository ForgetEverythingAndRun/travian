package by.travian.bean.troop;

import by.travian.bean.Nation;

import java.util.List;


public enum HunsTroop implements NationTroop {

    MERCENARY(TroopAlias.RequestTroopAlias.T1, UnitBuilding.getInfantryBuildingId()),

    BOWMAN(TroopAlias.RequestTroopAlias.T2, UnitBuilding.getInfantryBuildingId()),

    SPOTTER_SCOUT(TroopAlias.RequestTroopAlias.T3, UnitBuilding.getCavalryBuildingId()),

    STEPPE_RIDER(TroopAlias.RequestTroopAlias.T4, UnitBuilding.getCavalryBuildingId()),

    MARKSMAN(TroopAlias.RequestTroopAlias.T5, UnitBuilding.getCavalryBuildingId()),

    MARAUDER(TroopAlias.RequestTroopAlias.T6, UnitBuilding.getCavalryBuildingId()),

    RAM(TroopAlias.RequestTroopAlias.T7, UnitBuilding.getMachineBuildingId()),

    CATAPULT(TroopAlias.RequestTroopAlias.T8, UnitBuilding.getMachineBuildingId()),

    LOGADES(TroopAlias.RequestTroopAlias.T9, UnitBuilding.getSettlerAndInvaderBuildingId()),

    SETTLER(TroopAlias.RequestTroopAlias.T10, UnitBuilding.getSettlerAndInvaderBuildingId());

    private TroopAlias.RequestTroopAlias alias;

    private List<String> unitCreationBuildingIds;

    HunsTroop(TroopAlias.RequestTroopAlias alias, List<String> unitCreationBuildingIds) {
        this.alias = alias;
        this.unitCreationBuildingIds = unitCreationBuildingIds;
    }

    public Nation getNation() {
        return Nation.HUNS;
    }

    public RequestTroopAlias getAlias() {
        return alias;
    }

    public void setAlias(RequestTroopAlias alias) {
        this.alias = alias;
    }

    @Override
    public List<String> getUnitBuildingIds() {
        return unitCreationBuildingIds;
    }

    public void setUnitCreationBuildingIds(List<String> unitCreationBuildingIds) {
        this.unitCreationBuildingIds = unitCreationBuildingIds;
    }
}
