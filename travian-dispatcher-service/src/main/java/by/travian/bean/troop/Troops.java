package by.travian.bean.troop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
/**
 * Created by Администратор on 28.10.2017.
 */
public class Troops {
    private final static Logger logger = LoggerFactory.getLogger(Troops.class);//Logger.getLogger

    private SpyType spyType;

    protected Map<TroopAlias.RequestTroopAlias, Integer> troops; //TODO: use EnumMap instead of Map! Is enum map threadsafe?

    public Troops() {
        this.spyType = SpyType.RESOURCE;
        this.troops = troopsInit();
    }

    public Troops(Troops troops) {
        this.spyType = troops.getSpyType();
        this.troops = new ConcurrentHashMap<TroopAlias.RequestTroopAlias, Integer>(troops.getTroops());
    }


    private static ConcurrentHashMap<TroopAlias.RequestTroopAlias, Integer> troopsInit() {
        ConcurrentHashMap<TroopAlias.RequestTroopAlias, Integer> troopsList = new ConcurrentHashMap<TroopAlias.RequestTroopAlias, Integer>();
        troopsList.put(TroopAlias.RequestTroopAlias.T1, 0);
        troopsList.put(TroopAlias.RequestTroopAlias.T2, 0);
        troopsList.put(TroopAlias.RequestTroopAlias.T3, 0);
        troopsList.put(TroopAlias.RequestTroopAlias.T4, 0);
        troopsList.put(TroopAlias.RequestTroopAlias.T5, 0);
        troopsList.put(TroopAlias.RequestTroopAlias.T6, 0);
        troopsList.put(TroopAlias.RequestTroopAlias.T7, 0);
        troopsList.put(TroopAlias.RequestTroopAlias.T8, 0);
        troopsList.put(TroopAlias.RequestTroopAlias.T9, 0);
        troopsList.put(TroopAlias.RequestTroopAlias.T10, 0);
        troopsList.put(TroopAlias.RequestTroopAlias.T11, 0);//hero
        return troopsList;
    }

    public synchronized boolean hasEnoughTroops(Troops requiredTroops){
        Map<TroopAlias.RequestTroopAlias, Integer> map = requiredTroops.getTroops();
        for (Map.Entry<TroopAlias.RequestTroopAlias, Integer> entry : map.entrySet()) {
            if (troops.get(entry.getKey()) < entry.getValue()) {
                logger.info("Troops are not enoght(" + entry.getKey() + ")");
                return false;
            }
        }
        return true;
    }

    public Troops updateByType(GaulsTroop type, int count) {
        troops.put(type.getAlias(), count);
        return this;
    }

    //TODO: ROME troop type method & so on
    public Troops updateByAlias(TroopAlias.RequestTroopAlias alias, int count) {
        troops.put(alias, count);
        return this;
    }

    public Map<TroopAlias.RequestTroopAlias, Integer> getTroops() {
        return troops;
    }

    public void setTroops(Map<TroopAlias.RequestTroopAlias, Integer> troops) {
        this.troops = troops;
    }

    public int countByType(GaulsTroop type) {
        return troops.get(type.getAlias());
    }

    public int countByAlias(TroopAlias.RequestTroopAlias alias) {
        return troops.get(alias);
    }

    public SpyType getSpyType() {
        return spyType;
    }

    public void setSpyType(SpyType spyType) {
        this.spyType = spyType;
    }

    @Override
    public String toString() {
        return "Troops{" +
                "troops=" + troops +
                '}';
    }
}
