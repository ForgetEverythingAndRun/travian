package by.travian.bean.troop;

import by.travian.bean.Nation;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Администратор on 04.10.2017.
 */
public interface UnitBuilding {
    //see buildings_en_US.properties
    List<String> getUnitBuildingIds();

    static List<String> getInfantryBuildingId() {
        return Arrays.asList("19");
    }


    static List<String> getCavalryBuildingId() {
        return Arrays.asList("20");
    }


    static List<String> getMachineBuildingId() {
        return Arrays.asList("21");
    }


    static List<String> getSettlerAndInvaderBuildingId() {
        return Arrays.asList("25", "26");
    }


}
