package by.travian.bean.troop;

/**
 * Created by Администратор on 04.10.2017.
 */
public enum OffenseType {
    RAID("4"), FORCE("3");//, SPY()
    private String value;

    OffenseType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
