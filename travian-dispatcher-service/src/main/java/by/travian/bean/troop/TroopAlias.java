package by.travian.bean.troop;

public interface TroopAlias {
    RequestTroopAlias getAlias();

    enum RequestTroopAlias {
        T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11;
        //HERO =, T11
        //TRAPS = ,T99

        public static RequestTroopAlias createAliasByString(String alias) {
            return RequestTroopAlias.valueOf(alias.toUpperCase());
        }

        public static RequestTroopAlias getHeroAlias() {
            return RequestTroopAlias.T11;
        }

        @Override
        public String toString() {
            return this.name().toLowerCase();
        }

    }

}
