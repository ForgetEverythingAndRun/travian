package by.travian.bean.troop;

/**
 * Created by Администратор on 07.10.2017.
 */
public enum SpyType {
    RESOURCE(1,"Explore enemy resources and troops"),
    DEFENSE(2,"Explore troops and defenses");

    private int type;
    private String message;

    SpyType(int type, String message) {
        this.type = type;
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return type+"";
    }
}
