package by.travian.bean.entity.building;

import java.util.ResourceBundle;

public class SlotUtils {

    private static final int RESOURCE_SLOTS_COUNT = 18;
    private static final int MAIN_BUILDING_SLOT_ID = 26;
    private static final int RALLY_POINT_SLOT_ID = 39;
    private static final int WALL_SLOT_ID = 40;
    private static final int EMPTY_SLOT_ID = 0;
    private static final ResourceBundle bundle = ResourceBundle.getBundle("slot_bindings");

//    public boolean isResourceSlot() {
//        return id <= RESOURCE_SLOTS_COUNT;
//    }
//
//    public boolean isBindedSlot() {
//        return isResourceSlot() || isMainBuildingSlot() || isRallyPointBuildingSlot() || isWallBuildingSlot();
//    }
//
//    public boolean isSupportedBuilding(Building building) {
//        if (bundle.containsKey(building.getBuildingId())) {
//            return Integer.valueOf(bundle.getString(building.getBuildingId())).equals(id);
//        }
//
//        return !isBindedSlot();
//    }
//
//    public boolean isEmpty() {
//        return id == EMPTY_SLOT_ID;
//    }
//
//    public boolean isMainBuildingSlot() {
//        return id == MAIN_BUILDING_SLOT_ID;
//    }
//
//    public boolean isRallyPointBuildingSlot() {
//        return id == RALLY_POINT_SLOT_ID;
//    }
//
//    public boolean isWallBuildingSlot() {
//        return id == WALL_SLOT_ID;
//    }
}
