package by.travian.bean.entity.building;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "building")
@XmlType(name = "building")
@XmlAccessorType(XmlAccessType.FIELD)
public class Entry {

    @XmlAttribute
    private Integer id;
    private Levels levels;
}
