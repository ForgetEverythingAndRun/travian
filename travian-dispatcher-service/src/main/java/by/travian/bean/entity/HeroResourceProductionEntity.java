package by.travian.bean.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HeroResourceProductionEntity implements Entity {
    private String resource = "0"; // [0,5]
    private String attackBehaviour = "hide";// fight/hide

}
