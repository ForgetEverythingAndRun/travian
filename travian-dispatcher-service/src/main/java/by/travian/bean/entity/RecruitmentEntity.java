package by.travian.bean.entity;

import by.travian.bean.Nation;
import by.travian.bean.troop.TroopAlias;
import lombok.*;

@Data
@Builder(builderMethodName = "internalBuilder",toBuilder = true)
public class RecruitmentEntity implements Entity {
    @NonNull
    private String villageId;
    private String slotId;
    /**
     * building's slotId where units will be created
     */
    private Integer buildingId;
    @NonNull
    private TroopAlias.RequestTroopAlias alias;
    @NonNull
    private Nation nation;
    @NonNull
    private Integer buyCount;

    @Override
    public String getVillageId() {
        return villageId;
    }

    public static RecruitmentEntityBuilder builder(String villageId, TroopAlias.RequestTroopAlias alias, Nation nation, Integer buyCount) {
        return internalBuilder().villageId(villageId).alias(alias).nation(nation).buyCount(buyCount);
    }

    public static void main(String[] args) {
        RecruitmentEntity entity = RecruitmentEntity.builder("test", TroopAlias.RequestTroopAlias.T4, Nation.GAULS, 1).build();
        System.out.println(entity.toBuilder().buyCount(6).buildingId(666).slotId("999"));

    }
    //Sample: RecruitmentEntity.builder("test", TroopAlias.RequestTroopAlias.T4, Nation.GAULS, 1).build()

}
