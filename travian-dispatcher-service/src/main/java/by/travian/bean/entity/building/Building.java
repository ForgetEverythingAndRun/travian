package by.travian.bean.entity.building;

import by.travian.bean.entity.Entity;
import by.travian.request.TravianRequest;
import by.travian.util.JaxbParser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Map;

@Data
@ToString
@XmlRootElement(name = "building")
@XmlAccessorType(XmlAccessType.FIELD)
public class Building { //implements Entity {

    @XmlAttribute
    private Integer id; //required param for requests
    @XmlTransient
    private String name; //optional
    @XmlTransient
    private Level level; //optional
    private boolean isNewCenterBuilding = false;

    public Building(Integer id, String name, Level level, boolean isNewCenterBuilding) {
        this.id = id;
        this.name = name;
        this.level = level;
        this.isNewCenterBuilding = isNewCenterBuilding;
    }

    public Building(Integer id) {
        this.id = id;
        this.level = Level.createZeroLvl();
    }

    public Building() {
        this.level = Level.createZeroLvl();
    }

    public Building(Building b, boolean isNewCenterBuilding) {
        this.id = b.id;
        this.name = b.getName();
        this.level = b.getLevel();
        this.isNewCenterBuilding = isNewCenterBuilding;
    }

    public Building(Building b) {
        this.id = b.id;
        this.name = b.getName();
        this.level = b.getLevel();
        this.isNewCenterBuilding = b.isNewCenterBuilding();
    }

    public boolean isResourceBuilding() {
        return id > 0 && id < 5;
    }

    public static boolean isResourceBuilding(int id) {
        return id > 0 && id < 5;
    }

    public enum BuildingType {
        RESOURCE, CENTER;

        public BuildingType getBuildingTypeById(int id) {
            BuildingType type = CENTER;
            if (id > 0 && id < 5) {
                type = RESOURCE;
            }
            return type;
        }

        public BuildingType getBuildingTypeBySlotId(int id) {
            BuildingType type = null;
            if (id > 18 && id < 41) {
                type = CENTER;
            } else if (id > 0 && id < 19) {
                type = RESOURCE;
            }
            return type;
        }
    }
}