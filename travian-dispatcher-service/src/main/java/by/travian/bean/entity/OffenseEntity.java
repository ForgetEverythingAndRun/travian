package by.travian.bean.entity;

import by.travian.bean.Nation;
import by.travian.bean.entity.village.Coordinates;
import by.travian.bean.troop.OffenseType;
import by.travian.bean.troop.TroopAlias;
import by.travian.bean.troop.Troops;
import by.travian.configuration.TravianConfiguration;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder(builderMethodName = "internalBuilder", toBuilder = true)
public class OffenseEntity implements Entity {
    @NonNull
    private String villageId;
    @NonNull
    private Coordinates target;
    @NonNull
    private OffenseType offenseType;
    @NonNull
    private Troops troopsContainer;
    @NonNull
    private Nation nation;

    private String timestamp;
    private String checksum;//timestampchecksum
    private String a;
    private String kid;

    public static OffenseEntity.OffenseEntityBuilder builder(String villageId, Coordinates target, OffenseType offenseType, Troops troopsContainer, Nation nation) {
        return internalBuilder().villageId(villageId).target(target).offenseType(offenseType).troopsContainer(troopsContainer).nation(nation);
    }

    public boolean isSpySent() {
        return troopsContainer.getTroops().get(nation.getNationSpyAlias()) > 0;
    }

    public boolean isHeroSent() {
        return troopsContainer.getTroops().get(TroopAlias.RequestTroopAlias.T11) > 0;
    }

    @Override
    public String getVillageId() {
        return villageId;
    }
}
