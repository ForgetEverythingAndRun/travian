package by.travian.bean.entity.village;

public class Coordinates {
    private int x;
    private int y;

    public Coordinates(Coordinates villageLocation) {
        this.x = villageLocation.x;
        this.y = villageLocation.y;
    }


    public Coordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coordinates target = (Coordinates) o;

        if (x != target.x) return false;
        return y == target.y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    @Override
    public String toString() {
        return "VillageLocation{" +
                "x='" + x + '\'' +
                ", y='" + y + '\'' +
                '}';
    }
}

