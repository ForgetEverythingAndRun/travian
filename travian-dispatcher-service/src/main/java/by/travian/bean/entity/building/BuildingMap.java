package by.travian.bean.entity.building;

import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ToString
@XmlRootElement(name = "buildings")
@XmlType(name = "buildings")
@XmlAccessorType(XmlAccessType.FIELD)
public class BuildingMap {

    @XmlTransient
    private Map<Integer, List<Level>> map = new HashMap<>();

    @ToString.Exclude
    @XmlElement(name = "building")
    private List<Entry> entries = new ArrayList<>();

    public Map<Integer, List<Level>> getMap() {
        entries.forEach(x -> map.put(x.getId(), x.getLevels().getLevels()));
        return map;
    }
}