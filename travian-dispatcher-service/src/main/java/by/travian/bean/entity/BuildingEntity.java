package by.travian.bean.entity;

import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.building.Slot;
import by.travian.bean.entity.village.Village;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class BuildingEntity implements Entity {
    private Integer id;
    private Integer slotId;
    private String villageId;
    private Integer untilLvlRepeat;
    private boolean isNewCenterBuilding;

    public BuildingEntity(Building building, Slot slot, String villageId) {
        if (building != null) {
            this.id = building.getId();
            this.isNewCenterBuilding = building.isNewCenterBuilding();
        }
        this.slotId = slot.getId();
        this.villageId = villageId;
    }

    @Override
    public String getVillageId() {
        return villageId;
    }
}
