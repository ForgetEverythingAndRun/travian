package by.travian.bean.entity.building;

public class BuildingTabs {
    //TODO: load ids from file by name

    private final static int RESIDENCE_BUILDING = 25;
    private final static int PALACE_BUILDING = 26;

    private final static int RALLY_POINT_BUILDING = 16;

    public interface BuildingTabValue {
        int getValue();

        String getTabQueryParamKey();
    }

    public static BuildingTabValue createManagementTabByCenterBuildingId(int id) {
        if (id == RESIDENCE_BUILDING || id == PALACE_BUILDING) {
            return SettlerTab.MANAGEMENT;
        } else if (id == RALLY_POINT_BUILDING) {
            return RallyPointTab.MANAGEMENT;
        }
        return BuildingTabs.DefaultTab.MANAGEMENT;
    }

    public enum DefaultTab implements BuildingTabValue {
        MANAGEMENT(0);

        protected int value;

        DefaultTab(int value) {
            this.value = value;
        }

        @Override
        public int getValue() {
            return value;
        }


        @Override
        public String getTabQueryParamKey() {
            return "t";
        }
    }

    public enum SettlerTab implements BuildingTabValue {
        MANAGEMENT(0), BUY(1);

        protected int value;

        SettlerTab(int value) {
            this.value = value;
        }

        @Override
        public int getValue() {
            return value;
        }

        @Override
        public String getTabQueryParamKey() {
            return "s";
        }
    }

    public enum MarketTab implements BuildingTabValue {
        MANAGEMENT(0), SEND(5), BUY(1), SELL(2);

        private int value;

        MarketTab(int value) {
            this.value = value;
        }

        @Override
        public int getValue() {
            return value;
        }

        @Override
        public String getTabQueryParamKey() {
            return "t";
        }
    }

    public enum RallyPointTab implements BuildingTabValue {
        MANAGEMENT(0), VIEW(1), SEND(2);

        private int value;

        @Override
        public int getValue() {
            return value;
        }

        @Override
        public String getTabQueryParamKey() {
            return "tt";
        }

        RallyPointTab(int value) {
            this.value = value;
        }
    }
}
