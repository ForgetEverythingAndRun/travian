package by.travian.bean.entity.building;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "level")
@XmlAccessorType(XmlAccessType.FIELD)
public class Level {

    @XmlAttribute
    private Integer number;
    private Integer wood;
    private Integer clay;
    private Integer iron;
    private Integer crop;

    public static Level createZeroLvl(){
        return new Level(0,0,0,0,0);
    }
}
