package by.travian.bean.entity.village;

import by.travian.bean.Nation;
import by.travian.bean.container.BuildingSlotContainer;
import by.travian.bean.container.BuildingUpgradeContainer;
import by.travian.bean.entity.Entity;
import by.travian.bean.container.VillageResourceContainer;
import by.travian.bean.entity.MarketEntity;
import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.building.Slot;
import by.travian.bean.resource.VillageResource;
import by.travian.bean.troop.Troops;
import io.vavr.Tuple3;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

@Data
@Builder(builderMethodName = "internalBuilder", toBuilder = true)
public class Village implements Entity {
    @NonNull
    private String villageId;
    @NonNull
    private Coordinates location;
    @NonNull
    private Nation nation;

    @Builder.Default
    private String dname = "";

    @NonNull
    private VillageResourceContainer resources;
    @NonNull
    private Troops troops;
    @NonNull
    private BuildingSlotContainer buildingSlotContainer;
    @NonNull
    private BuildingUpgradeContainer buildingUpgradeContainer;


    public static Village.VillageBuilder builder(String villageId, Coordinates location, Nation nation) {
        return internalBuilder().villageId(villageId).location(location).nation(nation)
                .resources(new VillageResourceContainer())
                .troops(new Troops())
                .buildingSlotContainer(new BuildingSlotContainer())
                .buildingUpgradeContainer(new BuildingUpgradeContainer());
    }

    public String getVillageId() {
        return villageId;
    }

    public void setVillageId(String villageId) {
        this.villageId = villageId;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public Coordinates getLocation() {
        return location;
    }

    public void setLocation(Coordinates location) {
        this.location = location;
    }

    public VillageResourceContainer getResources() {
        return resources;
    }

    public List<VillageResource> getResourceList() {
        return Arrays.asList(resources.getWood(), resources.getClay(), resources.getIron(), resources.getCrop());
    }

    public void setResources(VillageResourceContainer resources) {
        if (this.resources != null) {
            this.resources.updateResources(resources);
        } else {
            this.resources = resources;
        }
    }

    public Troops getTroops() {
        return troops;
    }

    public void setTroops(Troops troops) {
        this.troops = troops;
    }

    public BuildingSlotContainer getBuildingSlotContainer() {
        return buildingSlotContainer;
    }

    public void setBuildingSlotContainer(BuildingSlotContainer buildingSlotContainer) {
        this.buildingSlotContainer = buildingSlotContainer;
    }

    public long getBuildingQueueTimeWait() {
        return buildingUpgradeContainer.getBuildingQueueTimeWait();
    }

    public void setBuildingQueueTimeWait(long buildingQueueTimeWait) {
        buildingUpgradeContainer.setBuildingQueueTimeWait(buildingQueueTimeWait, null);
    }

    @Deprecated
    public static List<String> getWallIds() {
        /*
        31 = Городская стена    //# Рим
        32 = Земляной вал       //# Немцы
        33 = Изгородь           //# Галлы
        42 = Каменная Ограда    //# Египет*/
        return Arrays.asList("31", "32", "33", "42");
    }

    @Override
    public String toString() {
        return "{villageId='" + villageId + '\'' +
                ", dname='" + dname + '\'' +
                ", location={x='" + location.getX() + "' y='" + location.getY() + "'}" +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Village village = (Village) o;
        return villageId.equals(village.villageId) && location.equals(village.location) && nation == village.nation && Objects.equals(dname, village.dname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(villageId, location, nation, dname);
    }
}

