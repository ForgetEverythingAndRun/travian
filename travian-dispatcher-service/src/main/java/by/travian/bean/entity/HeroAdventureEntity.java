package by.travian.bean.entity;

import by.travian.bean.entity.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Comparator;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HeroAdventureEntity implements Entity {
    private long time; // required to one side(as timestamp time)
    private String kid;
    private int difficulty; //hard = 0, normal > 0 (1,2,3)

}
