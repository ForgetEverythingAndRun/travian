package by.travian.bean.entity;

import java.io.Serializable;

public interface Entity extends Serializable {
    /**
     * It's required for village context dependent tasks
     * @return villageId
     */
    default String getVillageId() {
        return null;
    };

}
