package by.travian.bean.entity;

import by.travian.bean.Nation;
import by.travian.bean.entity.village.Coordinates;
import by.travian.bean.troop.TroopAlias;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;


@Data
@Builder(builderMethodName = "internalBuilder", toBuilder = true)
public class MarketEntity implements Entity {
    @NonNull
    private String villageId;// (village from) = a
    private String c;
    private String kid;
    private String sz;
    private int slotId; // id
    @Builder.Default
    private int x2 = 1;//check travian plus
    @Builder.Default
    private String dname = "";
    @NonNull
    private Coordinates target;
    private int wood;
    private int clay;
    private int iron;
    private int crop;

    public static MarketEntity.MarketEntityBuilder builder(String villageId, Coordinates target) {
        return internalBuilder().villageId(villageId).target(target);
    }

    @Override
    public String toString() {
        return "{" +
                "villageIdFrom='" + villageId + '\'' +
                ", dname='" + dname + '\'' +
                ", target=" + target +
                ", wood=" + wood +
                ", clay=" + clay +
                ", iron=" + iron +
                ", crop=" + crop +
                '}';
    }

    @Override
    public String getVillageId() {
        return villageId;
    }
}