package by.travian.bean.task.status;

public enum State {
    NOT_STARTED, STARTED, FAILED, COMPLETED;

}
