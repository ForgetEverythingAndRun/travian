package by.travian.bean.task.manager;

import by.travian.bean.task.Task;
import by.travian.bean.task.consumer.TaskConsumer;
import by.travian.bean.task.handler.ContextHandler;
import by.travian.bean.task.producer.TaskProducer;

import java.util.List;
import java.util.concurrent.BlockingQueue;

public interface TaskManager<T extends Task> {
    BlockingQueue<T> getQueue();

    TaskProducer<T> taskProducer(T task);

    TaskConsumer<T> taskConsumer();

    ContextHandler getContextHandler();

    void execute(List<T> taskList);

    //List<T> getTaskList();
}
