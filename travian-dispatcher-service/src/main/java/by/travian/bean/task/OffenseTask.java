package by.travian.bean.task;

import by.travian.bean.entity.OffenseEntity;

import java.util.concurrent.atomic.AtomicInteger;

public class OffenseTask extends Task<OffenseEntity> {

    public OffenseTask(long initialDelay, AtomicInteger repeat, long repeatTimeWait, OffenseEntity entity) {
        super(initialDelay, repeat, repeatTimeWait, entity);
    }

    @Override
    public OffenseEntity getEntity() {
        return super.getEntity();
    }
}
