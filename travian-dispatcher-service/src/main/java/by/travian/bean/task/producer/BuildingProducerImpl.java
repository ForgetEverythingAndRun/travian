package by.travian.bean.task.producer;

import by.travian.bean.container.StaticResourceContainer;
import by.travian.bean.container.VillageResourceContainer;
import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.building.Level;
import by.travian.bean.entity.building.Slot;
import by.travian.bean.entity.village.Village;
import by.travian.bean.resource.VillageResource;
import by.travian.bean.task.Task;
import by.travian.bean.task.handler.Context;
import by.travian.bean.task.handler.ContextHandler;
import by.travian.error.TravianException;
import by.travian.service.TravianService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class BuildingProducerImpl extends TaskProducerImpl<BuildingEntity> {
    public final static Logger logger = LoggerFactory.getLogger(BuildingProducerImpl.class);
    private TravianService service;

    public BuildingProducerImpl(ContextHandler contextHandler, BlockingQueue<Task<BuildingEntity>> queue, List<Task<BuildingEntity>> tasks, TravianService service) {
        super(contextHandler, queue, tasks);
        this.service = service;
    }


    @Override
    public void produce(Task<BuildingEntity> task) {
        logger.trace("Call handleContextAccessAndRun for " + task.getEntity().getVillageId() + " " + task.getEntity());
        contextHandler.handleContextAccessAndRun(task.getEntity().getVillageId(), () -> {
            BuildingEntity entity = task.getEntity();

            Village village = service.getVillageContainerManager().getVillageById(entity.getVillageId());

            if (village == null) {
                logger.error("Can't find village with id: " + entity.getVillageId());
                return;
            }

            Building building = null;
            try {
                building = verifyAndUpdateBuildingEntity(village, task.getEntity()).getValue();
            } catch (IllegalArgumentException e) {
                logger.error("", e);
                return;
            }

            task.setCurrentPhase(Task.ExecutionPhase.STARTED);
            boolean processed = verifyCompletion(village, task);
            if (processed) {
                logger.trace("Skip. VillageId: " + village + " Already upgraded: " + building.getLevel().getNumber() + " of " + task.getEntity().getUntilLvlRepeat() + " slot: " + task.getEntity().getSlotId() + " building: " + building);
            }

            boolean isFailed = false;
            while (!processed && !isFailed) {
                try {
                    verifyAndWaitBuildingDashboardQueue(village, task);
                    calculateDeficitResourceProduceTimeAndWait(village, task);
                    synchronized (task.getLock()) {
                        produce(getQueue(), task);
                        Context context = contextHandler.getContext(village.getVillageId());
                        context.getSemaphore().release();
                        task.getLock().wait();// until consumed & executed
                        context.getSemaphore().acquire();
                    }
                    processed = verifyCompletion(village, task);
                    isFailed = task.getCurrentPhase() == Task.ExecutionPhase.ERROR;
                } catch (InterruptedException | TravianException e) {
                    logger.error("", e);
                    isFailed = true;
                }
            }
            if (isFailed) {
                task.setCurrentPhase(Task.ExecutionPhase.ERROR);
                logger.error("Error. VillageId: " + village + " Skipped: " + task.getEntity());
            } else if (processed) {
                task.setCurrentPhase(Task.ExecutionPhase.COMPLETE);
            }
        });
    }

    protected Map.Entry<Slot, Building> verifyAndUpdateBuildingEntity(Village village, BuildingEntity entity) throws IllegalArgumentException {
        final List<Level> levels = service.getBuildingLevelMap().get(entity.getId());
        if (entity.getUntilLvlRepeat() > levels.size()) {
            throw new IllegalArgumentException("Incorrect task's entity. Check 'untilLvlRepeat' max level " + entity);
        }
        Slot slot = new Slot(entity.getSlotId());
        Building building = new Building(entity.getId());

        Map.Entry<Slot, Building> findActualBuildingSlot = village.getBuildingSlotContainer().getBuildingSlotEntry(slot, building);
        if (findActualBuildingSlot == null) {
            throw new IllegalArgumentException("Incorrect task's entity. Check 'buildingId' for corresponding slot: " + entity);
        }
        if (entity.getSlotId() == 0) { // slotId = 0 means that need to find out actual slot value for building
            entity.setSlotId(findActualBuildingSlot.getKey().getId());
        }
        return findActualBuildingSlot;
    }

    protected void verifyAndWaitBuildingDashboardQueue(Village village, Task<BuildingEntity> task) throws InterruptedException {
        Integer buildingId = task.getEntity().getId();
        if (!service.isBuildingQueueReady(village, Building.isResourceBuilding(buildingId))) {
            long wait = village.getBuildingQueueTimeWait() - System.currentTimeMillis();
            if (wait > 0) {
                service.sleepLogMessage(wait, TimeUnit.MILLISECONDS, "Building dashboard is full. VillageId: " + village.getVillageId() + ".  BuildingId: " + buildingId + ".");
                Context context = contextHandler.getContext(village.getVillageId());
                context.getSemaphore().release();
                TimeUnit.MILLISECONDS.sleep(wait);
                context.getSemaphore().acquire();
            }
        } else {
            logger.debug("Building dashboard has free capacity. VillageId: " + village.getVillageId() + ". BuildingId: " + task.getEntity().getId() + ".");
        }
    }

    protected boolean verifyCompletion(Village village, Task<BuildingEntity> task) {
        Building building = village.getBuildingSlotContainer().get(new Slot(task.getEntity().getSlotId()));
        return building != null && building.getLevel().getNumber() >= task.getEntity().getUntilLvlRepeat();
    }

    protected Long getProduceMaxTimeDeficitResource(Level levelCost, VillageResourceContainer container) {
        List<Long> list = Arrays.asList(
                VillageResourceContainer.getRequiredTimeToProduceResource(levelCost.getWood(), container.getWood()),
                VillageResourceContainer.getRequiredTimeToProduceResource(levelCost.getClay(), container.getClay()),
                VillageResourceContainer.getRequiredTimeToProduceResource(levelCost.getIron(), container.getIron()),
                VillageResourceContainer.getRequiredTimeToProduceResource(levelCost.getCrop(), container.getCrop())
        );
        Long resourceProduceMaxWaitTime = list
                .stream()
                .mapToLong(v -> v)
                .max().getAsLong();

        return resourceProduceMaxWaitTime;
    }

    protected boolean isEnoughResourceMaxStorageCapacity(Level levelCost, VillageResourceContainer container) {
        return levelCost.getWood() <= container.getWarehouseCapacity()
                && levelCost.getClay() <= container.getWarehouseCapacity()
                && levelCost.getIron() <= container.getWarehouseCapacity()
                && levelCost.getCrop() <= container.getGranaryCapacity();
    }

    protected void calculateDeficitResourceProduceTimeAndWait(Village village, Task<BuildingEntity> task) throws TravianException {
        Building building = village.getBuildingSlotContainer().get(new Slot(task.getEntity().getSlotId()));
        int nextUpgradeLevel = building != null ? building.getLevel().getNumber() + 1 : 1; // new building has level 1

        final List<Level> levels = service.getBuildingLevelMap().get(task.getEntity().getId());

        Integer maxLevelBound = nextUpgradeLevel <= levels.size() ? nextUpgradeLevel : levels.size();

        Level levelCost = levels.stream()
                .filter(x -> x.getNumber().equals(maxLevelBound)).findFirst()
                .orElseThrow(() -> new TravianException("Can't find resource cost level for buildingId: " + task.getEntity().getId() + " level: " + maxLevelBound));

        if (!isEnoughResourceMaxStorageCapacity(levelCost, village.getResources())) {
            throw new TravianException("Resource capacity isn't enough to execute task. " + village + " Details: " + village.getResources());
        }
        long resourceProduceMaxTimeWait = getProduceMaxTimeDeficitResource(levelCost, village.getResources());

        if (resourceProduceMaxTimeWait > 0) {
            service.sleepLogMessage(resourceProduceMaxTimeWait, TimeUnit.MILLISECONDS, "Village: " + village + " Not enough resources for buildingId: " + task.getEntity().getId() + ".");
            List<VillageResource> resourceList = village.getResourceList();
            List<Integer> requirements = Arrays.asList(levelCost.getWood(), levelCost.getClay(), levelCost.getIron(), levelCost.getCrop());
            int i = 0;
            for (VillageResource resource : resourceList) {
                logger.trace(String.format("Total %s: %d production: %d required: %d", resource.getType().toString(), resource.getAmount(), resource.getProduction(), requirements.get(i++)));
            }

            /*VillageResource wood = village.getResources().getWood();
            VillageResource clay = village.getResources().getClay();
            VillageResource iron = village.getResources().getIron();
            VillageResource crop = village.getResources().getCrop();
            logger.trace("Total Wood: " + wood.getAmount() + " production: " + wood.getProduction() + " required: " + levelCost.getWood());
            logger.trace("Total Clay: " + clay.getAmount() + " production: " + clay.getProduction() + " required: " + levelCost.getClay());
            logger.trace("Total Iron: " + iron.getAmount() + " production: " + iron.getProduction() + " required: " + levelCost.getIron());
            logger.trace("Total Crop: " + crop.getAmount() + " production: " + crop.getProduction() + " required: " + levelCost.getCrop());*/

            //TODO: Refactor this ... !
            //condition.await
            StaticResourceContainer resourcesRequired = new StaticResourceContainer(levelCost.getWood(), levelCost.getClay(), levelCost.getIron(), levelCost.getCrop());
            synchronized (resourcesRequired) {
                try {
                    service.trackResources(village, resourcesRequired);
                    Context context = contextHandler.getContext(village.getVillageId());
                    context.getSemaphore().release();
                    resourcesRequired.wait(resourceProduceMaxTimeWait);
                    context.getSemaphore().acquire();
                } catch (InterruptedException e) {
                    throw new TravianException(e);
                }
            }
        } else {
            logger.info("Village: " + village + " Resources updated. Task added to queue. ");
        }
    }

}
