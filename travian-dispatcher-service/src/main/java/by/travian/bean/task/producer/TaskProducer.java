package by.travian.bean.task.producer;

import by.travian.bean.task.Task;

import java.util.List;
import java.util.concurrent.BlockingQueue;

public interface TaskProducer<T extends Task> extends Runnable {
    List<T> getTasks();

    BlockingQueue<T> getQueue();

    void produce(BlockingQueue<T> queue, T task);

    void produce(T task);

    void produce(List<T> tasks);

}
