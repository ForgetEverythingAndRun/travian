package by.travian.bean.task.handler;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Condition;

public class Context {
    private String villageId;
    private Semaphore semaphore;
    private Condition condition;

    public Context(String villageId, Semaphore semaphore, Condition condition) {
        this.villageId = villageId;
        this.semaphore = semaphore;
        this.condition = condition;
    }

    public synchronized String getVillageId() {
        return villageId;
    }

    public synchronized Semaphore getSemaphore() {
        return semaphore;
    }

    public synchronized Condition getCondition() {
        return condition;
    }

}