package by.travian.bean.task.manager;

import by.travian.bean.entity.MarketEntity;
import by.travian.bean.task.handler.ContextHandler;
import by.travian.common.command.factory.CommandFactory;

public class MarketTaskManagerImpl extends DefaultTaskManager<MarketEntity> {

    public MarketTaskManagerImpl(CommandFactory commandFactory, ContextHandler contextHandler) {
        super(commandFactory.createMarketResourceSendCommand(), contextHandler);
    }

}
