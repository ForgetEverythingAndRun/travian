package by.travian.bean.task.handler;

import by.travian.error.TravianException;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ContextHandler {
    public final static Logger logger = LoggerFactory.getLogger(ContextHandler.class);
    private static final int ACCESS_LIMIT_THREAD = 1;
    private static final int TIMEOUT_SECONDS = 1;
    private static final String CONTEXT_HANDLER_THREAD_NAME = "contextHandler";


    private List<Context> contextList = new CopyOnWriteArrayList<>();
    private AtomicReference<Context> currentContext = new AtomicReference<>(null);
    private Lock lock = new ReentrantLock();

    private boolean isStarted;

    public Lock getLock() {
        return lock;
    }


    //How to update context map in case of lose or getting village?
    public void registerContext(List<String> villageIds) {
        for (String id : villageIds) {
            registerContext(id);
        }
    }

    public void registerContext(String villageId) {
        if (!contextList.stream().map(Context::getVillageId).filter(s -> s.equals(villageId)).findFirst().isPresent()) {
            Semaphore semaphore = new Semaphore(ACCESS_LIMIT_THREAD, true);
            semaphore.drainPermits();
            contextList.add(new Context(villageId, semaphore, lock.newCondition()));
        }
    }

    public Context getContext(String villageId) {
        return contextList.stream().filter(value -> value.getVillageId().equals(villageId)).findFirst().orElse(null);
    }

    public Context getCurrentContext() {
        return currentContext.get();
    }

    /**
     * village was re-taken and villageId should be removed
     */
    public void removeContext(String villageId) {
        Context context = getContext(villageId);
        if (context != null) {
            contextList.remove(context);
            //TODO: add behavior for task threads in case of missing context in list
            Semaphore semaphore = context.getSemaphore();
            int permits = ACCESS_LIMIT_THREAD - semaphore.availablePermits();
            if (permits > 0) {
                semaphore.release(permits);
            }
        }
    }

    public void handleContextAccessAndRun(String villageId, Runnable runnable) {
        if (villageId == null) {
            // there are no dependencies from context e.g. login, hero adventure & etc
            logger.error("Don't use context synchronization when it's unnecessary! ", new TravianException("VillageId is null"));
            //runnable.run();
        } else {
            Context context = getContext(villageId);
            if (context != null) {
                try {
                    context.getSemaphore().acquire();
                    String currentVillageIdContext = getCurrentContext().getVillageId();
                    if (villageId.equals(currentVillageIdContext)) {
                        runnable.run();
                    } else {
                        logger.error("Critical error: ", new TravianException("Wrong context! But should be the same."));
                    }
                } catch (InterruptedException e) {
                    logger.error("Error: ", e);
                } finally {
                    context.getSemaphore().release();
                }
            } else {
                logger.error("Can't find context by villageId: " + villageId);
            }
        }
    }

    public void handle() {
        Runnable runnable = new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                while (true) {
                    Context next = null;
                    for (Context context : contextList) {
                        int length = context.getSemaphore().getQueueLength();
                        if (length == 0) {
                            continue; //skip villages which hasn't any tasks in queue
                        } else {
                            System.out.println(new Date() + " " + Thread.currentThread().getName() + " Context(" + context.getVillageId() + ") queue length: " + length);
                        }
                        next = context;
                        Context current = currentContext.get();
                        if (current != null) {
                            System.out.println(new Date() + " " + Thread.currentThread().getName() + " Context(" + current.getVillageId() + ") queue length: " + current.getSemaphore().getQueueLength() + " Current");
                            if (current.equals(next)) {
                                continue;  //choose next village which will be processed next and skip if it's active one
                            }
                            try {
                                current.getSemaphore().acquire(ACCESS_LIMIT_THREAD);//wait till village has no active tasks in queue and freeze queue
                            } catch (InterruptedException e) {
                                logger.error("Error: ", e);
                            }
                        }
                        logger.info("Change Context(" + (current == null ? null : current.getVillageId()) + "->" + next.getVillageId() + ")!");
                        currentContext.set(next);
                        currentContext.get().getSemaphore().release(ACCESS_LIMIT_THREAD); // unfreeze queue for next active village
                    }
                    TimeUnit.SECONDS.sleep(TIMEOUT_SECONDS);
                }
            }
        };
        if (!isStarted) {
            createDaemonThreadWithName(CONTEXT_HANDLER_THREAD_NAME, runnable).start();
            isStarted = true;
        }
    }

    public static Thread createDaemonThreadWithName(String name, Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.setDaemon(true);
        thread.setName(name);
        return thread;
    }
}
