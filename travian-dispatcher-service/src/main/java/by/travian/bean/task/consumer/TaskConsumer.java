package by.travian.bean.task.consumer;

import by.travian.bean.task.Task;
import by.travian.bean.task.handler.ContextHandler;
import by.travian.common.command.Command;

import java.util.concurrent.BlockingQueue;

public interface TaskConsumer<T extends Task> extends Runnable {
    T getTask();

    BlockingQueue<T> getQueue();

    void consume(BlockingQueue<T> queue);

    void consume(T task);

    ContextHandler getContextHandler();

}
