package by.travian.bean.task.consumer;

import by.travian.bean.entity.Entity;
import by.travian.bean.task.Task;
import by.travian.bean.task.handler.ContextHandler;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

@Data
public abstract class AbstractTaskConsumer<E extends Entity> implements TaskConsumer<Task<E>> {
    private final static Logger logger = LoggerFactory.getLogger(AbstractTaskConsumer.class);
    private BlockingQueue<Task<E>> queue;
    private Task<E> task;
    protected ContextHandler contextHandler;

    protected AbstractTaskConsumer(BlockingQueue<Task<E>> queue, ContextHandler contextHandler) {
        this.queue = queue;
        this.contextHandler = contextHandler;
    }


    @Override
    public Task<E> getTask() {
        return task;
    }

    @Override
    public void consume(BlockingQueue<Task<E>> queue) {
        while (true) {
            try {
                task = queue.take();
                if (task.getRepeatTimeWait() > 0) { //doesn't work as expected!
                    long overdue = System.currentTimeMillis() - task.getNextExecutionTimestamp();
                    if (overdue > task.getRepeatTimeWait()) {
                        logger.info("Task is overdue on " + TimeUnit.MINUTES.convert(overdue,TimeUnit.MILLISECONDS) +" minutes. " + task.getEntity());
                        continue;
                    }
                }
                contextHandler.handleContextAccessAndRun(task.getEntity().getVillageId(), () -> consume(task));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public abstract void consume(Task<E> task);

    @Override
    public void run() {
        consume(getQueue());
    }


}


