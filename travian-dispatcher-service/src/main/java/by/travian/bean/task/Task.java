package by.travian.bean.task;

import by.travian.bean.entity.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.concurrent.atomic.AtomicInteger;

@Data
@AllArgsConstructor
public class Task<T extends Entity> implements Cloneable {
    protected long initialDelay = 0;
    protected AtomicInteger repeat = new AtomicInteger(1);
    protected long repeatTimeWait = 0;
    protected final Object lock = new Object();
    protected T entity;
    protected ExecutionPhase currentPhase = ExecutionPhase.NOT_STARTED;
    protected long nextExecutionTimestamp;

    @Override
    public Task clone() throws CloneNotSupportedException {
        Task task = new Task(this.entity); //TODO: clone entity
        task.setInitialDelay(this.initialDelay);
        task.setRepeat(new AtomicInteger(this.repeat.get()));
        task.setRepeatTimeWait(this.repeatTimeWait);

        task.setNextExecutionTimestamp(this.nextExecutionTimestamp);
        task.setCurrentPhase(this.currentPhase);
        return task;
        //throw new CloneNotSupportedException();
    }

    public enum ExecutionPhase {
        NOT_STARTED,
        STARTED,
        COMPLETE,
        ERROR
    }

    public enum Priority {
        LOW(1), NORMAL(2), HIGH(3), THE_HIGHEST(4);

        int value;

        Priority(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public Task(long initialDelay, AtomicInteger repeat, long repeatTimeWait, T entity) {
        this.initialDelay = initialDelay;
        this.repeat = repeat;
        this.repeatTimeWait = repeatTimeWait;
        this.entity = entity;
    }

    public Task(T entity) {
        this.entity = entity;
    }


    public Object getLock() {
        return lock;
    }

    public long getNextExecutionTimestamp() {
        return nextExecutionTimestamp;
    }

    public void setNextExecutionTimestamp(long nextExecutionTimestamp) {
        this.nextExecutionTimestamp = nextExecutionTimestamp;
    }

}
