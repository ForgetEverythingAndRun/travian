package by.travian.bean.task;

import by.travian.bean.entity.MarketEntity;

import java.util.concurrent.atomic.AtomicInteger;

public class MarketTask extends Task<MarketEntity> {

    public MarketTask(long initialDelay, AtomicInteger repeat, long repeatTimeWait, MarketEntity entity) {
        super(initialDelay, repeat, repeatTimeWait, entity);
    }

    @Override
    public MarketEntity getEntity() {
        return super.getEntity();
    }

}
