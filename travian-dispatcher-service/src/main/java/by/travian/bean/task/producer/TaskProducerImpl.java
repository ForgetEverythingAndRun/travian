package by.travian.bean.task.producer;

import by.travian.bean.entity.Entity;
import by.travian.bean.task.Task;
import by.travian.bean.task.handler.ContextHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.BlockingQueue;

public class TaskProducerImpl<T extends Entity> implements TaskProducer<Task<T>> {
    private final static Logger logger = LoggerFactory.getLogger(TaskProducer.class);
    private BlockingQueue<Task<T>> queue;
    private List<Task<T>> tasks;
    protected ContextHandler contextHandler;

    public TaskProducerImpl(ContextHandler contextHandler, BlockingQueue<Task<T>> queue, List<Task<T>> tasks) {
        this.contextHandler = contextHandler;
        this.queue = queue;
        this.tasks = tasks;
    }

    @Override
    public List<Task<T>> getTasks() {
        return tasks;
    }

    @Override
    public BlockingQueue<Task<T>> getQueue() {
        return queue;
    }

    @Override
    public void produce(BlockingQueue<Task<T>> queue, Task<T> task) {
        try {
            waitInitialDelay(task);
            //queue.put(task);
            queue.put(task.clone());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void produce(Task<T> task) {
        while (task.getRepeat().get() > 0) {
            try {
                produce(getQueue(), task);
                waitRepeatTimeDelay(task);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void produce(List<Task<T>> tasks) {
        for (Task<T> task : tasks) {
            produce(task);
        }
    }


    protected void waitInitialDelay(Task<T> task) throws InterruptedException {
        if (task.getInitialDelay() > 0) {
            Thread.sleep(task.getInitialDelay());
            task.setInitialDelay(0);
        }
    }

    protected void waitRepeatTimeDelay(Task<T> task) throws InterruptedException {
        if (task.getRepeat().decrementAndGet() > 0) {
            if (task.getRepeatTimeWait() > 0) {
                task.setNextExecutionTimestamp(System.currentTimeMillis() + task.getRepeatTimeWait());
                Thread.sleep(task.getRepeatTimeWait());
            }else{
                task.setNextExecutionTimestamp(System.currentTimeMillis());
            }
        }
    }

    @Override
    public void run() {
        produce(getTasks());
    }

}
