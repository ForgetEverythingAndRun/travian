package by.travian.bean.task.manager;

import by.travian.bean.Nation;
import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.building.Building;
import by.travian.bean.task.Task;
import by.travian.bean.task.consumer.AbstractTaskConsumer;
import by.travian.bean.task.consumer.TaskConsumer;
import by.travian.bean.task.handler.ContextHandler;
import by.travian.bean.task.producer.BuildingProducerImpl;
import by.travian.bean.task.producer.TaskProducer;
import by.travian.common.command.Command;
import by.travian.common.command.factory.CommandFactory;
import by.travian.common.context.TravianCommandContext;
import by.travian.configuration.TravianConfiguration;
import by.travian.service.TravianService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;


public class BuildingTaskManagerImpl extends AbstractTaskManager<BuildingEntity> {
    private final static int BUILDINGS_QUEUE_PER_VILLAGE = MIN_SIZE_OF_QUEUE + 5;
    private Command command;

    private TravianService service;
    private TravianConfiguration travianConfiguration;

    public BuildingTaskManagerImpl(CommandFactory commandFactory, ContextHandler contextHandler, TravianConfiguration travianConfiguration, TravianService service) {
        super(new LinkedBlockingQueue<>(BUILDINGS_QUEUE_PER_VILLAGE), contextHandler);
        this.travianConfiguration = travianConfiguration;
        this.command = commandFactory.createVillageBuildingCommand();
        this.service = service;
    }

    @Override
    public TaskProducer<Task<BuildingEntity>> taskProducer(List<Task<BuildingEntity>> tasks) {
        return new BuildingProducerImpl(contextHandler, getQueue(), tasks, service);
    }

    @Override
    public TaskProducer<Task<BuildingEntity>> taskProducer(Task<BuildingEntity> task) {
        return new BuildingProducerImpl(contextHandler, getQueue(), Collections.singletonList(task), service);
    }

    @Override
    public void execute(List<Task<BuildingEntity>> taskList) {
        //setTaskList(taskList);
        //setQueue(new LinkedBlockingQueue<>(taskList.size() + 1));
        //int poolSize = service.getVillageContainerManager().getVillages().size() * 2 + CONSUMER_THREAD;
        //setExecutor((ThreadPoolExecutor) Executors.newFixedThreadPool(poolSize, threadFactory()));

        executor.execute(taskConsumer());

        service.getVillageContainerManager().getVillages().forEach(village -> {
            List<Task<BuildingEntity>> villageTaskList = taskList.stream()
                    .filter(task -> task.getEntity().getVillageId().equals(village.getVillageId()))
                    .collect(Collectors.toList());
            if (Nation.ROMANS.equals(travianConfiguration.getNation(village.getVillageId()))) {
                executor.execute(taskProducer(villageTaskList.stream().filter(task -> !Building.isResourceBuilding(task.getEntity().getId())).collect(Collectors.toList())));
                executor.execute(taskProducer(villageTaskList.stream().filter(task -> Building.isResourceBuilding(task.getEntity().getId())).collect(Collectors.toList())));
            } else {
                executor.execute(taskProducer(villageTaskList));
            }
        });

        executor.shutdown();
    }

    @Override
    public TaskConsumer<Task<BuildingEntity>> taskConsumer() {
        return new AbstractTaskConsumer<BuildingEntity>(getQueue(), contextHandler) {
            Logger logger = LoggerFactory.getLogger(AbstractTaskConsumer.class);

            @Override
            public void consume(Task<BuildingEntity> task) {
                command.setCommandContext(new TravianCommandContext(task.getEntity()));
                command.execute();
                if (command.getCommandContext().isFailedCommand()){
                    task.setCurrentPhase(Task.ExecutionPhase.ERROR);
                }

                synchronized (task.getLock()) {
                    logger.info("Task consumed from queue: " + task.getEntity());
                    task.getLock().notifyAll();
                }
            }
        };
    }

}
