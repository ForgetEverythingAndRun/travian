package by.travian.bean.task;

import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.building.Building;

import java.util.concurrent.atomic.AtomicInteger;

public class BuildingTask extends Task<BuildingEntity> {

    public BuildingTask(BuildingEntity entity) {
        super(entity);
        this.setRepeat(new AtomicInteger(1));
    }

    @Override
    public BuildingEntity getEntity() {
        return super.getEntity();
    }
}
