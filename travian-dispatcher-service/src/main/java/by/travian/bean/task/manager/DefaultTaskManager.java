package by.travian.bean.task.manager;

import by.travian.bean.entity.Entity;
import by.travian.bean.task.Task;
import by.travian.bean.task.consumer.AbstractTaskConsumer;
import by.travian.bean.task.consumer.TaskConsumer;
import by.travian.bean.task.handler.ContextHandler;
import by.travian.bean.task.producer.TaskProducer;
import by.travian.bean.task.producer.TaskProducerImpl;
import by.travian.common.command.Command;
import by.travian.common.context.TravianCommandContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class DefaultTaskManager<E extends Entity> extends AbstractTaskManager<E> {
    protected Command command;

    public DefaultTaskManager(Command command, ContextHandler contextHandler) {
        super(new LinkedBlockingQueue<>(MIN_SIZE_OF_QUEUE), contextHandler);
        this.command = command;
    }

    public DefaultTaskManager(BlockingQueue<Task<E>> queue, Command command, ContextHandler contextHandler) {
        super(queue, contextHandler);
        this.command = command;
    }

    @Override
    public TaskProducer<Task<E>> taskProducer(List<Task<E>> tasks) {
        return new TaskProducerImpl(contextHandler, getQueue(), tasks);
    }

    @Override
    public TaskProducer<Task<E>> taskProducer(Task<E> task) {
        return new TaskProducerImpl(contextHandler, getQueue(), Collections.singletonList(task));
    }

    @Override
    public TaskConsumer<Task<E>> taskConsumer() {
        return new AbstractTaskConsumer<E>(getQueue(), contextHandler) {
            Logger logger = LoggerFactory.getLogger(AbstractTaskConsumer.class);

            @Override
            public void consume(Task<E> task) {
                task.setCurrentPhase(Task.ExecutionPhase.STARTED);
                command.setCommandContext(new TravianCommandContext(task.getEntity()));
                command.execute();
                if (command.getCommandContext().isFailedCommand()) {
                    task.setCurrentPhase(Task.ExecutionPhase.ERROR);
                }
                if(task.getRepeat().get() == 0){
                    task.setCurrentPhase(Task.ExecutionPhase.COMPLETE);
                }
            }
        };
    }

}
