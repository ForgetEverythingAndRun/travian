package by.travian.bean.task.manager;

import by.travian.bean.entity.OffenseEntity;
import by.travian.bean.task.handler.ContextHandler;
import by.travian.common.command.factory.CommandFactory;

import java.util.concurrent.LinkedBlockingQueue;

public class OffenseTaskManagerImpl extends DefaultTaskManager<OffenseEntity> {

    public OffenseTaskManagerImpl(CommandFactory commandFactory, ContextHandler contextHandler) {
        //new PriorityBlockingQueue<>(MIN_SIZE_OF_QUEUE, taskComparator());
        super(new LinkedBlockingQueue<>(MIN_SIZE_OF_QUEUE + 10),commandFactory.createOffenseCommand(), contextHandler);
    }

}
