package by.travian.bean.task.producer;

import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.building.Level;
import by.travian.bean.entity.building.Slot;
import by.travian.bean.entity.village.Village;
import by.travian.bean.task.Task;
import by.travian.service.TravianService;
import by.travian.state.BuildingVerifyCompletionState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

@Deprecated
//Find bug: getBuildingQueueTimeWait isn't updated
public class BuildingProducerImpl2 extends TaskProducerImpl<BuildingEntity> {
    public final static Logger logger = LoggerFactory.getLogger(BuildingProducerImpl.class);
    private TravianService service;

    public BuildingProducerImpl2(BlockingQueue<Task<BuildingEntity>> queue, List<Task<BuildingEntity>> tasks, TravianService service) {
        super(queue, tasks);
        this.service = service;
    }


    @Override
    public void produce(Task<BuildingEntity> task) {
        BuildingEntity entity = task.getEntity();

        Village village = service.getVillageContainerManager().getVillageById(entity.getVillageId());

        if (village == null) {
            logger.error("Can't find village with id: " + entity.getVillageId());
            return;
        }

        Building building = null;
        try {
            building = verifyAndUpdateBuildingEntity(village, task).getValue();
        } catch (IllegalArgumentException e) {
            logger.error("", e);
            return;
        }

        int iterationCount = 0;

        while (!task.isComplete()) {
            task.setState(new BuildingVerifyCompletionState(task, service));
            task.updateState(village);
            if (!task.isComplete()) {
                try {
                    synchronized (task.getLock()) {
                        produce(getQueue(), task);
                        task.getLock().wait();
                    }
                } catch (InterruptedException e) {
                    logger.error("", e);
                    return; //skip broken task
                }
                iterationCount++;
            }
        }

        if (iterationCount == 0) {
            logger.trace("Skip. VillageId: " + village + " Already upgraded: " + building.getLevel().getNumber() + " of " + task.getEntity().getUntilLvlRepeat() + " slot: " + task.getEntity().getSlotId() + " building: " + building);
        }
    }

    protected Map.Entry<Slot, Building> verifyAndUpdateBuildingEntity(Village village, Task<BuildingEntity> task) {
        BuildingEntity entity = task.getEntity();
        final List<Level> levels = service.getBuildingLevelMap().get(entity.getId());
        if (entity.getUntilLvlRepeat() > levels.size()) {
            throw new IllegalArgumentException("Incorrect task's entity. Check 'untilLvlRepeat' max level " + entity);
        }
        Slot slot = new Slot(entity.getSlotId());
        Building building = new Building(entity.getId());

        Map.Entry<Slot, Building> findActualBuildingSlot = village.getBuildingSlotContainer().getBuildingSlotEntry(slot, building);
        if (findActualBuildingSlot == null) {
            throw new IllegalArgumentException("Incorrect task's entity. Check 'buildingId' for corresponding slot: " + entity);
        }
        if (entity.getSlotId() == 0) { // slotId = 0 means that need to find out actual slot value for building
            entity.setSlotId(findActualBuildingSlot.getKey().getId());
        }
        return findActualBuildingSlot;
    }


}
