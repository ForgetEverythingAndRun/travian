package by.travian.bean.task.manager;

import by.travian.bean.entity.LoginEntity;
import by.travian.bean.task.handler.ContextHandler;
import by.travian.common.command.factory.CommandFactory;

import java.util.concurrent.LinkedBlockingQueue;

public class LoginTaskManagerImpl extends DefaultTaskManager<LoginEntity> {

    public LoginTaskManagerImpl(CommandFactory commandFactory, ContextHandler contextHandler) {
        super(new LinkedBlockingQueue<>(MIN_SIZE_OF_QUEUE),commandFactory.createLoginCommand(), contextHandler);
    }

}
