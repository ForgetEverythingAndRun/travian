package by.travian.bean.task.manager;

import by.travian.bean.entity.Entity;
import by.travian.bean.task.Task;
import by.travian.bean.task.consumer.TaskConsumer;
import by.travian.bean.task.handler.ContextHandler;
import by.travian.bean.task.producer.TaskProducer;
import by.travian.error.handler.ExceptionHandler;
import lombok.Data;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.*;

@Data
public abstract class AbstractTaskManager<E extends Entity> implements TaskManager<Task<E>> {
    protected final static int CONSUMER_THREAD = 1;
    protected final static int PRODUCER_THREAD = 1;
    protected final static int EXTRA_PRODUCER_THREAD = 1;
    protected final static int MIN_SIZE_OF_EXECUTOR_POLL = CONSUMER_THREAD + PRODUCER_THREAD + EXTRA_PRODUCER_THREAD;
    protected final static int MIN_SIZE_OF_QUEUE = MIN_SIZE_OF_EXECUTOR_POLL;

    protected BlockingQueue<Task<E>> queue;
    protected ThreadPoolExecutor executor;
    protected ContextHandler contextHandler;

    public AbstractTaskManager(ContextHandler contextHandler) {
        //it's required for high priority offensive operations in scope of one village
        this.queue = new LinkedBlockingQueue<>(MIN_SIZE_OF_QUEUE);
        this.executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(MIN_SIZE_OF_EXECUTOR_POLL, threadFactory());
        ((ThreadPoolExecutor) executor).setRejectedExecutionHandler(rejectedExecutionHandler());
        this.contextHandler = contextHandler;
    }

    public AbstractTaskManager(BlockingQueue<Task<E>> queue, ContextHandler contextHandler) {
        this.queue = queue;
        this.contextHandler = contextHandler;
        int capacity = queue.size() + queue.remainingCapacity();
        int size = capacity >= MIN_SIZE_OF_EXECUTOR_POLL ? capacity : MIN_SIZE_OF_EXECUTOR_POLL;
        this.executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(size, threadFactory());
        ((ThreadPoolExecutor) executor).setRejectedExecutionHandler(rejectedExecutionHandler());
    }

    @Override
    public BlockingQueue<Task<E>> getQueue() {
        return queue;
    }

    public abstract TaskProducer<Task<E>> taskProducer(List<Task<E>> tasks);

    public abstract TaskProducer<Task<E>> taskProducer(Task<E> task);

    public ContextHandler getContextHandler() {
        return contextHandler;
    }


    @Override
    public void execute(List<Task<E>> taskList) {
        executor.execute(taskConsumer());
        for (Task<E> task : taskList) {
            executor.execute(taskProducer(task));//default: each task has own thread(producer)
        }
        //executor.shutdown();
    }

    public RejectedExecutionHandler rejectedExecutionHandler() {
        return new RejectedTaskController();
    }

    protected ThreadFactory threadFactory() {
        return new ThreadFactory() {
            public Thread newThread(Runnable r) {
                Thread t = Executors.defaultThreadFactory().newThread(r);
                t.setDaemon(true);
                t.setUncaughtExceptionHandler(new ExceptionHandler());
                return t;
            }
        };
    }
}
