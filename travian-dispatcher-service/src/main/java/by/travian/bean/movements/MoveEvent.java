package by.travian.bean.movements;

import by.travian.bean.entity.village.Coordinates;
import by.travian.bean.entity.village.Village;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MoveEvent {
    private Village village;
    private long timestamp;
    private String details;
    private Coordinates from;
    private boolean isNew = true;

    public MoveEvent(Village village, long timestamp, String details, Coordinates from) {
        this.village = village;
        this.timestamp = timestamp;
        this.details = details;
        this.from = from;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MoveEvent event = (MoveEvent) o;
        //TODO: 'timestamp' / 1000 the same as 'details'. There is a deviation about a ~1 sec. Can be an issue in case of 3 waves in a sec
        //Solution: Ignore amount of waves which are arrived in the same time(+/-sec) and count it as one wave. Notification for specific case is also working but with clause.
        return  Objects.equals(village, event.village) &&
                Objects.equals(details, event.details) &&
                Objects.equals(from, event.from);
    }

    @Override
    public int hashCode() {
        return Objects.hash(village, details, from);
    }
}
