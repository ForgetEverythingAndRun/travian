package by.travian.bean.resource;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Администратор on 29.10.2017.
 */
@AllArgsConstructor
@Data
public class Resource {
    private ResourceType type;
    //wood, clay, iron, crop
    private int amount;

    public int getId() {
        return this.type.getId();
    }

    public enum ResourceType {
        WOOD(1), CLAY(2), IRON(3), CROP(4);
        private int id;

        public int getId() {
            return id;
        }

        private static final Map<Integer, ResourceType> map = new HashMap<>();

        static {
            for (ResourceType resourceType : ResourceType.values()) {
                map.put(resourceType.getId(), resourceType);
            }
        }


        public static ResourceType from(int value) {
            return map.get(value);
        }

        ResourceType(int id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return this.name().toLowerCase();
        }
    }

}
