package by.travian.bean.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Администратор on 29.10.2017.
 */
public class VillageResource extends Resource {
    private final static Logger logger = LoggerFactory.getLogger(VillageResource.class);
    private final static int DEFAULT_RESOURCE_PRODUCTION = 12;
    private int production = DEFAULT_RESOURCE_PRODUCTION;
    private long lastUpdate;

    public VillageResource(int id) {
        super(ResourceType.from(id), 0);
    }

    public VillageResource(int id, int amount) {
        super(ResourceType.from(id), amount);
    }

    public VillageResource(ResourceType resourceType) {
        super(resourceType, 0);
    }

    public VillageResource(ResourceType resourceType, int amount) {
        super(resourceType, amount);
    }

    public VillageResource(int id, int amount, int production) {
        super(ResourceType.from(id), amount);
        this.production = production;
    }

    public VillageResource(ResourceType resourceType, int amount, int production) {
        super(resourceType, amount);
        this.production = production;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public int getProduction() {
        return production;
    }

    public void setProduction(int production) {
        if (getType().getId() != 4 && production < DEFAULT_RESOURCE_PRODUCTION) {
            logger.debug("Couldn't set the production value less than default(" + DEFAULT_RESOURCE_PRODUCTION + ")");
            return;
        }
        this.production = production;
        setLastUpdate(System.currentTimeMillis());
    }

    @Override
    public void setAmount(int amount) {
        super.setAmount(amount);
        setLastUpdate(System.currentTimeMillis());
    }

    @Override
    public String toString() {
        return "{amount = " + getAmount() +
                ", production = " + production +
                '}';
    }
}
