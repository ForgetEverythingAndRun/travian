package by.travian.util;

import by.travian.constant.TravianConstant;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HttpHelper {

    public static class HeaderBuilder {
        private HttpHeaders headers = new HttpHeaders();

        public HeaderBuilder() {
            setCommonHeaders();
        }

        private HeaderBuilder setCommonHeaders() {
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.add(TravianConstant.Common.HOST_HEADER, TravianConstant.Common.HOST);
            headers.add(TravianConstant.Common.USER_AGENT_HEADER, TravianConstant.Common.BROWSER_USER_AGENT);
            return this;
        }

        public HeaderBuilder accept(MediaType mediaType) {
            headers.setAccept(Collections.singletonList(mediaType));
            return this;
        }

        public HeaderBuilder contentType(MediaType mediaType) {
            headers.setContentType(mediaType);
            return this;
        }

        public HeaderBuilder add(String key, String value) {
            headers.add(key, value);
            return this;
        }

        public HeaderBuilder cookies(String cookies) {
            if (!StringUtils.isBlank(cookies)) {
                headers.add(TravianConstant.Cookie.COOKIE, cookies);
            }
            return this;
        }

        public HeaderBuilder referer(String referer) {
            headers.add(TravianConstant.Common.REFERER_HEADER, referer);
            return this;
        }

        public HttpHeaders build() {
            return new HttpHeaders(headers);
        }
    }

    @Deprecated
    public static CloseableHttpClient createCloseableHttpClient() {
        CloseableHttpClient client = HttpClientBuilder.create().build();//useSystemProperties()?
        return client;
    }

    @Deprecated
    public static CloseableHttpResponse handleRequest(CloseableHttpClient client, HttpUriRequest requestBase) throws IOException {
        CloseableHttpResponse response = client.execute(requestBase);
        return response;
    }

    @Deprecated
    public static List<NameValuePair> createNameValuePairs(BasicNameValuePair... pairs) {
        List<NameValuePair> list = new ArrayList<NameValuePair>();
        for (NameValuePair pair : pairs) {
            list.add(pair);
        }
        return list;
    }

    @Deprecated
    public static String handleResponse(CloseableHttpResponse response) throws IOException {
        String bodyAsString = EntityUtils.toString(response.getEntity());
        return bodyAsString;
    }

}
