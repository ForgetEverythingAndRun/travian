package by.travian.util;

import by.travian.bean.entity.building.BuildingMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class JaxbParser {

    public final static Logger logger = LoggerFactory.getLogger(JaxbParser.class);
    private static final String BUILDING_XML = "buildings.xml";

    public static BuildingMap unmarshalBuildings() {
        BuildingMap container = null;
        try {
            JAXBContext jc = JAXBContext.newInstance(BuildingMap.class);

            Unmarshaller unmarshaller = jc.createUnmarshaller();
            container = (BuildingMap) unmarshaller.unmarshal(JaxbParser.class.getClassLoader().getResourceAsStream(BUILDING_XML));
        } catch (JAXBException e) {
            logger.error("", e);
        }

        return container;
    }
}
