package by.travian.util.hero;

public class HeroAdventureDifficultyUtil {

    public static void main(String[] args) {
        //System.out.println(Arrays.toString("1231".split(",")));
        System.out.println(getAdventureLossesHeroHP(180, AdventureDifficulty.DIFFICULT.getMin(), 16));
        System.out.println(getAdventureLossesHeroHP(180, AdventureDifficulty.DIFFICULT.getMax(), 16));
        //System.out.println(getAdventureLossesHeroHP(100, 8000, AdventureDifficulty.NORMAL.getMax(), 12));
    }

    //Find more here: http://answers.travian.ru/?view=answers&action=answer&aid=345
    public static int getAdventureLossesHeroHP(int power, int difficulty, int adventureCount) {
        int def = adventureCount * difficulty + 5;
        double loseHP = 100 * Math.pow((def / (double) power), 1.5);
        System.out.println(loseHP);
        return (int) Math.round(loseHP);
    }


    public static enum AdventureDifficulty {
        NORMAL(1, 4),
        DIFFICULT(4, 7);

        private int min;
        private int max;

        AdventureDifficulty(int min, int max) {
            this.min = min;
            this.max = max;
        }

        public int getMin() {
            return min;
        }

        public void setMin(int min) {
            this.min = min;
        }

        public int getMax() {
            return max;
        }

        public void setMax(int max) {
            this.max = max;
        }
    }
}
