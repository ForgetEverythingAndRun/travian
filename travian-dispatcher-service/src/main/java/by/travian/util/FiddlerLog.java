package by.travian.util;

public class FiddlerLog {
    public static void jmeter(){
        System.setProperty("http.proxyHost", "127.0.0.1");
        System.setProperty("https.proxyHost", "127.0.0.1");
        System.setProperty("http.proxyPort", "8888");
        System.setProperty("https.proxyPort", "8888");
    }

    public static void capture(){
        System.setProperty("http.proxyHost", "127.0.0.1");
        System.setProperty("https.proxyHost", "127.0.0.1");
        System.setProperty("http.proxyPort", "8888");
        System.setProperty("https.proxyPort", "8888");
    }

    public static void setByDefault() {
        System.setProperty("http.proxyHost", "");
        System.setProperty("http.proxyPort", "80");
        System.setProperty("https.proxyHost", "");
        System.setProperty("https.proxyPort", "443");
    }

}
