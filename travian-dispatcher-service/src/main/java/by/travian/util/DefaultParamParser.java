package by.travian.util;

import by.travian.error.SendTroopsException;
import by.travian.error.TravianException;
import org.apache.commons.lang3.StringUtils;

import java.util.Base64;

public class DefaultParamParser {

    public static String findChecksum(String response) throws SendTroopsException {
        String paramName = "checksum"; //timestamp_checksum
        if (StringUtils.isBlank(response)) {
            throw new SendTroopsException("Response is empty. Query param '" + paramName + "' is undefined.");
        }
        String keyword = "<input type=\"hidden\" name=\""+paramName+"\" value=\"";
        int start = response.indexOf(keyword);
        int end = response.indexOf("\"", start + keyword.length());
        if ((start == -1) || (end == -1))
            throw new SendTroopsException("Query param '" + paramName + "' is undefined. Cant parse it.");
        String timestampchecksum = response.substring(start + keyword.length(), end);
        return timestampchecksum;
    }

    public static String findTimestamp(String response) throws SendTroopsException {
        if (StringUtils.isBlank(response)) {
            throw new SendTroopsException("Response is empty. Query param '" + "timestamp" + "' is undefined.");
        }
        String keyword = "<input type=\"hidden\" name=\"timestamp\" value=\"";
        int start = response.indexOf(keyword);
        int end = response.indexOf("\"", start + keyword.length());
        if ((start == -1) || (end == -1))
            throw new SendTroopsException("Query param '" + "timestamp" + "' is undefined. Cant parse it.");
        return response.substring(start + keyword.length(), end);
    }

    public static String findKid(String response) throws SendTroopsException {
        if (StringUtils.isBlank(response)) {
            throw new SendTroopsException("Response is empty. Query param '" + "kid" + "' is undefined.");
        }
        String keyword = "<input type=\"hidden\" name=\"kid\" value=\"";
        int start = response.indexOf(keyword);
        int end = response.indexOf("\"", start + keyword.length());
        if ((start == -1) || (end == -1))
            throw new SendTroopsException("Query param '" + "kid" + "' is undefined. Cant parse it.");
        return response.substring(start + keyword.length(), end);
    }

    public static String findA(String response) throws SendTroopsException {
        if (StringUtils.isBlank(response)) {
            throw new SendTroopsException("Response is empty. Query param '" + "a" + "' is undefined.");
        }
        String keyword = "<button type=\"submit\" value=\"";
        int start = response.lastIndexOf(keyword);
        int end = response.indexOf("\" name=\"a\"", start + keyword.length());
        if ((start == -1) || (end == -1))
            throw new SendTroopsException("Query param '" + "a" + "' is undefined. Cant parse it.");
        return response.substring(start + keyword.length(), end);
    }

    public static String findInput(String name, String response) throws SendTroopsException {
        if (StringUtils.isBlank(response)) {
            throw new SendTroopsException("Response is empty. Query param '" + name + "' is undefined.");
        }
        String keyword = "<input type=\"hidden\" name=\"" + name + "\" value=\"";
        int start = response.indexOf(keyword);
        int end = response.indexOf("\"", start + keyword.length());
        if ((start == -1) || (end == -1))
            throw new SendTroopsException("Query param '" + name + "' is undefined. Cant parse it.");
        return response.substring(start + keyword.length(), end);
    }

    public static String findInputWithNameAndId(String name, String response) throws TravianException {
        if (StringUtils.isBlank(response)) {
            throw new SendTroopsException("Response is empty. Query param '" + name + "' is undefined.");
        }
        String keyword = "<input type=\"hidden\" name=\"" + name + "\" id=\""+name+"\" value=\"";
        int start = response.indexOf(keyword);
        int end = response.indexOf("\"", start + keyword.length());
        if ((start == -1) || (end == -1))
            throw new SendTroopsException("Query param '" + name + "' is undefined. Cant parse it.");
        return response.substring(start + keyword.length(), end);
    }

    /*building upgrade*/
    public static String findC(String response) throws TravianException {
        if (StringUtils.isBlank(response)) {
            throw new TravianException("Response is empty. Query param 'c' is undefined.");
        }
        String keyword = "c=";
        int start = response.indexOf(keyword);
        int end = response.indexOf("'; return", start + keyword.length());
        if ((start == -1) || (end == -1))
            throw new TravianException("Query param 'c' is undefined. Upgrade building request can't be sent.");
        String c = response.substring(start + keyword.length(), end);
        return c;
    }

    public static String findBuildingQueue(String response) throws TravianException {
        if (StringUtils.isBlank(response)) {
            throw new TravianException("Response is empty. Can't find buildings in queue.");
        }
        String keyword = "<script type=\"text/javascript\">var bld=";
        int start = response.indexOf(keyword);
        int end = response.indexOf("</script>", start + keyword.length());
        if ((start == -1) || (end == -1))
            throw new TravianException("Buildings queue is empty.");
        String items = response.substring(start + keyword.length(), end);
        return items;
    }


    public static String findToken(String response) throws TravianException {
        if (StringUtils.isBlank(response)) {
            throw new TravianException("Response is empty. Token for header 'Authorization' is undefined.");
        }
        String keyword = "eval(atob(\'";
        int start = response.indexOf(keyword);
        int end = response.indexOf("\'));", start + keyword.length());
        if ((start == -1) || (end == -1))
            throw new TravianException("Token for header 'Authorization' is undefined.");
        String token = response.substring(start + keyword.length(), end);
        return extractToken(new String(Base64.getDecoder().decode(token.getBytes())));
    }

    private static String extractToken(String response) throws TravianException {
        //response = Travian.courtroomSalsasBombastic = window === parent && '15256514472521e9b541024d0366268c'
        if (StringUtils.isBlank(response)) {
            throw new TravianException("Response is empty. Token for header 'Authorization' is undefined.");
        }
        String keyword = "\'";
        int start = response.indexOf(keyword);
        int end = response.indexOf("\'", start + keyword.length());
        if ((start == -1) || (end == -1))
            throw new TravianException("Token for header 'Authorization' is undefined.");
        String token = response.substring(start + keyword.length(), end);
        return token;
    }

    public static String findVillageActiveId(String str) throws TravianException {
        if (StringUtils.isBlank(str)) {
            throw new TravianException("Response is empty. Can't parse village active id");
        }
        String keyword = "?newdid=";
        int start = str.indexOf(keyword);
        int end = str.indexOf("&", start + keyword.length());
        if ((start == -1) || (end == -1)) throw new TravianException("Can't find village active id");
        String c = str.substring(start + keyword.length(), end);
        return c;
    }
}
