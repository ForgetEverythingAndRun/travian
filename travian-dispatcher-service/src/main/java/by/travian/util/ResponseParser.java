package by.travian.util;


import by.travian.bean.Nation;
import by.travian.bean.container.*;
import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.building.Level;
import by.travian.bean.entity.building.Slot;
import by.travian.bean.entity.village.Village;
import by.travian.bean.entity.village.Coordinates;
import by.travian.bean.entity.HeroAdventureEntity;
import by.travian.bean.movements.MoveEvent;
import by.travian.bean.troop.Troops;
import by.travian.configuration.TravianConfiguration;
import by.travian.error.TravianException;
import com.google.common.base.CharMatcher;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Viktar_Kapachou on 11/1/2017.
 */

public class ResponseParser {
    public final static Logger logger = LoggerFactory.getLogger(ResponseParser.class);


    private final XPathExpression VILLAGE_ATTACK_LIST = XPathHelper.compileXpathExpression("//table[contains(@class,'inAttack') or contains(@class,'inRaid')]");
    private final XPathExpression VILLAGE_ATTACK_INFO = XPathHelper.compileXpathExpression("tbody[contains(@class,'infos')]");
    private final XPathExpression VILLAGE_ATTACK_TIMER = XPathHelper.compileXpathExpression(".//div[@class='in']/span[@class='timer']/@value");
    private final XPathExpression VILLAGE_ATTACK_ARRIVAL_TIME_VALUE = XPathHelper.compileXpathExpression(".//div[@class='at']/span[1]");
    private final XPathExpression VILLAGE_ATTACK_FROM = XPathHelper.compileXpathExpression("tbody[contains(@class,'units')]");

    private final XPathExpression VILLAGE_ACTIVE_ID_XPATH = XPathHelper.compileXpathExpression("//a[@class='active']/@href");
    private final XPathExpression OWN_VILLAGES_XPATH = XPathHelper.compileXpathExpression("//div[@class='villageList']/div[@class='dropContainer']/div[contains(@class,'listEntry')]/a");


    private final XPathExpression VILLAGE_NAME_XPATH = XPathHelper.compileXpathExpression(".//span[@class='name']");
    private final XPathExpression VILLAGE_COORDINATES_XPATH = XPathHelper.compileXpathExpression("..//span[contains(@class,'coordinates ')]");

    private final XPathExpression VILLAGE_COORDINATE_X_XPATH = XPathHelper.compileXpathExpression("span[@class='coordinateX']");
    private final XPathExpression VILLAGE_COORDINATE_Y_XPATH = XPathHelper.compileXpathExpression("span[@class='coordinateY']");
    private final XPathExpression VILLAGE_ID = XPathHelper.compileXpathExpression("@href[contains(.,'?newdid=')]");

    private final XPathExpression BUILDING_DASHBOARD_WAIT_TIME = XPathHelper.compileXpathExpression("//div[@class='buildingList']/ul/li/div[@class='buildDuration']/span[@class='timer']/@value");

    private final XPathExpression HERO_ADVENTURE_STATUS_NOW = XPathHelper.compileXpathExpression("//div[contains(@class,'heroStatusMessage')][img[contains(@class,'heroStatus')]]");
    private final XPathExpression HERO_AT_HOME_NOW = XPathHelper.compileXpathExpression("img[contains(@class,'heroStatus')]/@class");
    private final XPathExpression HERO_WAIT_TIME_NOW = XPathHelper.compileXpathExpression(".//span[@class='timer']/@value");

    private final XPathExpression TRAVIAN_PLUS_STATUS_NOW = XPathHelper.compileXpathExpression("//div[contains(@class,'featureContent') and contains(@class,'plus') and contains(@class,'active')]");
    private final XPathExpression TRAVIAN_PLUS_TILL_TIME = XPathHelper.compileXpathExpression(".//span[@class='timer']/@value");

    private final XPathExpression MARKET_MERCHANTS_AVAILABLE_COUNT = XPathHelper.compileXpathExpression("//span[@class='merchantsAvailable']");
    private final XPathExpression MARKET_MERCHANTS_CAPACITY = XPathHelper.compileXpathExpression("//div[@class='carry']/b");

    private final XPathExpression HERO_ADVENTURE_LIST = XPathHelper.compileXpathExpression("//div[@id='adventureListForm']/table/tbody/tr[contains(@id,'adventure')]");
    private final XPathExpression HERO_ADVENTURE_KID_WAIT_TIME = XPathHelper.compileXpathExpression("td[@class='location']/input[@name='adventureWalktimeOriginalVillage[]']/@value");
    private final XPathExpression HERO_ADVENTURE_DIFFICULTY_TIME = XPathHelper.compileXpathExpression("td[@class='difficulty']/img/@class");

    private final String BUILDINGS_START_FLAG = "window.location.href='/build.php?id=";

    private final XPathExpression BUILDINGS_XPATH = XPathHelper.compileXpathExpression("//a[contains(@href,'?id=') and parent::div[contains(@id,'resourceFieldContainer')]]");
    private final XPathExpression BUILDING_SLOT_ID_XPATH = XPathHelper.compileXpathExpression("./@href");
    private final XPathExpression BUILDING_ID_XPATH = XPathHelper.compileXpathExpression("./@class");

    private final XPathExpression CENTER_BUILDING_UNDER_CONSTRUCTION_XPATH = XPathHelper.compileXpathExpression("./*/@class");//"./div/@class"

    private final XPathExpression CENTER_BUILDINGS_XPATH = XPathHelper.compileXpathExpression("//*[contains(@class, 'buildingSlot') and not(contains(@class, 'top')) and div]");
    private final XPathExpression BUILDING_LVL_XPATH = XPathHelper.compileXpathExpression("following-sibling::div[@class='labelLayer']");
    private final XPathExpression BUILDING_CENTER_LVL_XPATH = XPathHelper.compileXpathExpression(".//div[@class='labelLayer']");

    // *[@class='value']
    private final XPathExpression RESOURCE_AMOUNT_CONTAINER_XPATH = XPathHelper.compileXpathExpression("//div[@id='stockBar']//*[contains(@class,'value')]");
    private final XPathExpression RESOURCE_PRODUCTION_CONTAINER_XPATH = XPathHelper.compileXpathExpression("//*[@id='production']//*[@class='num']");
    private final XPathExpression BUILDING_UPDATE_BUTTON = XPathHelper.compileXpathExpression("//*[contains(@class,'green build')]");
    private final XPathExpression BUILDING_SLOT_FORM_C_ID_XPATH = XPathHelper.compileXpathExpression("//*[contains(@class,'green build') or contains(@class,'green new')]/@onclick");

    private final XPathExpression HERO_HAS_NEW_ADVENTURE = XPathHelper.compileXpathExpression("//div[@id='topBarHero']/a[@href='/hero/adventures']/following-sibling::div[contains(@class,'content')]");

    private TravianConfiguration travianConfiguration;
    private ResourceBundle resourceBundle;
    private Map<Integer, List<Level>> buildingLevelMap;

    public ResponseParser(TravianConfiguration travianConfiguration, ResourceBundle resourceBundle, Map<Integer, List<Level>> buildingLevelMap) {
        this.travianConfiguration = travianConfiguration;
        this.resourceBundle = resourceBundle;
        this.buildingLevelMap = buildingLevelMap;
    }

    public List<Village> getOwnVillages(String responseHtml) {
        List<Village> villageList = new ArrayList<>();
        Document doc = XPathHelper.responseAsDocument(responseHtml);
        try {
            NodeList villages = (NodeList) OWN_VILLAGES_XPATH.evaluate(doc, XPathConstants.NODESET);
            logger.trace("Available villages: " + villages.getLength());
            for (int i = 0; i < villages.getLength(); i++) {
                Node villageNode = villages.item(i);
                String villageId = (String) VILLAGE_ID.evaluate(villageNode, XPathConstants.STRING);
                String villageName = (String) VILLAGE_NAME_XPATH.evaluate(villageNode, XPathConstants.STRING);
                Node villageCoordnates = (Node) VILLAGE_COORDINATES_XPATH.evaluate(villageNode, XPathConstants.NODE);
                String villageCoordnateX = (String) VILLAGE_COORDINATE_X_XPATH.evaluate(villageCoordnates, XPathConstants.STRING);
                String villageCoordnateY = (String) VILLAGE_COORDINATE_Y_XPATH.evaluate(villageCoordnates, XPathConstants.STRING);
                Nation nation = null;
                try {
                    String villageIdValue = villageId.replace("?newdid=", "").split("&")[0].trim();
                    int villageCoordinateXValue = parseCoordinate(villageCoordnateX);
                    int villageCoordinateYValue = parseCoordinate(villageCoordnateY);

                    Village village = Village.builder(villageIdValue, new Coordinates(villageCoordinateXValue, villageCoordinateYValue), travianConfiguration.getNation(villageIdValue))
                            .dname(villageName).build();
                    villageList.add(village);

                } catch (NumberFormatException e) {
                    logger.error(e.getMessage(), e);
                }
            }

        } catch (XPathExpressionException e) {
            logger.error(e.getMessage(), e);
        }
        return villageList;
    }

    public String getActiveVillageId(String responseHtml) {
        Document doc = XPathHelper.responseAsDocument(responseHtml);
        try {
            String activeId = (String) VILLAGE_ACTIVE_ID_XPATH.evaluate(doc, XPathConstants.STRING);
            String result = DefaultParamParser.findVillageActiveId(activeId);
            logger.trace("Active village id: " + result);
            return result;
        } catch (XPathExpressionException e) {
            logger.error(e.getMessage(), e);
        } catch (TravianException e) {
            logger.error(e.getMessage(), "");
        }
        return null;
    }

    public long villageBuildingDashboardWaitTimestamp(String responseHtml) {
        Document doc = XPathHelper.responseAsDocument(responseHtml);
        try {
            /*List<Long> timeouts = new ArrayList<>();
            NodeList timers = (NodeList)XPathHelper.compileXpathExpression("//div[@class='buildingList']/ul//li/div[@class='buildDuration']/span[@class='timer']/@value").evaluate(doc, XPathConstants.NODESET);
            if(timers!=null && timers.getLength() > 0) {
                for (int i = 0; i < timers.getLength(); i++) {
                    Node item = timers.item(i);
                    timeouts.add(new Timestamp(Long.parseLong(item.getTextContent()) * 1000).getTime());
                }
            }*/
            String timestamp = (String) BUILDING_DASHBOARD_WAIT_TIME.evaluate(doc, XPathConstants.STRING);
            try {
                if (!timestamp.isEmpty()) {
                    //String text = (String) XPathHelper.compileXpathExpression("//div[@class='buildingList']/ul/li/div[@class='buildDuration']/span[@class='timer']/text()").evaluate(doc, XPathConstants.STRING);
                    //logger.debug("villageBuildingDashboardWaitTimestamp: " + text);
                    return new Timestamp(Long.parseLong(timestamp) * 1000).getTime();
                }
            } catch (NumberFormatException e) {
                logger.error("", e);
            }
        } catch (XPathExpressionException e) {
            logger.error("", e);
        }
        return 0;
    }

    public JSONArray villageBuildingItemsInQueue(String responseHtml) {
        /* expected string: [{"stufe":1,"gid":"4","aid":"9"},{"stufe":1,"gid":"3","aid":"11"}] */
        JSONArray array = new JSONArray();
        try {
            String items = DefaultParamParser.findBuildingQueue(responseHtml);
            if (!items.isBlank()) {
                array = new JSONArray(items);
            }
        } catch (TravianException e) {
            logger.info("", e.getMessage());
        } catch (JSONException e) {
            logger.error("", e);
        }

        return array;
    }

    public void updateDashboardAndResourcesAndBuildingsByBuildingOwner(Village village, boolean isResourceBuilding, String responseHtml) {
        village.setResources(getVillageResources(responseHtml));

        village.setBuildingQueueTimeWait(System.currentTimeMillis() + villageBuildingDashboardWaitTimestamp(responseHtml));

        Map<Slot, Building> buildings = isResourceBuilding ? getVillageResourceBuildings(responseHtml) : getVillageCenterBuildings(responseHtml);
        logger.trace("Village: " + village + " building list update:");
        logger.trace("[Begin]-------------------------");
        buildings.forEach((k, v) -> {
            BuildingSlotContainer buildingSlotContainer = village.getBuildingSlotContainer();
            buildingSlotContainer.put(k, v);
            logger.trace(k.toString() + " " + v.toString());
        });
    }

    public Map<Slot, Building> getVillageResourceBuildings(String responseHtml) {
        Map<Slot, Building> buildingMap = new HashMap<>();
        Document doc = XPathHelper.responseAsDocument(responseHtml);
        try {
            NodeList buildings = (NodeList) BUILDINGS_XPATH.evaluate(doc, XPathConstants.NODESET);
            logger.trace("Village's resouce buildings " + buildings.getLength());
            for (int i = 0; i < buildings.getLength(); i++) {
                try {
                    Node buildingNode = buildings.item(i);
                    String buildingSlotIdString = (String) BUILDING_SLOT_ID_XPATH.evaluate(buildingNode, XPathConstants.STRING);
                    //String buildingIdEndFlag = "'";
                    Integer buildingSlotId = Integer.valueOf(buildingSlotIdString.substring("/build.php?id=".length())); //Integer.valueOf(buildingSlotIdString.substring(BUILDINGS_START_FLAG.length(), buildingSlotIdString.length() - buildingIdEndFlag.length()));
                    String buildingId = (String) BUILDING_ID_XPATH.evaluate(buildingNode, XPathConstants.STRING);
                    Matcher matcher = Pattern.compile(".+ gid(\\d{1,2}).+").matcher(buildingId);//or use substringAfterLast
                    matcher.find();
                    Integer buildingGidValue = Integer.valueOf(matcher.group(1));
                    String buildingName = resourceBundle.getString(buildingGidValue.toString());
                    String buildingLvlValue = (String) BUILDING_LVL_XPATH.evaluate(buildingNode, XPathConstants.STRING);
                    if (StringUtils.isEmpty(buildingLvlValue)) {
                        buildingLvlValue = "0";
                    }
                    int extraLvl = buildingId.contains("underConstruction") ? 1 : 0; //@class = "notNow level colorLayer gid2 buildingSlot6 underConstruction level4"
                    Integer lvl = Integer.valueOf(buildingLvlValue) + extraLvl;
                    Level currentLevel = Level.createZeroLvl();
                    if (lvl > 0) {
                        currentLevel = buildingLevelMap.get(buildingGidValue).stream()
                                .filter(x -> lvl.equals(x.getNumber())).findFirst().get();
                    }
                    Building building = new Building(buildingGidValue, buildingName, currentLevel, false);
                    buildingMap.put(new Slot(buildingSlotId), building);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        } catch (XPathExpressionException e) {
            logger.error(e.getMessage(), e);
        }
        return buildingMap;
    }

    public Map<Slot, Building> getVillageCenterBuildings(String responseHtml) {
        Map<Slot, Building> buildingMap = new HashMap<>();
        Document doc = XPathHelper.responseAsDocument(responseHtml);
        try {
            NodeList buildings = (NodeList) CENTER_BUILDINGS_XPATH.evaluate(doc, XPathConstants.NODESET);
            logger.trace("Village inner buildings " + buildings.getLength());
            //collect only slots with existing buildings
            for (int i = 0; i < buildings.getLength(); i++) {
                Node buildingNode = buildings.item(i);
                String buildingId = (String) BUILDING_ID_XPATH.evaluate(buildingNode, XPathConstants.STRING);
                Matcher matcher = Pattern.compile(".+a(\\d{1,2}) g(\\d{1,2}).+").matcher(buildingId);
                matcher.find();
                Integer buildingSlotIdValue = Integer.valueOf(matcher.group(1));
                Integer buildingIdValue = Integer.valueOf(matcher.group(2));
                String buildingName = resourceBundle.getString(buildingIdValue.toString());
                String buildingLvlValue = (String) BUILDING_CENTER_LVL_XPATH.evaluate(buildingNode, XPathConstants.STRING);

                String extraLvlClass = (String) CENTER_BUILDING_UNDER_CONSTRUCTION_XPATH.evaluate(buildingNode, XPathConstants.STRING);
                // <div class="buildingSlot a35 g17 aid35 roman"><div class="level colorLayer notNow underConstruction aid35 roman" ..></div></div>
                int extraLvl = extraLvlClass.contains("underConstruction") ? 1 : 0;
                Integer lvl = Integer.valueOf(buildingLvlValue) + extraLvl;
                if (buildingLevelMap.get(buildingIdValue) == null) {
                    new TravianException("Add resource cost for building " + buildingIdValue + " of buildings.xml");
                }
                Level currentLevel = buildingLevelMap.get(buildingIdValue).stream()
                        .filter(x -> lvl.equals(x.getNumber())).findFirst().orElse(Level.createZeroLvl());
                Building building = new Building(buildingIdValue, buildingName, currentLevel, false);
                buildingMap.put(new Slot(buildingSlotIdValue), building);
            }
        } catch (XPathExpressionException e) {
            logger.error(e.getMessage(), e);
        }
        return buildingMap;
    }

    public VillageResourceContainer getVillageResources(String responseHtml) {
        VillageResourceContainer container = new VillageResourceContainer();
        Document doc = XPathHelper.responseAsDocument(responseHtml);
        try {
            NodeList resources = (NodeList) RESOURCE_AMOUNT_CONTAINER_XPATH.evaluate(doc, XPathConstants.NODESET);
            //warehouse capacity list's position is 0
            if (resources != null && resources.getLength() != 0) {
                String warehouse = resources.item(0).getTextContent();
                //granary capacity list's position is 4
                String granary = resources.item(4).getTextContent();
                String freeCropAmountString = resources.item(6).getTextContent();

                String woodAmountString = resources.item(1).getTextContent();
                String clayAmountString = resources.item(2).getTextContent();
                String ironAmountString = resources.item(3).getTextContent();
                String cropAmountString = resources.item(5).getTextContent();

                int warehouseCapacity = parseAmount(warehouse);
                int granaryCapacity = parseAmount(granary);
                int freeCrop = parseAmount(freeCropAmountString);

                container.setWarehouseCapacity(warehouseCapacity);
                container.setGranaryCapacity(granaryCapacity);
                container.setFreeCrop(freeCrop);

                int woodAmount = parseAmount(woodAmountString);
                int clayAmount = parseAmount(clayAmountString);
                int ironAmount = parseAmount(ironAmountString);
                int cropAmount = parseAmount(cropAmountString);

                NodeList production = (NodeList) RESOURCE_PRODUCTION_CONTAINER_XPATH.evaluate(doc, XPathConstants.NODESET);
                if (production.getLength() != 0) {
                    String woodProductionString = production.item(0).getTextContent();
                    String clayProductionString = production.item(1).getTextContent();
                    String ironProductionString = production.item(2).getTextContent();
                    String cropProductionString = production.item(3).getTextContent();

                    int woodProduction = parseAmount(woodProductionString);
                    int clayProduction = parseAmount(clayProductionString);
                    int ironProduction = parseAmount(ironProductionString);
                    int cropProduction = parseAmount(cropProductionString);
                    container.updateResources(woodAmount, clayAmount, ironAmount, cropAmount, woodProduction, clayProduction, ironProduction, cropProduction);
                } else {
                    container.updateResources(woodAmount, clayAmount, ironAmount, cropAmount);
                }
            } else {
                logger.error("Can't parse resources: " + responseHtml);
            }
        } catch (XPathExpressionException e) {
            logger.error(e.getMessage(), e);
        }
        return container;
    }

    public boolean isBuildingUpgradeAvailable(String responseHtml) {
        Document doc = XPathHelper.responseAsDocument(responseHtml);
        try {
            Node buildingNode = (Node) BUILDING_UPDATE_BUTTON.evaluate(doc, XPathConstants.NODE);
            return buildingNode != null;

        } catch (XPathExpressionException e) {
            logger.error(e.getMessage(), e);
        }

        return false;
    }

    public String findQueryParamC(String responseHtml) {
        Document doc = XPathHelper.responseAsDocument(responseHtml);
        try {
            String formValueC = (String) BUILDING_SLOT_FORM_C_ID_XPATH.evaluate(doc, XPathConstants.STRING);
            return DefaultParamParser.findC(formValueC);
        } catch (XPathExpressionException e) {
            logger.error(e.getMessage(), e);
        } catch (TravianException e) {
            logger.trace(e.getMessage());
        }
        return null;
    }

    private int parseCoordinate(String villageCoordinate) {
        villageCoordinate = villageCoordinate.replace("−", "-");
        return Integer.parseInt(CharMatcher.INVISIBLE.removeFrom(villageCoordinate).replace("(", "").replace(")", ""));
    }

    private int parseAmount(String resourceStringAmount) {
        resourceStringAmount = resourceStringAmount.replaceAll("\\u202c", "").replaceAll("\\u202d", "").replaceAll("\\s", "");
        resourceStringAmount = resourceStringAmount.replace("−", "-");//'−' 8722
        return Integer.parseInt(CharMatcher.INVISIBLE.removeFrom((CharSequence) resourceStringAmount));
    }

    @Deprecated
    public Troops getVillageTroops(String villageResource) {
        throw new IllegalStateException("Not implemented");
    }


    public long travianPlusActiveTimestamp(String timestamp) throws Exception {
        return waitTimestamp(timestamp, "Travian Plus timestamp does not found.");
    }

    public long heroAdventureWaitTimestamp(String timestamp) throws Exception {
        return waitTimestamp(timestamp, "Adventure timestamp does not found.");
    }

    public long enemyMovementWaitTimestamp(String timestamp) throws Exception {
        return waitTimestamp(timestamp, "Can't parse enemy movement timer for timestamp");
    }

    public long waitTimestamp(String timestamp, String errorMessage) throws Exception {
        try {
            if (!timestamp.isBlank()) {
                return new Timestamp(Long.parseLong(timestamp) * 1000).getTime();
            }
            throw new TravianException(errorMessage);
        } catch (NumberFormatException e) {
            logger.error("", e);
        }
        return 0;
    }

    public TravianPlusContainer updateTravianPlus(TravianPlusContainer container, String responseHtml) {
        if (container != null) {
            long timestamp = 0;
            Document doc = XPathHelper.responseAsDocument(responseHtml);
            try {
                Node travianPlus = (Node) TRAVIAN_PLUS_STATUS_NOW.evaluate(doc, XPathConstants.NODE);
                if (travianPlus != null) {
                    String timer = (String) TRAVIAN_PLUS_TILL_TIME.evaluate(travianPlus, XPathConstants.STRING);
                    if (timer != null) {
                        if (!timer.isEmpty()) {
                            timestamp = travianPlusActiveTimestamp(timer);
                        } else {
                            timestamp = TimeUnit.HOURS.toMillis(24);
                        }
                    }
                }
            } catch (XPathExpressionException e) {
                logger.error("", e);
            } catch (TravianException e) {
                logger.error("", e);
            } catch (Exception e) {
                logger.error("", e);
            }
            container.setTimestamp(System.currentTimeMillis() + timestamp);
        }
        return container;
    }

    public HeroAdventureContainer updateHeroAdventureStatus(HeroAdventureContainer container, String responseHtml) throws Exception {
        if (container != null) {
            Document doc = XPathHelper.responseAsDocument(responseHtml);
            try {
                Node heroAdventureStatus = (Node) HERO_ADVENTURE_STATUS_NOW.evaluate(doc, XPathConstants.NODE);
                if (heroAdventureStatus != null) {
                    String statusValue = (String) HERO_AT_HOME_NOW.evaluate(heroAdventureStatus, XPathConstants.STRING);
                    Boolean isHeroAtHome = "heroStatus100".equalsIgnoreCase(statusValue);
                    container.setHeroAtHome(isHeroAtHome);

                    if (!isHeroAtHome) {
                        Boolean reinforcement = "heroStatus103".equalsIgnoreCase(statusValue);
                        if (!reinforcement) {
                            String timer = (String) HERO_WAIT_TIME_NOW.evaluate(heroAdventureStatus, XPathConstants.STRING);
                            if (timer != null) {
                                long timestamp = heroAdventureWaitTimestamp(timer);
                                container.setWaitHeroTimestamp(System.currentTimeMillis() + timestamp);
                            }
                        } else {
                            logger.debug("Hero in reinforcement. Status: " + statusValue);
                            //TODO: uncomment it after test!
                            /*
                            TimeUnit timeUnit = travianConfiguration.getHeroAdventuresTimeUnit();
                            long timeout = travianConfiguration.getHeroAdventuresTimeout();
                            container.setWaitHeroTimestamp(System.currentTimeMillis() + timeUnit.toMillis(timeout));
                            */
                        }
                    }
                } else {
                    logger.error("updateHeroAdventureStatus responseHtml: ", responseHtml);
                }
            } catch (XPathExpressionException e) {
                logger.error("", e);
            } catch (TravianException e) {
                throw e;
            }
        }
        return container;
    }

    public HeroAdventureContainer getHeroAdventures(String responseHtml) throws Exception {
        HeroAdventureContainer container = new HeroAdventureContainer();
        Document doc = XPathHelper.responseAsDocument(responseHtml);
        try {
            updateHeroAdventureStatus(container, responseHtml);
            NodeList adventures = (NodeList) HERO_ADVENTURE_LIST.evaluate(doc, XPathConstants.NODESET);
            if (adventures.getLength() != 0) {
                for (int i = 0; i < adventures.getLength(); i++) {
                    try {
                        Node adventureNode = adventures.item(i);
                        String adventureKidAndTime = (String) HERO_ADVENTURE_KID_WAIT_TIME.evaluate(adventureNode, XPathConstants.STRING);
                        String adventureDifficulty = (String) HERO_ADVENTURE_DIFFICULTY_TIME.evaluate(adventureNode, XPathConstants.STRING);
                        String[] kidAndTime = adventureKidAndTime.split("-");
                        if (kidAndTime.length == 2) {
                            int difficulty = Integer.parseInt(adventureDifficulty.replace("adventureDifficulty", ""));
                            HeroAdventureEntity heroAdventure = new HeroAdventureEntity(heroAdventureWaitTimestamp(kidAndTime[1]), kidAndTime[0], difficulty);
                            container.add(heroAdventure);
                        }
                    } catch (XPathExpressionException e) {
                        logger.error("", e);
                    } catch (NumberFormatException e) {
                        logger.error("", e);
                    }
                }
            }
        } catch (XPathExpressionException e) {
            logger.error("", e);
        }
        return container;
    }

    public int getAvailableMerchants(String responseHtml) {
        Document doc = XPathHelper.responseAsDocument(responseHtml);
        try {
            String availableMerchants = (String) MARKET_MERCHANTS_AVAILABLE_COUNT.evaluate(doc, XPathConstants.STRING);
            return parseAmount(availableMerchants);
        } catch (XPathExpressionException e) {
            logger.error(e.getMessage(), e);
        } catch (NumberFormatException e) {
            logger.error("", e);
        } catch (TravianException e) {
            logger.trace(e.getMessage());
        }
        return 0;
    }

    public int getMerchantCapacity(String responseHtml) {
        Document doc = XPathHelper.responseAsDocument(responseHtml);
        try {
            String capacity = (String) MARKET_MERCHANTS_CAPACITY.evaluate(doc, XPathConstants.STRING);
            return parseAmount(capacity);
        } catch (XPathExpressionException e) {
            logger.error(e.getMessage(), e);
        } catch (NumberFormatException e) {
            logger.error("", e);
        } catch (TravianException e) {
            logger.trace(e.getMessage());
        }
        return 0;
    }

    public static void sleep(long timeout, TimeUnit unit) {
        try {
            long millisec = unit.toMillis(timeout);//use minutes as default timeunit cause it's more suitable as visual value
            //--------
            SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT+3"));
            //--------
            logger.info("Start. Sleep time: ~" + unit.toMinutes(timeout) + " minute(s) till " + sdf.format(System.currentTimeMillis() + millisec));
            Thread.sleep(millisec);
            logger.info("End. Sleep time: ~" + unit.toMinutes(timeout) + " minute(s) till " + sdf.format(System.currentTimeMillis() + millisec));
        } catch (InterruptedException e) {
            logger.error("", e);
        }
    }

    public void updateEnemyMovementContainer(MovementContainer container, Village village, String responseHtml) throws Exception {
        Document doc = XPathHelper.responseAsDocument(responseHtml);
        try {
            NodeList attacks = (NodeList) VILLAGE_ATTACK_LIST.evaluate(doc, XPathConstants.NODESET);
            if (attacks.getLength() != 0) {
                for (int i = 0; i < attacks.getLength(); i++) {

                    Node attack = attacks.item(i);
                    Node attackInfoNode = (Node) VILLAGE_ATTACK_INFO.evaluate(attack, XPathConstants.NODE);
                    String time = (String) VILLAGE_ATTACK_TIMER.evaluate(attackInfoNode, XPathConstants.STRING);
                    String arrivalTimeMessage = (String) VILLAGE_ATTACK_ARRIVAL_TIME_VALUE.evaluate(attackInfoNode, XPathConstants.STRING);

                    Node coordinates = (Node) VILLAGE_ATTACK_FROM.evaluate(attack, XPathConstants.NODE);
                    long timestamp = System.currentTimeMillis() + waitTimestamp(time, "Can't parse enemy movement timer for timestamp");
                    MoveEvent event = new MoveEvent(village, timestamp, arrivalTimeMessage, parseCoordinates(coordinates));
                    container.put(village.getVillageId(), event);
                }
            }
        } catch (XPathExpressionException e) {
            logger.error("", e);
        } catch (NumberFormatException e) {
            logger.error("", e);
        }
    }

    public Coordinates parseCoordinates(Node coordinateContainerNode) throws XPathExpressionException {
        Node villageCoordnates = (Node) VILLAGE_COORDINATES_XPATH.evaluate(coordinateContainerNode, XPathConstants.NODE);
        String villageCoordnateX = (String) VILLAGE_COORDINATE_X_XPATH.evaluate(villageCoordnates, XPathConstants.STRING);
        String villageCoordnateY = (String) VILLAGE_COORDINATE_Y_XPATH.evaluate(villageCoordnates, XPathConstants.STRING);
        try {
            int villageCoordinateXValue = parseCoordinate(villageCoordnateX);
            int villageCoordinateYValue = parseCoordinate(villageCoordnateY);
            return new Coordinates(villageCoordinateXValue, villageCoordinateYValue);
        } catch (NumberFormatException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }


    public boolean hasAdventure(String responseHtml) {
        Document doc = XPathHelper.responseAsDocument(responseHtml);
        try {
            String adventure = (String) HERO_HAS_NEW_ADVENTURE.evaluate(doc, XPathConstants.STRING);
            if (adventure != null && adventure.trim() != "") {
                try {
                    return Integer.parseInt(adventure) > 0;
                } catch (NumberFormatException e) {
                    logger.debug("Can't parse adventure count ", e);
                    return false;
                }
            }
        } catch (XPathExpressionException e) {
            logger.error("", e);
        } catch (TravianException e) {
            logger.error("", e);
        } catch (Exception e) {
            logger.error("", e);
        }
        return false;
    }
}
