package by.travian.util;

import by.travian.bean.Nation;
import by.travian.bean.entity.OffenseEntity;
import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.MarketEntity;
import by.travian.bean.entity.village.Coordinates;
import by.travian.bean.task.BuildingTask;
import by.travian.bean.task.MarketTask;
import by.travian.bean.task.OffenseTask;
import by.travian.bean.task.Task;
import by.travian.bean.troop.OffenseType;
import by.travian.bean.troop.TroopAlias;
import by.travian.bean.troop.Troops;
import by.travian.configuration.TravianConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class FileReader {
    public final static Logger logger = LoggerFactory.getLogger(FileReader.class);
    public static String RAID_TARGET_FILE = "raid-targets.txt";
    public static String BUILDING_TASKS_FILE = "building-tasks.txt";
    public static String MARKET_TASKS_FILE = "market-tasks.txt";

    public static LinkedHashSet<Task<OffenseEntity>> read(String fileName, TravianConfiguration travianConfiguration) {
        LinkedHashSet<Task<OffenseEntity>> uniqueTasks = new LinkedHashSet<Task<OffenseEntity>>();
        LinkedHashSet<OffenseEntity> uniqueTargets = new LinkedHashSet<OffenseEntity>();
        Scanner scannner = new Scanner(FileReader.class.getClassLoader().getResourceAsStream(fileName));
        int rowCount = 0;
        int alreadyExist = 0;
        while (scannner.hasNextLine()) {
            rowCount++;
            boolean isFailed = false;
            String row = scannner.nextLine();
            try {
            /*if (row.startsWith("#") && row.contains("end")) {//create unique condition
                logger.info("End of file marker was found");
                break;
            }*/
                if (row.contains("#")) {
                    continue;
                }
                if ("".equals(row.trim())) {
                    continue;
                }
                String[] params = row.trim().split(";");
                String villageId = params[0].trim();
                String[] coordinate = params[1].trim().split(",");
                int x = 0, y = 0;
                try {
                    x = Integer.parseInt(coordinate[0].trim());
                } catch (NumberFormatException e) {
                    logger.error("Can't parse x coordinate(row: " + rowCount + "). ", e);
                    isFailed = true;
                }
                try {
                    y = Integer.parseInt(coordinate[1].trim());
                } catch (NumberFormatException e) {
                    logger.error("Can't parse y coordinate(row: " + rowCount + "). ", e);
                    isFailed = true;
                }

                String[] t = params[2].trim().split(",");
                Troops troops = new Troops();
                for (int i = 0; i < t.length; i++) {
                    try {
                        String[] troopInfo = t[i].split("=");
                        String type = troopInfo[0].trim();
                        int count = Integer.parseInt(troopInfo[1].trim());
                        if (count <= 0) {
                            isFailed = true;
                            logger.error(type + "=" + count + " is incorrect value");
                        } else {
                            troops.updateByAlias(TroopAlias.RequestTroopAlias.createAliasByString(type.toLowerCase()), count);
                        }
                    } catch (NumberFormatException e) {
                        isFailed = true;
                        logger.error("Can't parse row " + rowCount + ":" + e);
                        break;
                    }
                }
                OffenseType offenseType = OffenseType.valueOf(params[3].trim().toUpperCase());

                Long initialDelay = 0L;
                TimeUnit initialDelayTimeUnit = null;
                try {
                    String[] pair = params[4].trim().split("=");
                    initialDelayTimeUnit = TimeUnit.valueOf(pair[0].trim().toUpperCase());
                    initialDelay = Long.parseLong(pair[1].trim());
                } catch (NumberFormatException e) {
                    logger.error("Can't parse initial delay (row: " + rowCount + "). ", e);
                    isFailed = true;
                } catch (IllegalArgumentException e) {
                    logger.error("Can't parse time unit of initial delay (row: " + rowCount + "). ", e);
                    isFailed = true;
                }

                AtomicInteger repeatCount = new AtomicInteger(1);
                try {
                    repeatCount = new AtomicInteger(Integer.parseInt(params[5].trim()));
                } catch (NumberFormatException e) {
                    logger.error("Can't parse repeat count (row: " + rowCount + "). ", e);
                    isFailed = true;
                }

                Long repeatTimeWait = 0L;
                TimeUnit repeatTimeUnit = null;
                try {
                    String[] pair = params[6].trim().split("=");
                    repeatTimeUnit = TimeUnit.valueOf(pair[0].trim().toUpperCase());
                    repeatTimeWait = Long.parseLong(pair[1].trim());
                } catch (NumberFormatException e) {
                    logger.error("Can't parse delay time after repeat (row: " + rowCount + "). ", e);
                    isFailed = true;
                } catch (IllegalArgumentException e) {
                    logger.error("Can't parse time unit of repeat's delay time (row: " + rowCount + "). ", e);
                    isFailed = true;
                }

                if (!isFailed) {
                    Coordinates target = new Coordinates(x, y);
                    Nation nation =  travianConfiguration.getNation(villageId);
                    OffenseEntity entity = OffenseEntity.builder(villageId, target, offenseType, troops, nation).build();
                    if (uniqueTargets.contains(entity)) {
                        alreadyExist++;
                        logger.debug("Already exist:" + row);
                    }
                    uniqueTargets.add(entity);
                    uniqueTasks.add(new Task<OffenseEntity>(initialDelayTimeUnit.toMillis(initialDelay), repeatCount, repeatTimeUnit.toMillis(repeatTimeWait), entity));
                }
            } catch (RuntimeException e) {
                logger.error("File has incorrect data (row: " + rowCount + "). Data:" + row, e);
            }
        }
        logger.debug("------File is loaded [" + fileName + "]------");
        logger.debug("Total raid targets count: " + uniqueTargets.size() + " repeated: " + alreadyExist);
        scannner.close();
        return uniqueTasks;
    }

    /*public static LinkedHashSet<TroopTask> fillDefaultDelayIfRepeatCountMoreThanOne(LinkedHashSet<TroopTask> list) {
        for (TroopTask task : list) {
            if(task.getRepeat().get() > 1){
                int time = 0;
                task.getEntity().get ...
                task.setRepeatTimeWait(time);
            }
        }
        return list;
    }*/

    public static void main(String[] args) {
        System.out.println(FileReader.readBuildingTasks());
    }


    public static List<BuildingTask> readBuildingTasks() {
        String fileName = FileReader.BUILDING_TASKS_FILE;
        String villageId = "";
        return readBuildingTasks(villageId, fileName);
    }

    public static List<BuildingTask> readBuildingTasks(String id, String fileName) {
        List<BuildingTask> tasks = new ArrayList<BuildingTask>();
        Scanner scannner = new Scanner(FileReader.class.getClassLoader().getResourceAsStream(fileName));
        int rowCount = 0;

        while (scannner.hasNextLine()) {
            rowCount++;
            boolean isFailed = false;
            String row = scannner.nextLine();
            if (row.contains("#")) {
                continue;
            }
            if ("".equals(row.trim())) {
                continue;
            }
            String[] params = row.trim().split(";");
            String villageId = !id.isBlank() ? id : params[0].trim();
            if (villageId.isEmpty()) {
                logger.error("VillageId can't be empty (row: " + rowCount + "). ");
                isFailed = true;
            }
            Integer buildingId = -1;
            try {
                buildingId = Integer.parseInt(params[1].trim());
            } catch (NumberFormatException e) {
                logger.error("Can't parse buildingId(row: " + rowCount + "). ", e);
                isFailed = true;
            }
            Integer untilLvlRepeat = -1;
            try {
                untilLvlRepeat = Integer.parseInt(params[2].trim());
            } catch (NumberFormatException e) {
                logger.error("Can't parse untilLvlRepeat(row: " + rowCount + "). ", e);
                isFailed = true;
            }


            Integer slotId = -1;
            try {
                slotId = Integer.parseInt(params[3].trim());
            } catch (NumberFormatException e) {
                logger.error("Can't parse slotId(row: " + rowCount + "). ", e);
                isFailed = true;
            }


            if (!isFailed) {
                BuildingEntity entity = new BuildingEntity();
                entity.setVillageId(villageId);
                entity.setId(buildingId);
                entity.setUntilLvlRepeat(untilLvlRepeat);
                entity.setSlotId(slotId);

                tasks.add(new BuildingTask(entity));
            }
        }
        logger.debug("------File is loaded [" + fileName + "]------");
        scannner.close();
        return tasks;
    }

    public static LinkedHashSet<MarketTask> readMarketTasks() {
        String fileName = MARKET_TASKS_FILE;
        LinkedHashSet<MarketTask> uniqueTasks = new LinkedHashSet<MarketTask>();
        LinkedHashSet<MarketEntity> uniqueTargets = new LinkedHashSet<MarketEntity>();
        Scanner scannner = new Scanner(FileReader.class.getClassLoader().getResourceAsStream(fileName));
        int rowCount = 0;
        int alreadyExist = 0;
        while (scannner.hasNextLine()) {
            rowCount++;
            boolean isFailed = false;
            String row = scannner.nextLine();

            if (row.contains("#")) {
                continue;
            }
            if ("".equals(row.trim())) {
                continue;
            }
            String[] params = row.trim().split(";");
            String villageId = params[0].trim();
            String[] coordinate = params[1].trim().split(",");
            int x = 0, y = 0;
            try {
                x = Integer.parseInt(coordinate[0].trim());
            } catch (NumberFormatException e) {
                logger.error("Can't parse x coordinate(row: " + rowCount + "). ", e);
                isFailed = true;
            }
            try {
                y = Integer.parseInt(coordinate[1].trim());
            } catch (NumberFormatException e) {
                logger.error("Can't parse y coordinate(row: " + rowCount + "). ", e);
                isFailed = true;
            }

            String[] r = params[2].trim().split(",");
            int wood = 0, clay = 0, iron = 0, crop = 0;
            for (int i = 0; i < r.length; i++) {
                try {
                    String[] resource = r[i].split("=");
                    String type = resource[0].trim();
                    int count = Integer.parseInt(resource[1].trim());
                    if (count < 0) {
                        isFailed = true;
                        logger.error(type + "=" + count + " is incorrect value");
                    } else {
                        if ("wood".equalsIgnoreCase(type)) {
                            wood = count;
                        }
                        if ("clay".equalsIgnoreCase(type)) {
                            clay = count;
                        }
                        if ("iron".equalsIgnoreCase(type)) {
                            iron = count;
                        }
                        if ("crop".equalsIgnoreCase(type)) {
                            crop = count;
                        }
                    }
                } catch (NumberFormatException e) {
                    isFailed = true;
                    logger.error("Can't parse row " + rowCount + ":" + e);
                    break;
                }
            }
            Long initialDelay = 0L;
            TimeUnit initialDelayTimeUnit = null;
            try {
                String[] pair = params[3].trim().split("=");
                initialDelayTimeUnit = TimeUnit.valueOf(pair[0].trim().toUpperCase());
                initialDelay = Long.parseLong(pair[1].trim());
            } catch (NumberFormatException e) {
                logger.error("Can't parse initial delay (row: " + rowCount + "). ", e);
                isFailed = true;
            } catch (IllegalArgumentException e) {
                logger.error("Can't parse time unit of initial delay (row: " + rowCount + "). ", e);
                isFailed = true;
            }

            AtomicInteger repeatCount = new AtomicInteger(1);
            try {
                repeatCount = new AtomicInteger(Integer.parseInt(params[4].trim()));//4
            } catch (NumberFormatException e) {
                logger.error("Can't parse repeat count (row: " + rowCount + "). ", e);
                isFailed = true;
            }

            Long repeatTimeWait = 0L;
            TimeUnit repeatTimeUnit = null;
            try {
                String[] pair = params[5].trim().split("=");
                repeatTimeUnit = TimeUnit.valueOf(pair[0].trim().toUpperCase());
                repeatTimeWait = Long.parseLong(pair[1].trim());
            } catch (NumberFormatException e) {
                logger.error("Can't parse delay time after repeat (row: " + rowCount + "). ", e);
                isFailed = true;
            } catch (IllegalArgumentException e) {
                logger.error("Can't parse time unit of repeat's delay time (row: " + rowCount + "). ", e);
                isFailed = true;
            }

            if (!isFailed) {
                Coordinates target = new Coordinates(x, y);
                MarketEntity entity = MarketEntity.builder(villageId, target).wood(wood).clay(clay).iron(iron).crop(crop).build();
                if (uniqueTargets.contains(entity)) {
                    alreadyExist++;
                    logger.debug("Already exist:" + row);
                }
                uniqueTargets.add(entity);
                uniqueTasks.add(new MarketTask(initialDelayTimeUnit.toMillis(initialDelay), repeatCount, repeatTimeUnit.toMillis(repeatTimeWait), entity));
            }
        }
        logger.debug("------File is loaded [" + fileName + "]------");
        logger.debug("Total market tasks: " + uniqueTargets.size() + " repeated: " + alreadyExist);
        scannner.close();
        return uniqueTasks;
    }
}
