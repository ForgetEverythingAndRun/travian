package by.travian.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import static by.travian.util.XPathHelper.responseAsDocument;

/**
 * Created by Администратор on 28.10.2017.
 */
public enum ExtractMainPageHeaderUtil {
    REPORT("reports"), MESSAGE("messages");
    private String cssClass;

    ExtractMainPageHeaderUtil(String cssClass) {
        this.cssClass = cssClass;
    }

    public String getCssClass() {
        return cssClass;
    }

    /* Example:
       title="Сообщения||Новых сообщений: 0"
       title="Сообщения||Новых отчетов: 10"
       * */
    public static int getHeaderUpdate(ExtractMainPageHeaderUtil info, String responseHtml) {
        Logger logger = LoggerFactory.getLogger(ExtractMainPageHeaderUtil.class);
        Document doc = responseAsDocument(responseHtml);
        try {
            String expression = "//div[@id='navigation']/a[@class='" + info.getCssClass() + "']/@title";
            //old
            //String expression = "//ul[@id='navigation']/li[@class='" + info.getCssClass() + "']/a/@title";
            String reportInfo = (String) XPathHelper.compileXpathExpression(expression).evaluate(doc, XPathConstants.STRING);
            try {
                logger.info(reportInfo);
                int lastWhiteSpaceIndex = reportInfo.lastIndexOf(" ");
                String countValue = reportInfo.substring(lastWhiteSpaceIndex + 1);
                return Integer.parseInt(countValue);
            } catch (NumberFormatException e) {
                logger.error("",e);
                logger.error(responseHtml);
            }
        } catch (XPathExpressionException e) {
            logger.error("",e);
        }
        return 0;
    }
}