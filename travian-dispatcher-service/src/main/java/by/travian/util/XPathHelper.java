package by.travian.util;

import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;


/**
 * Created by Администратор on 01.05.2017.
 */
public class XPathHelper {
    public final static Logger logger = LoggerFactory.getLogger(XPathHelper.class);
    private static final XPathFactory XPATH_FACTORY = XPathFactory.newInstance();
    private static final XPath XPATH = XPATH_FACTORY.newXPath();

    private XPathHelper() {
        super();
    }

    public static XPathExpression compileXpathExpression(String xpath) {
        try {
            return XPATH.compile(xpath);
        } catch (XPathExpressionException e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    public static Document responseAsDocument(String responseHtml) {
        TagNode tagNode = new HtmlCleaner().clean(responseHtml);
        Document doc = null;
        try {
            doc = new DomSerializer(
                    new CleanerProperties()).createDOM(tagNode);
        } catch (ParserConfigurationException e) {
            logger.error(e.getMessage());
        }
        return doc;
    }

}
