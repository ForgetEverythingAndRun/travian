package by.travian.util.operation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by Администратор on 10.12.2017.
 */
public class OffensiveOperation {
    private final static TimeZone timeZone = TimeZone.getTimeZone("GMT+3");
    private final static SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");

    static {
        sdf.setTimeZone(timeZone);
    }

    public static long operationTime(String input) {
        try {
            return sdf.parse(input).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            throw new RuntimeException("Operation time input is wrong");
        }
    }


    public static String millisecAsString(long millisec){
        return sdf.format(millisec);
    }

    private String label;
    private long startTime;
    private long arrivalTime;
    private int hours;
    private int minutes;
    private int seconds;


    public OffensiveOperation(String label, long startTime, int hours, int minutes, int seconds) {
        this.label = label;
        this.startTime = startTime;
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
        this.arrivalTime = arriveTime(hours, minutes, seconds);
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    private long arriveTime(int hours, int munutes, int seconds) {
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setTimeInMillis(startTime);
        calendar.add(Calendar.HOUR, hours);
        calendar.add(Calendar.MINUTE, munutes);
        calendar.add(Calendar.SECOND, seconds);
        return calendar.getTimeInMillis();
    }


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getArrivalTime() {
        return arrivalTime;
    }

    @Override
    public String toString() {
        return label + ": " + millisecAsString(getStartTime());
    }
}
