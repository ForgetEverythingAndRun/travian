package by.travian.util.operation;

/**
 * Created by Администратор on 11.12.2017.
 */
public class TestOperation {
    public static void main(String[] args) {
        //Forsted("2017-12-11 01:10:00");
        Regber1("2017-12-11 01:30:00");
        //Svolochmax("2017-12-11 00:10:00");
    }

    public static void Regber1(String planningDate){
        OffensiveOperationManager manager = new OffensiveOperationManager();
        long operationTime = OffensiveOperation.operationTime(planningDate);
        manager.add(new OffensiveOperation("Корма(каты+офф) to Winterfell(-146,123)", operationTime, 4, 12, 41));
        manager.add(new OffensiveOperation("Корма(говорун без кат) to Winterfell(-146,123)", operationTime, 2, 31, 37));
        manager.add(new OffensiveOperation("Чечерск(говорун без кат) to Winterfell(-146,123)", operationTime, 6, 4, 58));
        manager.add(new OffensiveOperation("Коротьки(говорун без кат) to Winterfell(-146,123)", operationTime, 6,48,11));

        for (OffensiveOperation operation : manager.calculate()) {
            System.out.println(operation);
        }
        manager.operationEnd();
    }

    public static void Forsted(String planningDate){
        OffensiveOperationManager manager = new OffensiveOperationManager();
        long operationTime = OffensiveOperation.operationTime(planningDate);
        manager.add(new OffensiveOperation("Корма(каты+офф) to 13(НЕТ)(-160,109)", operationTime, 2,45, 32));
        manager.add(new OffensiveOperation("Корма(говорун без кат) to 13(НЕТ)(-160,109)", operationTime, 1, 39, 19));
        manager.add(new OffensiveOperation("Чечерск(говорун без кат) to 13(НЕТ)(-160,109)", operationTime, 2, 7, 34));
        manager.add(new OffensiveOperation("Коротьки(говорун без кат) to 13(НЕТ)(-160,109)", operationTime, 9,56,45));

        for (OffensiveOperation operation : manager.calculate()) {
            System.out.println(operation);
        }
        manager.operationEnd();
    }

    public static void Svolochmax(String planningDate){
        OffensiveOperationManager manager = new OffensiveOperationManager();
        long operationTime = OffensiveOperation.operationTime(planningDate);
        //spam
        manager.add(new OffensiveOperation("Минск(фаланга спам) to SvoCity(-130,104)", operationTime, 3, 31, 35));
        manager.add(new OffensiveOperation("Минск(фаланга спам) to Svoburg(-134,98)", operationTime, 3, 44, 30));

        manager.add(new OffensiveOperation("Минск(мечи спам) to SvoCity(-130,104)", operationTime, 4, 7, 14));
        manager.add(new OffensiveOperation("Минск(мечи спам) to Svoburg(-134,98)", operationTime, 4, 21, 55));
        //waves
        manager.add(new OffensiveOperation("Корма(каты+офф) to SvoGrad(-142,98)", operationTime, 4, 4, 8));
        manager.add(new OffensiveOperation("Корма(говорун без кат) to SvoGrad(-142,98)", operationTime, 2, 26, 29));
        manager.add(new OffensiveOperation("Чечерск(говорун без кат) to SvoGrad(-142,98)", operationTime, 5, 15, 40));
        manager.add(new OffensiveOperation("Коротьки(говорун без кат) to SvoGrad(-142,98)", operationTime, 11,50,32));

        for (OffensiveOperation operation : manager.calculate()) {
            System.out.println(operation);
        }
        manager.operationEnd();
    }
}
