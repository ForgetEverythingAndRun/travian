package by.travian.util.operation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Администратор on 10.12.2017.
 */
public class OffensiveOperationManager {
    private List<OffensiveOperation> planningOperations = new ArrayList<OffensiveOperation>();
    private long operationEnd;

    public void operationEnd() {
        System.out.println("End operation: " + OffensiveOperation.millisecAsString(operationEnd));
    }


    public void add(OffensiveOperation operation) {
        planningOperations.add(operation);
    }

    public List<OffensiveOperation> calculate() {
        Collections.sort(planningOperations, new DescArrivalTimeComparator());
        operationEnd = planningOperations.get(0).getArrivalTime();
        List<OffensiveOperation> calculatedOperations = new ArrayList<OffensiveOperation>();
        for (OffensiveOperation operation : planningOperations) {
            calculatedOperations.add(fitOffensiveOperation(operation, operationEnd));
        }
        Collections.sort(calculatedOperations, new StartOperationComparator());
        return calculatedOperations;
    }

    public static OffensiveOperation fitOffensiveOperation(OffensiveOperation operation, long maxArrival) {
        return new OffensiveOperation(operation.getLabel(), operation.getStartTime() + (maxArrival - operation.getArrivalTime()), operation.getHours(), operation.getMinutes(), operation.getSeconds());
    }



}
