package by.travian.util.operation;

import java.util.Comparator;

/**
 * Created by Администратор on 10.12.2017.
 */
public class DescArrivalTimeComparator implements Comparator<OffensiveOperation> {

    @Override
    public int compare(OffensiveOperation o1, OffensiveOperation o2) {
        return Long.compare(o2.getArrivalTime(), o1.getArrivalTime());
    }
}
