package by.travian.runner;

import by.travian.bean.Nation;
import by.travian.error.TravianException;
import io.vavr.control.Try;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class Test {
    public final static Logger logger = LoggerFactory.getLogger(Test.class);

    static List<String> update(String... elements) {
        return Arrays.asList(Optional.ofNullable(elements).orElseThrow(TravianException::new));
    }

    static Map<String, Nation> villageNationMap = null;

    static Nation getNationDefault = Nation.GAULS;
    static String villageBindNationIds = "111; 123 : romans ;  666 :gauls; 999: germans; ";//    GAULS, ROMANS, GERMANS

    public static Nation getNation(String id) {
        if (villageNationMap == null) {
            villageNationMap = new HashMap<>();
            List<String> list = Arrays.asList(villageBindNationIds.trim().split(";"));
            for (String item : list) {
                List<String> pair = Arrays.asList(item.trim().split(":"));
                String villageId = pair.get(0).trim();
                Nation nation = Try.of(() -> Nation.valueOf(pair.get(1).trim().toUpperCase())).getOrElse(getNationDefault);
                villageNationMap.put(villageId, nation);
            }
        }
        return Optional.ofNullable(villageNationMap.get(id)).orElse(getNationDefault);
    }

    public static void main(String[] args) {


     //System.out.println(a.equals(b));
        /*for (String e : Arrays.asList("123", "666", "999", "000","111")) {
            System.out.println(e + " = " + getNation(e));

        }*/

        /*@Data
        @AllArgsConstructor
        class Filter {
            String key;
            List<String> values;
        }
        List<String> list2 = Stream.of(new String("abc"), new String("zxc"), new String("qwe"), new String("zxc")).collect(Collectors.toList());
        Map<String, String> data = new HashMap<>();
        data.put("key1", "value3");
        data.put("key2", "value22");
        data.put("key4", null);//
        data.put("key3", "value111");

        List<Filter> filters = Stream.of(
                new Filter("key1", Arrays.asList("value1", "value3", "value2")),
                new Filter("key2", Arrays.asList("value11", "value33", "value22")),
                new Filter("key3", Arrays.asList("value111", "value333", "value222"))
        ).collect(Collectors.toList());

        boolean allKeysMatched = filters.stream().allMatch(filter -> Objects.nonNull(data.get(filter.getKey())));
        boolean valuesByKeyMatched = filters.stream().allMatch(filter -> filter.getValues().contains(data.get(filter.getKey())));
        System.out.println("allKeysMatched: " + allKeysMatched);
        System.out.println("valuesByKeyMatched: " + valuesByKeyMatched);

        Predicate<Filter> predicate = (filter) -> {
            return Objects.nonNull(data.get(filter.getKey())) && filter.getValues().contains(data.get(filter.getKey()));
        };
            boolean test = filters.stream().allMatch(predicate);*/
    }
}
