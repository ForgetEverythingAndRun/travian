package by.travian.runner;

import by.travian.service.TravianServiceFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


@Order(1)
@Component
public class ApplicationRunnerImpl implements ApplicationRunner {
    @Autowired
    private TravianServiceFacade travianServiceFacade;
    @Autowired
    ApplicationContext context;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        travianServiceFacade.execute(args);
    }


}
