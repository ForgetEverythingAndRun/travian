package by.travian.error;

public class BuildingExtraDependencyException extends Exception{
    public BuildingExtraDependencyException() {
    }

    public BuildingExtraDependencyException(String message) {
        super(message);
    }

    public BuildingExtraDependencyException(String message, Throwable cause) {
        super(message, cause);
    }

    public BuildingExtraDependencyException(Throwable cause) {
        super(cause);
    }

    public BuildingExtraDependencyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
