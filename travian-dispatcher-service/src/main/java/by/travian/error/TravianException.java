package by.travian.error;

/**
 * Created by Viktar_Kapachou on 2/8/2017.
 */
public class TravianException extends RuntimeException {
    public TravianException(String message) {
        super(message);
    }

    public TravianException(Throwable cause) {
        super(cause);
    }

    public TravianException() {

    }
}
