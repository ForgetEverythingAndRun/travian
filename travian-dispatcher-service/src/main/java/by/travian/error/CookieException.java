package by.travian.error;

/**
 * Created by Администратор on 03.10.2017.
 */
public class CookieException extends TravianException {
    public CookieException(String message) {
        super(message);
    }

}
