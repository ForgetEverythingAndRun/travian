package by.travian.error;

/**
 * Created by Viktar_Kapachou on 11/1/2017.
 */
public class VillageContainerUpdateException extends TravianException{
    public final static String VILLAGE_PARSE_ERROR =  "Can't parse villages";

    public VillageContainerUpdateException() {
        super(VILLAGE_PARSE_ERROR);
    }
}
