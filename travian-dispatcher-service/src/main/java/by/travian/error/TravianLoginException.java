package by.travian.error;

/**
 * Created by Администратор on 11.11.2017.
 */
public class TravianLoginException extends TravianException {
    public TravianLoginException() {
        super("Login or password is wrong");
    }
}
