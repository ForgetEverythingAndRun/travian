package by.travian.error;

/**
 * Created by Администратор on 24.09.2017.
 */
public class SendTroopsException extends TravianException {
    public final static String REQUEST_PARAM_ERROR_MESSAGE  =  "Can't find param value!";
    public final static String TROOPS_PARSE_ERROR  =  "Can't parse troops";
    public final static String ARRIVAL_TIME_PARSE_ERROR  =  "Arrival time parse error";

    public SendTroopsException() {
        super(REQUEST_PARAM_ERROR_MESSAGE);
    }

    public SendTroopsException(String message) {
        super(message);
    }
}
