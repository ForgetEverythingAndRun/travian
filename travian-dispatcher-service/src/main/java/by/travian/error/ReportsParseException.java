package by.travian.error;

/**
 * Created by Администратор on 05.12.2017.
 */
public class ReportsParseException extends TravianException {
    public final static String REPORTS_PARSE_ERROR =  "Can't parse reports.";
    public final static String REPORTS_XPATH_PARSE_ERROR =  "Internal parse issue!";
    public final static String LAST_PAGE_VALUE_PARSE_ERROR =  "Can't parse value of last page.";
    public final static String LAST_PAGE_PARSE_ERROR =  "Last page is reached.";

    public ReportsParseException() {
        super(REPORTS_PARSE_ERROR);
    }

    public ReportsParseException(String message) {
        super(message);
    }

}
