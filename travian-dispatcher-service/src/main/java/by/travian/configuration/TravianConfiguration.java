package by.travian.configuration;

import by.travian.bean.Nation;
import by.travian.bean.container.HeroAdventureContainer;
import by.travian.bean.task.BuildingTask;
import by.travian.util.FileReader;
import io.vavr.control.Try;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import java.awt.*;
import java.lang.reflect.Field;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.List;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;


//@RefreshScope
//TODO: check 4.2 https://www.baeldung.com/spring-reloading-properties
@PropertySources({
        @PropertySource("classpath:retryConfig.properties"),
        @PropertySource("classpath:account.properties")})
@Configuration
@NoArgsConstructor
public class TravianConfiguration {
    private static final String SPLIT_SEPARATOR = ",";

    @Bean(BeanNameConst.HAS_HERO_ADVENTURE)
    public Object hasHeroAdventure() {
        return new AtomicBoolean(false);
    }

    @Bean(BeanNameConst.IS_SHUTDOWN)
    public AtomicBoolean isShutdown() {
        return new AtomicBoolean(false);
    }

    @Bean(BeanNameConst.IS_AUTHENTICATED)
    public AtomicBoolean isAuthenticated() {
        return new AtomicBoolean(false);
    }

    @Bean(BeanNameConst.IS_PAUSED)
    public AtomicBoolean isPaused() {
        return new AtomicBoolean(false);
    }

    @Value("${app.retry.maxAttempts}")
    private Integer maxAttempts;
    @Value("${app.retry.maxDelay}")
    private Long maxDelay;

    @Value("${login}")
    private String login;
    @Value("${password}")
    private String password;
    @Value("${nation.default}")
    private String nationDefault;
    @Value("${nation.specific.village.ids}")
    private String nationSpecificVillageIds;

    private Map<String, Nation> villageNationMap;

    @Value("${discord.token}")
    private String discordToken;
    @Value("${discord.text.channel.name}")
    private String discordTextChannelName;
    @Value("${discord.channel.message.color}")
    private String discordChannelMessageColor;

    @Value("${track.enemy.enabled}")
    private boolean trackEnemyEnabled;
    @Value("${track.enemy.village.ids}")
    private String trackEnemyVillageIds;

    @Value("${market.tasks.enabled}")
    private boolean marketTasksEnabled;

    @Value("${raid.tasks.enabled}")
    private boolean raidEnabled;

    @Value("${building.tasks.enabled}")
    private boolean buildingTasksEnabled;

    @Value("${building.tasks.bind.templates}")
    private String buildingTasksBindTemplates;

    @Value("${auto.building.enabled}")
    private boolean autoBuildingEnabled;
    @Value("${auto.building.village.ids}")
    private String autoBuildingVillageIds;
    @Value("${auto.building.crop.enabled}")
    private boolean autoBuildingCropEnabled;

    @Value("${loop.timeout}")
    private Long loopTimeout;
    @Value("${loop.time.unit}")
    private String loopTimeUnit;

    @Value("${military.buy.mode.enabled}")
    private Boolean militaryBuyModeEnabled;
    @Value("${military.buy.village.ids}")
    private String militaryBuyVillageIds;

    @Value("${hero.adventure.enabled}")
    private boolean heroAdventureEnabled;

    @Value("${hero.adventures.priority:time}")
    private String heroAdventuresPriority;

    @Value("${hero.adventures.sort.order:ascending}")
    private String heroAdventuresSortOrder;

    @Value("${hero.adventures.repeat.timeout}")
    private Long heroAdventuresTimeout;

    @Value("${hero.adventures.repeat.time.unit}")
    private String heroAdventuresTimeUnit;


    @Value("${travian.plus.track.timeout}")
    private Long travianPlusTrackTimeout;

    @Value("${travian.plus.track.time.unit}")
    private String travianPlusTrackTimeUnit;

    @Value("${proxy.enabled}")
    private Boolean proxyEnabled;
    @Value("${proxy.host}")
    private String proxyHost;
    @Value("${proxy.port}")
    private int proxyPort;
    @Value("${proxy.scheme}")
    private String proxyScheme;

    public boolean isTrackEnemyEnabled() {
        return trackEnemyEnabled;
    }

    public List<String> getTrackEnemyVillageIds() {
        return Arrays.asList(trackEnemyVillageIds.split(SPLIT_SEPARATOR));
    }

    public Integer getMaxAttempts() {
        return maxAttempts;
    }

    public Long getMaxDelay() {
        return maxDelay;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public boolean isMarketTasksEnabled() {
        return marketTasksEnabled;
    }

    public boolean isRaidEnabled() {
        return raidEnabled;
    }

    public boolean isBuildingTasksEnabled() {
        return buildingTasksEnabled;
    }

    public boolean isAutoBuildingEnabled() {
        return autoBuildingEnabled;
    }

    public boolean isAutoBuildingCropEnabled() {
        return autoBuildingCropEnabled;
    }

    public Boolean isMilitaryBuyModeEnabled() {
        return militaryBuyModeEnabled;
    }


    public Long getLoopTimeout() {
        return loopTimeout;
    }

    public TimeUnit getLoopTimeUnit() {
        return TimeUnit.valueOf(loopTimeUnit.trim().toUpperCase());
    }

    public List<String> getAutoBuildingVillageIds() {
        return Arrays.asList(autoBuildingVillageIds.split(SPLIT_SEPARATOR));
    }

    public Map<String, String> getMilitaryBuyVillageIds() {
        Map<String, String> map = new HashMap<>();
        List<String> list = Arrays.asList(militaryBuyVillageIds.trim().split(";"));
        for (String item : list) {
            List<String> pair = Arrays.asList(item.trim().split(":"));
            String villageId = pair.get(0).trim();
            List<String> fileNames = new ArrayList<>();
            if (pair.size() == 2) {
                String troopType = pair.get(1).trim();
                if (!troopType.isEmpty() && !villageId.isEmpty()) {
                    map.put(villageId, troopType);
                }
            }
        }
        return map;
    }

    protected Nation getNationDefault() {
        return Nation.valueOf(nationDefault.trim().toUpperCase());
    }

    public Boolean getProxyEnabled() {
        return proxyEnabled;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public String getProxyScheme() {
        return proxyScheme;
    }

    public Long getHeroAdventuresTimeout() {
        return heroAdventuresTimeout;
    }

    public boolean isHeroAdventureEnabled() {
        return heroAdventureEnabled;
    }

    public TimeUnit getHeroAdventuresTimeUnit() {
        TimeUnit timeUnit = TimeUnit.valueOf(heroAdventuresTimeUnit.trim().toUpperCase());
        return timeUnit;
    }

    public HeroAdventureContainer.HeroAdventureComparatorType getHeroAdventuresPriority() {
        return HeroAdventureContainer.HeroAdventureComparatorType
                .valueOf(heroAdventuresPriority.trim().toUpperCase());
    }

    public HeroAdventureContainer.HeroAdventureSortOrder getHeroAdventuresSortOrder() {
        return HeroAdventureContainer.HeroAdventureSortOrder
                .valueOf(heroAdventuresSortOrder.trim().toUpperCase());
    }

    public Map<String, List<String>> getBuildingTaskBindTemplates() {
        Map<String, List<String>> map = new HashMap<>();
        List<String> list = Arrays.asList(buildingTasksBindTemplates.trim().split(";"));
        for (String item : list) {
            List<String> pair = Arrays.asList(item.trim().split(":"));
            String villageId = pair.get(0).trim();
            List<String> fileNames = new ArrayList<>();
            if (pair.size() == 2) {
                for (String fileName : pair.get(1).trim().split(",")) {
                    if (!fileName.trim().isEmpty()) {
                        fileNames.add(fileName.trim());
                    }
                }
            }
            map.put(villageId, fileNames);
        }
        return map;
    }

    public String getDiscordToken() {
        return discordToken;
    }

    @Deprecated
    public void setDefaultAuthenticator() {
        final String proxyUser = System.getProperty("http.proxyUser");
        final String proxyPassword = System.getProperty("http.proxyPassword");
        if (proxyUser != null && proxyPassword != null) {
            Authenticator.setDefault(
                    new Authenticator() {
                        public PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(
                                    proxyUser, proxyPassword.toCharArray());
                        }
                    }
            );
        }
    }

    public String getDiscordTextChannelName() {
        return discordTextChannelName.trim();
    }

    public Color getDiscordChannelMessageColor() {
        Color color = null;
        try {
            Field field = Class.forName("java.awt.Color").getField(discordChannelMessageColor.toUpperCase().trim());
            color = (Color) field.get(null);
        } catch (Exception e) {
            color = Color.BLACK; // Not defined
        }
        return color;
    }

    public Long getTravianPlusTrackTimeout() {
        return travianPlusTrackTimeout;
    }

    public TimeUnit getTravianPlusTrackTimeUnit() {
        TimeUnit timeUnit = TimeUnit.valueOf(travianPlusTrackTimeUnit.trim().toUpperCase());
        return timeUnit;
    }

    public List<BuildingTask> createBuildingTasks() {
        Map<String, List<String>> map = getBuildingTaskBindTemplates();
        List<BuildingTask> allTasks = FileReader.readBuildingTasks();//common file is loaded first
        for (Map.Entry<String, List<String>> entry : map.entrySet()) {
            for (String fileName : entry.getValue()) {
                List<BuildingTask> buildingTasks = FileReader.readBuildingTasks(entry.getKey(), fileName);
                allTasks.addAll(buildingTasks);
            }
        }
        return allTasks;
    }

    public synchronized Nation getNation(String id) {
        if (villageNationMap == null) {
            villageNationMap = new HashMap<>();
            List<String> list = Arrays.asList(nationSpecificVillageIds.trim().split(";"));
            for (String item : list) {
                List<String> pair = Arrays.asList(item.trim().split(":"));
                String villageId = pair.get(0).trim();
                Nation nation = Try.of(() -> Nation.valueOf(pair.get(1).trim().toUpperCase())).getOrElse(getNationDefault());
                if(!villageId.isEmpty()) {
                    villageNationMap.put(villageId, nation);
                }
            }
        }
        return Optional.ofNullable(villageNationMap.get(id)).orElse(getNationDefault());
    }
}
