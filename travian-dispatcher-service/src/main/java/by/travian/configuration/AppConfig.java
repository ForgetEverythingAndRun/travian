package by.travian.configuration;

import by.travian.bean.container.MovementContainer;
import by.travian.bean.container.TravianPlusContainer;
import by.travian.bean.container.VillageContainer;
import by.travian.bean.entity.building.Level;
import by.travian.bean.task.handler.ContextHandler;
import by.travian.bean.task.manager.BuildingTaskManagerImpl;
import by.travian.bean.task.manager.LoginTaskManagerImpl;
import by.travian.bean.task.manager.MarketTaskManagerImpl;
import by.travian.bean.task.manager.OffenseTaskManagerImpl;
import by.travian.common.command.factory.CommandFactory;
import by.travian.common.context.TravianApplicationContext;
import by.travian.executor.HttpExecutor;
import by.travian.observer.ResponseManagerObserver;
import by.travian.observer.ResponseObserverImpl;
import by.travian.service.*;
import by.travian.service.discord.DiscordApplication;
import by.travian.util.JaxbParser;
import by.travian.util.ResponseParser;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicBoolean;

@Configuration
@Import(TravianConfiguration.class)
public class AppConfig {
    private final static String FILE_BUILDINGS = "buildings";
    private final static String MINSK_TIME_ZONE = "GMT+3";
    private final static String DATE_FORMAT_TIME_ZONE = "yyyy-MM-dd HH:mm:ss";

    //@Autowired
    //@Qualifier(BeanNameConst.TRAVIAN_SERVICE)
    private TravianService travianService;
    private DiscordApplication discordApplication;
    private TravianConfiguration travianConfiguration;
    private CookieManager cookieManager;
    private RetryTemplate retryTemplate;
    private RestTemplate restTemplate;
    private TravianOffenseService travianOffenseService;
    private TravianLoginService travianLoginService;
    private TravianMarketService travianMarketService;
    private TravianVillageService travianVillageService;

    @Autowired
    public AppConfig(TravianService travianService, DiscordApplication discordApplication, TravianConfiguration travianConfiguration, CookieManager cookieManager, RetryTemplate retryTemplate, RestTemplate restTemplate, TravianOffenseService travianOffenseService, TravianLoginService travianLoginService, TravianMarketService travianMarketService, TravianVillageService travianVillageService) {
        this.travianService = travianService;
        this.discordApplication = discordApplication;
        this.travianConfiguration = travianConfiguration;
        this.cookieManager = cookieManager;
        this.retryTemplate = retryTemplate;
        this.restTemplate = restTemplate;
        this.travianOffenseService = travianOffenseService;
        this.travianLoginService = travianLoginService;
        this.travianMarketService = travianMarketService;
        this.travianVillageService = travianVillageService;
    }



    @Bean(BeanNameConst.MOVEMENT_CONTAINER)
    public MovementContainer movementContainer() {
        return new MovementContainer();
    }

    @Bean(BeanNameConst.VILLAGE_CONTAINER)
    public VillageContainer villageContainer() {
        return new VillageContainer();
    }

    @Bean(BeanNameConst.TRAVIAN_PLUS_CONTAINER)
    public TravianPlusContainer travianPlusContainer() {
        return new TravianPlusContainer();
    }

    @Bean
    @Lazy
    public ResponseManagerObserver managerObserver() {
        ResponseManagerObserver managerObserver = new ResponseManagerObserver();
        managerObserver.add(responseObserverImpl());
        return managerObserver;
    }

    @Bean
    public HttpExecutor httpExecutor() {
        return new HttpExecutor(restTemplate, cookieManager, managerObserver(), retryTemplate, travianConfiguration);
    }

    @Bean
    public ResponseObserverImpl responseObserverImpl() {
        return new ResponseObserverImpl(travianService, travianConfiguration, villageContainer(), responseParser());

    }

    @Bean
    public ResponseParser responseParser() {
        return new ResponseParser(travianConfiguration, buildingsResourceBundle(), buildingLevelMap());
    }

    @Bean(BeanNameConst.BUILDINGS_RESOURCE_BUNDLE)
    public ResourceBundle buildingsResourceBundle() {
        return ResourceBundle.getBundle(FILE_BUILDINGS, cookieManager.getLocale());// pull locale from cookies
    }

    @Bean(BeanNameConst.TIME_ZONE)
    public TimeZone timeZone() {
        return TimeZone.getTimeZone(MINSK_TIME_ZONE);
    }

    @Bean
    public SimpleDateFormat simpleDateFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_TIME_ZONE);
        sdf.setTimeZone(timeZone());
        return sdf;
    }

    @Bean(BeanNameConst.BUILDING_LEVEL_MAP)
    public Map<Integer, List<Level>> buildingLevelMap() {
        Map<Integer, List<Level>> buildingLevelMap = JaxbParser.unmarshalBuildings().getMap();
        return buildingLevelMap;
    }


    @Bean
    public ContextHandler contextHandler() {
        return new ContextHandler();
    }


    @Bean
    public TravianApplicationContext applicationContext() {
        AtomicBoolean isAuthenticated = travianConfiguration.isAuthenticated();
        return new TravianApplicationContext() {
            @Override
            public AtomicBoolean isAuthenticated() {
                return isAuthenticated;
            }

            @Override
            public TravianConfiguration getTravianConfiguration() {
                return travianConfiguration;
            }

            @Override
            public VillageContainer getVillageContainer() {
                return villageContainer();
            }

            @Override
            public TravianPlusContainer getTravianPlusContainer() {
                return travianPlusContainer();
            }

            @Override
            public MovementContainer getMovementContainer() {
                return movementContainer();
            }

            @Override
            public ContextHandler getContextHandler() {
                return contextHandler();
            }
        };
    }

    @Bean
    public CommandFactory commandFactory() {
        CommandFactory commandFactory = new CommandFactory(applicationContext(), httpExecutor(), responseParser(), discordApplication,
                travianOffenseService,
                travianLoginService,
                travianMarketService,
                travianVillageService);
        return commandFactory;
    }

    @Bean
    public OffenseTaskManagerImpl offenseTaskManager() {
        return new OffenseTaskManagerImpl(commandFactory(), contextHandler());
    }

    @Bean
    public LoginTaskManagerImpl loginTaskManager() {
        return new LoginTaskManagerImpl(commandFactory(), contextHandler());
    }

    @Bean
    public MarketTaskManagerImpl marketTaskManager() {
        return new MarketTaskManagerImpl(commandFactory(), contextHandler());
    }

    @Lazy
    @Bean
    public BuildingTaskManagerImpl buildingTaskManagerImpl() {
        return new BuildingTaskManagerImpl(commandFactory(), contextHandler(), travianConfiguration, travianService);// TODO: remove dependency from travianService
    }
}
