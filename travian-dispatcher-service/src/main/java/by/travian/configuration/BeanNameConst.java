package by.travian.configuration;

public class BeanNameConst {

    public static final String HAS_HERO_ADVENTURE = "HAS_HERO_ADVENTURE";

    public static final String IS_AUTHENTICATED = "IS_AUTHENTICATED";

    public static final String IS_PAUSED = "IS_PAUSED";

    public static final String IS_SHUTDOWN = "IS_SHUTDOWN";

    public static final String DEFAULT_LISTENER_SUPPORT = "DEFAULT_LISTENER_SUPPORT";

    public static final String TRAVIAN_SERVICE = "TRAVIAN_SERVICE";

    public static final String BUILDINGS_RESOURCE_BUNDLE = "BUILDINGS_RESOURCE_BUNDLE";

    public static final String TIME_ZONE = "TIME_ZONE";

    public static final String BUILDING_LEVEL_MAP = "BUILDING_LEVEL_MAP";

    public static final String TRAVIAN_PLUS_CONTAINER = "TRAVIAN_PLUS_CONTAINER";

    public static final String VILLAGE_CONTAINER = "VILLAGE_CONTAINER";

    public static final String MOVEMENT_CONTAINER = "MOVEMENT_CONTAINER";



}
