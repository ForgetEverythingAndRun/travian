package by.travian.configuration;

import by.travian.service.CookieManager;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.listener.RetryListenerSupport;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;


@Configuration
public class RestTemplateConfig {

    private final static String THREAD_NAME_PREFIX = "poolScheduler";
    private final static int THREAD_POOL_SIZE = 50;

    private CloseableHttpClient httpClient;
    private TravianConfiguration travianConfiguration;
    private AtomicBoolean isShutdown;
    private AtomicBoolean isPaused;
    private AtomicBoolean isAuthenticated;

    @Lazy
    @Autowired
    public RestTemplateConfig(CloseableHttpClient httpClient, TravianConfiguration travianConfiguration) {
        this.httpClient = httpClient;
        this.travianConfiguration = travianConfiguration;
        this.isShutdown = travianConfiguration.isShutdown();
        this.isPaused = travianConfiguration.isPaused();
        this.isAuthenticated = travianConfiguration.isAuthenticated();
    }

    //READ: https://www.baeldung.com/spring-retry
    @Bean
    public RetryTemplate retryTemplate() {
        RetryTemplate retryTemplate = new RetryTemplate();
        FixedBackOffPolicy fixedBackOffPolicy = new FixedBackOffPolicy();
        fixedBackOffPolicy.setBackOffPeriod(travianConfiguration.getMaxDelay());
        retryTemplate.setBackOffPolicy(fixedBackOffPolicy);
        retryTemplate.registerListener(defaultListenerSupport());
        SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
        retryPolicy.setMaxAttempts(travianConfiguration.getMaxAttempts());
        retryTemplate.setRetryPolicy(retryPolicy);

        return retryTemplate;
    }


    @Bean
    public CookieManager cookieManager() {
        return new CookieManager();
    }

    @Bean(BeanNameConst.DEFAULT_LISTENER_SUPPORT)
    public RetryListenerSupport defaultListenerSupport() {
        return new RetryListenerSupport() {
            private final Logger logger = LoggerFactory.getLogger(RetryListenerSupport.class);

            @Override
            public <T, E extends Throwable> void close(RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {
                logger.info("onClose retry count: " + context.getRetryCount());
                super.close(context, callback, throwable);
            }

            @Override
            public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {
                logger.error("onError retry count: " + context.getRetryCount() + " Error: ", throwable);
                super.onError(context, callback, throwable);
            }

            @Override
            public <T, E extends Throwable> boolean open(RetryContext context, RetryCallback<T, E> callback) {
                logger.info("onOpen retry count: " + context.getRetryCount());
                return super.open(context, callback);
            }
        };
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory());
        restTemplate.setMessageConverters(getMessageConverters());
        return restTemplate;
    }

    //converters @link https://www.baeldung.com/spring-httpmessageconverter-rest
    public static List<HttpMessageConverter<?>> getMessageConverters() {
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        messageConverters.add(new StringHttpMessageConverter());
        MappingJackson2HttpMessageConverter jsonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
        jsonHttpMessageConverter.getObjectMapper().configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);//jackson-databind-2.9.9
        messageConverters.add(jsonHttpMessageConverter);
        messageConverters.add(new FormHttpMessageConverter());
        messageConverters.add(new FormHttpMessageConverter());
        return messageConverters;
    }

    @Bean
    public HttpComponentsClientHttpRequestFactory clientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setHttpClient(httpClient);
        return clientHttpRequestFactory;
    }

    @Bean
    public TaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setThreadNamePrefix(THREAD_NAME_PREFIX);
        scheduler.setPoolSize(THREAD_POOL_SIZE);
        scheduler.setDaemon(true);
        return scheduler;
    }
}