package by.travian.executor;

import by.travian.configuration.TravianConfiguration;
import by.travian.observer.ResponseManagerObserver;
import by.travian.request.TravianRequest;
import by.travian.service.CookieManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class HttpExecutor {
    private static final Logger logger = LoggerFactory.getLogger(HttpExecutor.class);

    private RestTemplate restTemplate;
    private CookieManager cookieManager;
    private ResponseManagerObserver managerObserver;
    private RetryTemplate retryTemplate;
    public AtomicBoolean isPaused;

    @Autowired
    public HttpExecutor(RestTemplate restTemplate, CookieManager cookieManager, ResponseManagerObserver managerObserver, RetryTemplate retryTemplate,
                        TravianConfiguration travianConfiguration) {
        this.restTemplate = restTemplate;
        this.cookieManager = cookieManager;
        this.managerObserver = managerObserver;
        this.retryTemplate = retryTemplate;
        this.isPaused = travianConfiguration.isPaused();
    }


    public ResponseTimestampEntity execute(TravianRequest request) {
        if (isPaused.get()) {
            try {
                synchronized (isPaused) {
                    isPaused.wait();
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        HttpEntity entity = request.entity(cookieManager);
        logger.trace(request.uri() + " \n" + entity.getBody().toString());
        AtomicReference<ResponseEntity<String>> response = new AtomicReference<ResponseEntity<String>>();
        retryTemplate.execute(ctx -> {
            response.set(restTemplate.exchange(request.uri(), request.method(), entity, String.class));
            return null;
        });

        cookieManager.storeCookies(request.headers(cookieManager).getHost().getHostString(), response.get().getHeaders());

        ResponseTimestampEntity responsePage = new ResponseTimestampEntity(response.get(), System.currentTimeMillis());

        managerObserver.responseUpdate(request, responsePage);

        //redirected page has no body (make sure that redirecting feature works)
        return responsePage;
    }

}
