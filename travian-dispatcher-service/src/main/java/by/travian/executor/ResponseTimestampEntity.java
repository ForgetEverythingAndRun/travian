package by.travian.executor;

import org.springframework.http.ResponseEntity;

public class ResponseTimestampEntity extends ResponseEntity<String> {
    private long timestamp;

    public ResponseTimestampEntity(ResponseEntity responseEntity, long timestamp) {
        super((String) responseEntity.getBody(), responseEntity.getStatusCode());
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }


}