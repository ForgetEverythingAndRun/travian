package by.travian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("by.travian.*")
public class DispatcherServiceApplication {

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(DispatcherServiceApplication.class, args);
        SpringApplication.run(DispatcherServiceApplication.class, args);
    }

}
