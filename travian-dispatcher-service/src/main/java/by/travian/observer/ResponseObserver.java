package by.travian.observer;


import by.travian.executor.ResponseTimestampEntity;
import by.travian.request.TravianRequest;

/**
 * Created by Администратор on 26.10.2017.
 */
public interface ResponseObserver {
    void responseUpdate(TravianRequest request, ResponseTimestampEntity response);
    void observe(Runnable runnable);
}
