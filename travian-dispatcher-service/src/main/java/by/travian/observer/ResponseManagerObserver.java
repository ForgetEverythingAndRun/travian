package by.travian.observer;

import by.travian.executor.ResponseTimestampEntity;
import by.travian.request.TravianRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ResponseManagerObserver extends AbstractResponseObserver {
    public final static Logger logger = LoggerFactory.getLogger(ResponseManagerObserver.class);
    private List<ResponseObserver> observerList = new ArrayList<ResponseObserver>();

    public final static int HTTP_SEE_OTHER = 303;

    public void add(ResponseObserver observer) {
        observerList.add(observer);
    }

    public void responseUpdate(TravianRequest request, ResponseTimestampEntity response) {
        if (response.getStatusCode().value() == HTTP_SEE_OTHER) {
            return;
        }
        if (response != null && response.getBody() != null) {
            Runnable runnable = () -> {
                for (ResponseObserver observer : observerList) {
                    observer.responseUpdate(request, response);
                }
            };
            observe(runnable);
        } else {
            logger.info("Response is empty, or request is failed!", request, response);
        }
    }

}
