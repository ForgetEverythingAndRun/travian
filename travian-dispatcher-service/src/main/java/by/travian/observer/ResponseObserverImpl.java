package by.travian.observer;

import by.travian.bean.container.VillageContainer;
import by.travian.bean.entity.LoginEntity;
import by.travian.bean.entity.village.Village;
import by.travian.bean.container.StaticResourceContainer;
import by.travian.bean.container.VillageResourceContainer;
import by.travian.bean.task.Task;
import by.travian.configuration.BeanNameConst;
import by.travian.configuration.TravianConfiguration;
import by.travian.constant.TravianConstant;
import by.travian.executor.ResponseTimestampEntity;
import by.travian.request.TravianRequest;
import by.travian.service.TravianService;
import by.travian.util.ResponseParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class ResponseObserverImpl extends AbstractResponseObserver {
    private final static Logger logger = LoggerFactory.getLogger(ResponseObserverImpl.class);
    private AtomicBoolean isAuth;
    private TravianService travianService;
    private TravianConfiguration travianConfiguration;
    private VillageContainer villageContainer;
    private ResponseParser responseParser;

    public ResponseObserverImpl(TravianService travianService, TravianConfiguration travianConfiguration, VillageContainer villageContainer, ResponseParser responseParser) {
        this.isAuth = travianConfiguration.isAuthenticated();
        this.travianService = travianService;
        this.travianConfiguration = travianConfiguration;
        this.villageContainer = villageContainer;
        this.responseParser = responseParser;
    }

    public void responseUpdate(TravianRequest request, ResponseTimestampEntity response) {
        Runnable runnable = () -> {
            if (!isAuth(response.getBody())) {
                logger.error("Required re-login!");
                isAuth.set(false);
                travianService.loginTaskExecute(new Task<LoginEntity>(new LoginEntity(travianConfiguration.getLogin(), travianConfiguration.getPassword())));
            } else {
                String responseBody = response.getBody();



                String activeId = responseParser.getActiveVillageId(responseBody);
                if (activeId != null) {
                    Village village = villageContainer.getVillageById(activeId);
                    if (village != null) {
                        VillageResourceContainer resourceContainer = responseParser.getVillageResources(responseBody);
                        if (resourceContainer != null) {
                            StaticResourceContainer actualResources = new StaticResourceContainer(resourceContainer.getWood().getAmount(), resourceContainer.getClay().getAmount(), resourceContainer.getIron().getAmount(), resourceContainer.getCrop().getAmount());
                            village.setResources(resourceContainer);
                            travianService.notifyWhenEnoughResources(village, actualResources);
                        }
                    }
                    //Update all list of villages
                    travianService.updateVillages(responseBody);

                    /*********/
                    //error case related with json body (e.g. travian plus -> payment wizard)
                    //and activeId's existing means that response body is html
                    if (travianConfiguration.isHeroAdventureEnabled()) {
                        if (responseParser.hasAdventure(responseBody)) {
                            synchronized (travianConfiguration.hasHeroAdventure()) {
                                travianConfiguration.hasHeroAdventure().notifyAll();
                            }
                        }
                    }
                }
                //TODO: Parse info about hero adventures


            }
        };
        observe(runnable);
    }

    public boolean isAuth(String responseBody) {
        return !StringUtils.isBlank(responseBody) && responseBody.indexOf(TravianConstant.Common.LOGIN_FORM) == -1;
    }
}
