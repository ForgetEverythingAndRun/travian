package by.travian.observer;

import by.travian.executor.ResponseTimestampEntity;
import by.travian.request.TravianRequest;
import by.travian.util.ExtractMainPageHeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Deprecated
public class ReportResponseObserver extends AbstractResponseObserver {
    public final static Logger logger = LoggerFactory.getLogger(ReportResponseObserver.class);

    public void responseUpdate(TravianRequest request, ResponseTimestampEntity response) {
        Runnable runnable = () -> {
            if (response != null && response.getBody() != null) {
                int count = ExtractMainPageHeaderUtil.getHeaderUpdate(ExtractMainPageHeaderUtil.REPORT, response.getBody());
                logger.info("Reports count:" + count);
            }
        };
        observe(runnable);
    }
}
