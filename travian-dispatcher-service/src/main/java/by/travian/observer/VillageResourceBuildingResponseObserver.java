package by.travian.observer;

import by.travian.bean.container.BuildingSlotContainer;
import by.travian.bean.container.VillageContainer;
import by.travian.bean.entity.village.Village;
import by.travian.constant.TravianConstant;
import by.travian.executor.ResponseTimestampEntity;
import by.travian.request.TravianRequest;
import by.travian.util.ResponseParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Deprecated
public class VillageResourceBuildingResponseObserver extends AbstractResponseObserver {
    public final static Logger logger = LoggerFactory.getLogger(VillageResourceBuildingResponseObserver.class);

    private final VillageContainer villageContainer;
    private final ResponseParser responseParser;

    public VillageResourceBuildingResponseObserver(ResponseParser responseParser, VillageContainer villageContainer) {
        this.responseParser = responseParser;
        this.villageContainer = villageContainer;
    }

    // OBSERVERS CAN BE A REASON OF ERROR: 408 Request Time-out
    // Required for:
    // Tracking resources, new villages, building dashboard timers & etc
    public void responseUpdate(TravianRequest request, ResponseTimestampEntity response) {
        Runnable runnable = () -> {
            if (request.uri().contains(TravianConstant.Common.VILLAGE_MAIN_PAGE_PATH)) {
                String responseBody = response.getBody();
                String activeId = responseParser.getActiveVillageId(responseBody);
                responseParser.getVillageResourceBuildings(responseBody).forEach((k, v) -> {
                    Village village = villageContainer.getVillageById(activeId);

                    //Error: "village" is null for some pages! Login???
                    if(village != null) {
                        village.setResources(responseParser.getVillageResources(responseBody));
                        BuildingSlotContainer buildingSlotContainer = village.getBuildingSlotContainer();
                        buildingSlotContainer.put(k, v);
                        //logger.info(v.toString());
                    }
                });
            }
        };
        observe(runnable);
    }

}
