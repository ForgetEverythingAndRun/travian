package by.travian.observer;

public abstract class AbstractResponseObserver implements ResponseObserver{

    @Override
    public void observe(Runnable runnable) {
        Thread background = new Thread(runnable);
        background.setDaemon(true);
        background.start();
    }
}
