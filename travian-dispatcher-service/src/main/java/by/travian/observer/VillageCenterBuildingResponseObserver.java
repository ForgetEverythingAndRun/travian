package by.travian.observer;

import by.travian.bean.container.BuildingSlotContainer;
import by.travian.bean.container.VillageContainer;
import by.travian.bean.entity.village.Village;
import by.travian.constant.TravianConstant;
import by.travian.executor.ResponseTimestampEntity;
import by.travian.request.TravianRequest;
import by.travian.util.ResponseParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Deprecated
public class VillageCenterBuildingResponseObserver extends AbstractResponseObserver {
    public final static Logger logger = LoggerFactory.getLogger(VillageCenterBuildingResponseObserver.class);

    private VillageContainer villageContainer;
    private ResponseParser responseParser;

    public VillageCenterBuildingResponseObserver(ResponseParser responseParser, VillageContainer villageContainer) {
        this.responseParser = responseParser;
        this.villageContainer = villageContainer;
    }

    // OBSERVERS CAN BE A REASON OF ERROR: 408 Request Time-out

    // Required for:
    // Tracking resources, new villages, building dashboard timers & etc
    public void responseUpdate(TravianRequest request, ResponseTimestampEntity response) {
        //Seems that it's useless: identify url
        //Much easy to check response after request
        Runnable runnable = () -> {
            if (request.uri().contains(TravianConstant.Common.VILLAGE_CENTER_PATH)) {
                String responseBody = response.getBody();
                String activeId = responseParser.getActiveVillageId(responseBody);
                responseParser.getVillageCenterBuildings(responseBody).forEach((k, v) -> {
                    Village village = villageContainer.getVillageById(activeId);
                    if (village != null) {
                        village.setResources(responseParser.getVillageResources(responseBody));
                        BuildingSlotContainer buildingSlotContainer = village.getBuildingSlotContainer();
                        buildingSlotContainer.put(k, v);
                        //logger.info(k.toString() + " " + v.toString());
                    }
                });
            }
        };
        observe(runnable);
    }

}
