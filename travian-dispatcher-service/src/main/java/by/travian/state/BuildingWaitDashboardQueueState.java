package by.travian.state;

import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.village.Village;
import by.travian.bean.task.Task;
import by.travian.service.TravianService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class BuildingWaitDashboardQueueState extends State<BuildingEntity> {
    public final static Logger logger = LoggerFactory.getLogger(BuildingWaitDashboardQueueState.class);


    public BuildingWaitDashboardQueueState(Task<BuildingEntity> task, TravianService service) {
        super(task, service);
    }

    @Override
    public void update(Village village) {
        active = verifyAndWaitBuildingDashboardQueue(village, task);
        active.update(village);
    }

    protected State verifyAndWaitBuildingDashboardQueue(Village village, Task<BuildingEntity> task) {
        Integer buildingId = task.getEntity().getId();
        if (!service.isBuildingQueueReady(village, Building.isResourceBuilding(buildingId))) {
            long wait = village.getBuildingQueueTimeWait() - System.currentTimeMillis();
            if (wait > 0) {
                logger.info("Building dashboard is full.  Wait(min): ~" + TimeUnit.MILLISECONDS.toMinutes(wait) + " " + village + ".  VillageId: " + village.getVillageId() + " BuildingId: " + buildingId);
                try {
                    TimeUnit.MILLISECONDS.sleep(wait);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } else {
            logger.trace("Building dashboard has free capacity for building: " + task.getEntity());
            return new BuildingWaitResourceProduceTimeState(this.task, service);
        }
        return this;
    }

}
