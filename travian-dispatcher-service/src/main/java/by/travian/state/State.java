package by.travian.state;

import by.travian.bean.entity.Entity;
import by.travian.bean.entity.village.Village;
import by.travian.bean.task.Task;
import by.travian.service.TravianService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class State<T extends Entity> {
    public final static Logger logger = LoggerFactory.getLogger(State.class);

    protected Task task;
    protected TravianService service;
    protected State active;
    protected boolean isComplete;

    public State(Task<T> task, TravianService service) {
        this.task = task;
        this.service = service;
        this.active = this;
    }

    public abstract void update(Village village);


    public boolean isComplete() {
        return active.isComplete;
    }
}
