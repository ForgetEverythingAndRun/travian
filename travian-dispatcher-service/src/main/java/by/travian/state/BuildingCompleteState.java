package by.travian.state;

import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.village.Village;
import by.travian.bean.task.Task;
import by.travian.service.TravianService;

public class BuildingCompleteState extends State<BuildingEntity>{

    public BuildingCompleteState(Task<BuildingEntity> context, TravianService service) {
        super(context, service);
    }

    @Override
    public void update(Village village) {
        isComplete = true;
    }
}
