package by.travian.state;

import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.building.Level;
import by.travian.bean.entity.building.Slot;
import by.travian.bean.entity.village.Village;
import by.travian.bean.container.StaticResourceContainer;
import by.travian.bean.resource.VillageResource;
import by.travian.bean.container.VillageResourceContainer;
import by.travian.bean.task.Task;
import by.travian.error.TravianException;
import by.travian.service.TravianService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BuildingWaitResourceProduceTimeState extends State<BuildingEntity> {
    public final static Logger logger = LoggerFactory.getLogger(BuildingWaitResourceProduceTimeState.class);

    public BuildingWaitResourceProduceTimeState(Task<BuildingEntity> task, TravianService service) {
        super(task, service);
    }

    @Override
    public void update(Village village) {
        try {
            calculateDeficitResourceProduceTimeAndWait(village, task);
        } catch (IllegalArgumentException | TravianException e) {
            logger.error("", e);
            active = new BuildingErrorState(this.task, service);
            active.update(village);
        }
    }

    protected Long getProduceMaxTimeDeficitResource(Level levelCost, VillageResourceContainer container) {
        List<Long> list = Arrays.asList(
                VillageResourceContainer.getRequiredTimeToProduceResource(levelCost.getWood(), container.getWood()),
                VillageResourceContainer.getRequiredTimeToProduceResource(levelCost.getClay(), container.getClay()),
                VillageResourceContainer.getRequiredTimeToProduceResource(levelCost.getIron(), container.getIron()),
                VillageResourceContainer.getRequiredTimeToProduceResource(levelCost.getCrop(), container.getCrop())
        );
        return list.stream().mapToLong(v -> v).max().getAsLong();
    }

    protected boolean isEnoughResourceMaxStorageCapacity(Level levelCost, VillageResourceContainer container) {
        return levelCost.getWood() <= container.getWarehouseCapacity()
                && levelCost.getClay() <= container.getWarehouseCapacity()
                && levelCost.getIron() <= container.getWarehouseCapacity()
                && levelCost.getCrop() <= container.getGranaryCapacity();
    }

    protected void calculateDeficitResourceProduceTimeAndWait(Village village, Task<BuildingEntity> task) {
        Building building = village.getBuildingSlotContainer().get(new Slot(task.getEntity().getSlotId()));
        int nextUpgradeLevel = building != null ? building.getLevel().getNumber() + 1 : 1; // new building has level 1

        final List<Level> levels = service.getBuildingLevelMap().get(task.getEntity().getId());

        Integer maxLevelBound = Math.min(nextUpgradeLevel, levels.size());

        Level levelCost = levels.stream()
                .filter(x -> x.getNumber().equals(maxLevelBound)).findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Can't find resource cost level for buildingId: " + task.getEntity().getId() + " level: " + maxLevelBound));

        logger.info("Village: " + village + " Resources updated: " + service.getVillageResources(village));
        if (!isEnoughResourceMaxStorageCapacity(levelCost, village.getResources())) {
            throw new TravianException("Resource capacity isn't enough to execute task. " + village + " Details: " + village.getResources());
        }
        long resourceProduceMaxTimeWait = getProduceMaxTimeDeficitResource(levelCost, village.getResources());

        if (resourceProduceMaxTimeWait > 0) {
            //logger.info("Village: " + village + " Not enough resources for buildingId: " + task.getEntity().getId() + ".");
            service.sleepLogMessage(resourceProduceMaxTimeWait, TimeUnit.MILLISECONDS, "Village: " + village + " Not enough resources for buildingId: " + task.getEntity().getId() + ".");
            VillageResource wood = village.getResources().getWood();
            VillageResource clay = village.getResources().getClay();
            VillageResource iron = village.getResources().getIron();
            VillageResource crop = village.getResources().getCrop();
            logger.trace("Total Wood: " + wood.getAmount() + " production: " + wood.getProduction() + " required: " + levelCost.getWood());
            logger.trace("Total Clay: " + clay.getAmount() + " production: " + clay.getProduction() + " required: " + levelCost.getClay());
            logger.trace("Total Iron: " + iron.getAmount() + " production: " + iron.getProduction() + " required: " + levelCost.getIron());
            logger.trace("Total Crop: " + crop.getAmount() + " production: " + crop.getProduction() + " required: " + levelCost.getCrop());
            //condition.await
            StaticResourceContainer resourcesRequired = new StaticResourceContainer(levelCost.getWood(), levelCost.getClay(), levelCost.getIron(), levelCost.getCrop());
            synchronized (resourcesRequired) {
                try {
                    service.trackResources(village, resourcesRequired);
                    resourcesRequired.wait(resourceProduceMaxTimeWait);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
