package by.travian.state;

import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.building.Slot;
import by.travian.bean.entity.village.Village;
import by.travian.bean.task.Task;
import by.travian.service.TravianService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BuildingVerifyCompletionState extends State<BuildingEntity> {
    public final static Logger logger = LoggerFactory.getLogger(BuildingVerifyCompletionState.class);

    public BuildingVerifyCompletionState(Task<BuildingEntity> task, TravianService service) {
        super(task, service);
    }

    @Override
    public void update(Village village) {
        boolean processed = verifyCompletion(village, task);
        if (processed) {
            active = new BuildingCompleteState(this.task, service);
        } else {
            active = new BuildingWaitDashboardQueueState(this.task, service);
        }
        active.update(village);
    }

    protected boolean verifyCompletion(Village village, Task<BuildingEntity> task) {
        Building building = village.getBuildingSlotContainer().get(new Slot(task.getEntity().getSlotId()));
        return building != null && building.getLevel().getNumber() >= task.getEntity().getUntilLvlRepeat();
    }
}
