package by.travian.state;

import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.village.Village;
import by.travian.bean.task.Task;
import by.travian.service.TravianService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BuildingErrorState  extends State<BuildingEntity> {
    public final static Logger logger = LoggerFactory.getLogger(BuildingErrorState.class);


    public BuildingErrorState(Task<BuildingEntity> context, TravianService service) {
        super(context, service);
    }

    @Override
    public void update(Village village) {
        isComplete = true;
    }
}
