package by.travian.common.executor;

import by.travian.common.command.Command;
import by.travian.common.context.ProcessorContext;
import by.travian.common.processor.ExecutorProcessor;


public class CommonExecutorProcessor<T extends Command> implements ExecutorProcessor<T> {

    private ExecutorProcessor<T> successor;

    @Override
    public final void setSuccessor(ExecutorProcessor<T> successor) {
        if (this.successor != null) {
            throw new IllegalStateException("Successor already initialized. Chain is immutable.");
        }
        this.successor = successor;
    }

    @Override
    public ExecutorProcessor<T> getSuccessor() {
        return successor;
    }

    @Override
    public void inbound(ProcessorContext<T> context) throws Exception {
        getSuccessor().inbound(context);
    }

    @Override
    public void process(ProcessorContext<T> context) throws Exception {
        getSuccessor().process(context);
    }

    @Override
    public void outbound(ProcessorContext<T> context) throws Exception {
        getSuccessor().outbound(context);
    }

    @Override
    public void onFault(ProcessorContext<T> context, Exception ex) throws Exception {
        //do nothing by default
    }
}

