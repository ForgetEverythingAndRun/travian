package by.travian.common.executor;

public enum CommandPhase {

    NOT_STARTED,
    INBOUND,
    PROCESS,
    OUTBOUND
}
