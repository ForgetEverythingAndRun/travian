package by.travian.common.command.factory;

import by.travian.common.command.*;
import by.travian.common.context.TravianApplicationContext;
import by.travian.executor.HttpExecutor;
import by.travian.service.TravianLoginService;
import by.travian.service.TravianMarketService;
import by.travian.service.TravianOffenseService;
import by.travian.service.TravianVillageService;
import by.travian.service.discord.DiscordApplication;
import by.travian.util.ResponseParser;

public class CommandFactory {
    private final HttpExecutor httpExecutor;
    private final ResponseParser responseParser;

    private final TravianApplicationContext applicationContext;
    private final DiscordApplication discordApplication;

    private final TravianOffenseService travianOffenseService;
    private final TravianLoginService travianLoginService;
    private final TravianMarketService travianMarketService;
    private final TravianVillageService travianVillageService;

    public CommandFactory(TravianApplicationContext applicationContext,
                          HttpExecutor httpExecutor,
                          ResponseParser responseParser,
                          DiscordApplication discordApplication,
                          TravianOffenseService travianOffenseService,
                          TravianLoginService travianLoginService,
                          TravianMarketService travianMarketService,
                          TravianVillageService travianVillageService) {
        this.applicationContext = applicationContext;
        this.httpExecutor = httpExecutor;
        this.responseParser = responseParser;
        this.discordApplication = discordApplication;
        this.travianOffenseService = travianOffenseService;
        this.travianLoginService = travianLoginService;
        this.travianMarketService = travianMarketService;
        this.travianVillageService = travianVillageService;
    }

    public RallyPointTrackEnemyCommand createRallyPointTrackEnemyCommand() {
        return new RallyPointTrackEnemyCommand(applicationContext, httpExecutor, responseParser, discordApplication);
    }

    public RecruitUnitCommand createRecruitUnitCommand() {
        return new RecruitUnitCommand(applicationContext, httpExecutor, responseParser);
    }

    public OffenseCommand createOffenseCommand() {
        return new OffenseCommand(applicationContext, travianOffenseService);
    }

    public LoginCommand createLoginCommand() {
        return new LoginCommand(applicationContext, travianLoginService);
    }

    public MarketResourceSendCommand createMarketResourceSendCommand() {
        return new MarketResourceSendCommand(applicationContext, travianMarketService);
    }

    public VillageBuildingCommand createVillageBuildingCommand() {
        return new VillageBuildingCommand(applicationContext, travianVillageService);
    }

}
