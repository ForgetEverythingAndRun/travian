package by.travian.common.command;

import by.travian.bean.entity.LoginEntity;
import by.travian.common.context.CommandContext;
import by.travian.common.context.TravianApplicationContext;
import by.travian.service.TravianLoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginCommand extends VoidCommand<LoginEntity> {
    public final static Logger logger = LoggerFactory.getLogger(LoginCommand.class);

    private CommandContext<LoginEntity> commandContext;
    private TravianLoginService service;

    public LoginCommand(final TravianApplicationContext applicationContext, final TravianLoginService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        verifyCommandContext();
        LoginEntity entity = getCommandContext().getEntity();
        service.login(entity);
    }

    @Override
    public void setCommandContext(CommandContext<LoginEntity> commandContext) {
        this.commandContext = commandContext;
    }

    @Override
    public CommandContext<LoginEntity> getCommandContext() {
        return commandContext;
    }
}
