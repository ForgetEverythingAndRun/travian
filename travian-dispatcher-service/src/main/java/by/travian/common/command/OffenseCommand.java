package by.travian.common.command;

import by.travian.bean.container.VillageContainer;
import by.travian.bean.entity.OffenseEntity;
import by.travian.bean.entity.village.Village;
import by.travian.common.context.CommandContext;
import by.travian.common.context.TravianApplicationContext;
import by.travian.service.TravianOffenseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OffenseCommand extends VoidCommand<OffenseEntity> {
    public final static Logger logger = LoggerFactory.getLogger(OffenseCommand.class);

    private CommandContext<OffenseEntity> commandContext;

    private final VillageContainer villageContainer;
    private final TravianOffenseService service;

    public OffenseCommand(final TravianApplicationContext applicationContext, final TravianOffenseService service) {
        this.service = service;
        this.villageContainer = applicationContext.getVillageContainer();
    }

    @Override
    public void execute() {
        verifyCommandContext();
        Village village = villageContainer.getVillageById(commandContext.getVillageId());
        if (village == null) {
            logger.debug(String.format("Village isn't exist: %s", commandContext.getVillageId()));
            return;
        }
        OffenseEntity entity = getCommandContext().getEntity();
        service.fastAttack(entity);        //EventHelper.pullDataFromResponse(event)
    }

    @Override
    public void setCommandContext(CommandContext<OffenseEntity> commandContext) {
        this.commandContext = commandContext;
    }

    @Override
    public CommandContext<OffenseEntity> getCommandContext() {
        return commandContext;
    }

}
