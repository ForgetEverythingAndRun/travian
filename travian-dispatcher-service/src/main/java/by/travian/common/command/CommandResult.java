package by.travian.common.command;

public interface CommandResult<R> {
    public R getResult();
}
