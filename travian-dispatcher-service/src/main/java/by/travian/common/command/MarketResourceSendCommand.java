package by.travian.common.command;

import by.travian.bean.container.VillageContainer;
import by.travian.bean.entity.MarketEntity;
import by.travian.bean.entity.village.Village;
import by.travian.common.context.CommandContext;
import by.travian.common.context.TravianApplicationContext;
import by.travian.error.BuildingExtraDependencyException;
import by.travian.error.TravianException;
import by.travian.service.TravianMarketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MarketResourceSendCommand extends VoidCommand<MarketEntity> {
    public final static Logger logger = LoggerFactory.getLogger(MarketResourceSendCommand.class);

    private CommandContext<MarketEntity> commandContext;
    private final VillageContainer villageContainer;
    private final TravianMarketService service;

    public MarketResourceSendCommand(final TravianApplicationContext applicationContext, final TravianMarketService service) {
        this.service = service;
        this.villageContainer = applicationContext.getVillageContainer();
    }

    @Override
    public void execute() {
        verifyCommandContext();
        Village village = villageContainer.getVillageById(commandContext.getVillageId());
        if (village == null) {
            logger.debug(String.format("Village isn't exist: %s", commandContext.getVillageId()));
            return;
        }
        MarketEntity entity = getCommandContext().getEntity();
        //try {
            service.resourcesSend(entity); //this error case isn't critical and task should be repeated next time even error happens
        /*} catch (TravianException e) {
            getCommandContext().setFailedCommand(true, e);
        }*/
    }

    @Override
    public void setCommandContext(CommandContext<MarketEntity> commandContext) {
        this.commandContext = commandContext;
    }

    @Override
    public CommandContext<MarketEntity> getCommandContext() {
        return commandContext;
    }
}
