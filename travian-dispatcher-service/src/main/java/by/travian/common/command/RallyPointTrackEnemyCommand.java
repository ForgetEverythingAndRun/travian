package by.travian.common.command;

import by.travian.bean.container.VillageContainer;
import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.building.BuildingTabs;
import by.travian.bean.entity.village.Village;
import by.travian.bean.container.MovementContainer;
import by.travian.bean.container.VillageResourceContainer;
import by.travian.common.context.CommandContext;
import by.travian.common.context.TravianApplicationContext;
import by.travian.configuration.TravianConfiguration;
import by.travian.executor.HttpExecutor;
import by.travian.request.TravianBuildingRequest;
import by.travian.service.discord.DiscordApplication;
import by.travian.util.ResponseParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

public class RallyPointTrackEnemyCommand extends VoidCommand<BuildingEntity> {
    private final static Logger logger = LoggerFactory.getLogger(RallyPointTrackEnemyCommand.class);

    private final HttpExecutor httpExecutor;
    private final ResponseParser responseParser;

    private final TravianConfiguration travianConfiguration;

    private final VillageContainer villageContainer;
    private final MovementContainer movementContainer;
    private final DiscordApplication discordApplication;

    private CommandContext<BuildingEntity> commandContext;

    public RallyPointTrackEnemyCommand(final TravianApplicationContext applicationContext, final HttpExecutor httpExecutor, final ResponseParser responseParser, DiscordApplication discordApplication) {
        this.httpExecutor = httpExecutor;
        this.responseParser = responseParser;

        this.travianConfiguration = applicationContext.getTravianConfiguration();
        this.villageContainer = applicationContext.getVillageContainer();
        this.movementContainer = applicationContext.getMovementContainer();

        this.discordApplication = discordApplication;
    }

    @Override
    public void execute() {
        verifyCommandContext();
        trackEnemy(commandContext.getEntity());
    }

    @Override
    public void setCommandContext(CommandContext<BuildingEntity> commandContext) {
        this.commandContext = commandContext;
    }

    @Override
    public CommandContext getCommandContext() {
        return commandContext;
    }

    private void trackEnemy(BuildingEntity buildingEntity) {
        Village village = villageContainer.getVillageById(commandContext.getVillageId());
        if (village == null) {
            logger.debug(String.format("Village isn't exist: %s", commandContext.getVillageId()));
            return;
        }
        TravianBuildingRequest openRallyPointViewTab = new TravianBuildingRequest(buildingEntity, BuildingTabs.RallyPointTab.VIEW);
        ResponseEntity<String> response = httpExecutor.execute(openRallyPointViewTab);

        String responseBody = response.getBody();
        if (StringUtils.isBlank(responseBody)) {
            return;
        }

        //update resources cause we get a response which provides it
        VillageResourceContainer resourceContainer = responseParser.getVillageResources(response.getBody());
        village.setResources(resourceContainer);

        try {
            logger.debug("Track enemy movements village id: " + village.getVillageId());
            responseParser.updateEnemyMovementContainer(movementContainer, village, responseBody);
            discordApplication.push(movementContainer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
