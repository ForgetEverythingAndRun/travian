package by.travian.common.command;

import by.travian.bean.entity.EmptyEntity;
import by.travian.common.context.CommandContext;
import by.travian.configuration.TravianConfiguration;
import by.travian.error.TravianException;
import by.travian.executor.HttpExecutor;
import by.travian.request.TravianDefaultRequest;
import by.travian.request.TravianRequest;
import by.travian.util.DefaultParamParser;
import by.travian.util.ResponseParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

@Deprecated
public class GetBaseAuthTokenCommand implements Command<EmptyEntity, String> {
    private final static Logger logger = LoggerFactory.getLogger(GetBaseAuthTokenCommand.class);
    private final HttpExecutor httpExecutor;
    private final ResponseParser responseParser;
    private final TravianConfiguration travianConfiguration;

    private CommandContext<EmptyEntity> commandContext;

    private String token;

    public GetBaseAuthTokenCommand(HttpExecutor httpExecutor, ResponseParser responseParser, TravianConfiguration travianConfiguration) {
        this.httpExecutor = httpExecutor;
        this.responseParser = responseParser;
        this.travianConfiguration = travianConfiguration;
    }

    public GetBaseAuthTokenCommand(CommandContext<EmptyEntity> commandContext, HttpExecutor httpExecutor, ResponseParser responseParser, TravianConfiguration travianConfiguration) {
        this(httpExecutor, responseParser, travianConfiguration);
        this.commandContext = commandContext;
    }

    @Override
    public void execute() {
        TravianRequest villageRequest = new TravianDefaultRequest();
        ResponseEntity<String> response = httpExecutor.execute(villageRequest);

        try {
            token = DefaultParamParser.findToken(response.getBody());
        } catch (TravianException e) {
            getCommandContext().setFailedCommand(true, e);
        }
    }

    @Override
    public void setCommandContext(CommandContext<EmptyEntity> commandContext) {
        this.commandContext = commandContext;
    }

    @Override
    public CommandContext<EmptyEntity> getCommandContext() {
        return commandContext;
    }

    @Override
    public String getResult() {
        return token;
    }
}
