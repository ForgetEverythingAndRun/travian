package by.travian.common.command;

import by.travian.bean.entity.Entity;

@Deprecated
//TODO: If command do something then it can't extend voidCommand!
public abstract class VoidCommand<T extends Entity> implements Command<T,String>{
    @Override
    public String getResult() {
        return null;
    }
}
