package by.travian.common.command;

import by.travian.bean.container.VillageContainer;
import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.village.Village;
import by.travian.common.context.CommandContext;
import by.travian.common.context.TravianApplicationContext;
import by.travian.error.BuildingExtraDependencyException;
import by.travian.service.TravianVillageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VillageBuildingCommand extends VoidCommand<BuildingEntity> {
    public final static Logger logger = LoggerFactory.getLogger(VillageBuildingCommand.class);

    private CommandContext<BuildingEntity> commandContext;
    private final VillageContainer villageContainer;
    private final TravianVillageService service;

    public VillageBuildingCommand(final TravianApplicationContext applicationContext, final TravianVillageService service) {
        this.service = service;
        this.villageContainer = applicationContext.getVillageContainer();
    }

    @Override
    public void execute() {
        verifyCommandContext();
        Village village = villageContainer.getVillageById(commandContext.getVillageId());
        if (village == null) {
            logger.debug(String.format("Village isn't exist: %s", commandContext.getVillageId()));
            return;
        }
        BuildingEntity entity = getCommandContext().getEntity();
        try {
            service.makeBuildingTask(entity);
        } catch (BuildingExtraDependencyException e) {
            getCommandContext().setFailedCommand(true, e);
        }
    }

    @Override
    public void setCommandContext(CommandContext<BuildingEntity> commandContext) {
        this.commandContext = commandContext;
    }

    @Override
    public CommandContext<BuildingEntity> getCommandContext() {
        return commandContext;
    }

}
