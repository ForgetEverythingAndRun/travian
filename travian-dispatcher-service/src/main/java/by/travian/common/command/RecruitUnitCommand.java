package by.travian.common.command;

import by.travian.bean.Nation;
import by.travian.bean.container.VillageContainer;
import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.RecruitmentEntity;
import by.travian.bean.entity.building.Building;
import by.travian.bean.entity.building.BuildingTabs;
import by.travian.bean.entity.building.Slot;
import by.travian.bean.entity.village.Village;
import by.travian.bean.troop.TroopAlias;
import by.travian.bean.troop.UnitBuilding;
import by.travian.common.context.CommandContext;
import by.travian.common.context.TravianApplicationContext;
import by.travian.configuration.TravianConfiguration;
import by.travian.executor.HttpExecutor;
import by.travian.request.TravianBuildingRequest;
import by.travian.request.TravianRecruitmentRequest;
import by.travian.request.TravianRequest;
import by.travian.util.DefaultParamParser;
import by.travian.util.ResponseParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public class RecruitUnitCommand extends VoidCommand<RecruitmentEntity> {
    private final static Logger logger = LoggerFactory.getLogger(RecruitUnitCommand.class);
    private final static int CHIEFTAIN_OR_SETTLER_BUILDING_MIN_LEVEL_REQUIRED = 10;

    private CommandContext<RecruitmentEntity> commandContext;
    private final HttpExecutor httpExecutor;
    private final ResponseParser responseParser;
    private final VillageContainer villageContainer;
    private final TravianConfiguration travianConfiguration;

    public RecruitUnitCommand(TravianApplicationContext applicationContext, HttpExecutor httpExecutor, ResponseParser responseParser) {
        this.httpExecutor = httpExecutor;
        this.responseParser = responseParser;
        this.villageContainer = applicationContext.getVillageContainer();
        this.travianConfiguration = applicationContext.getTravianConfiguration();
    }

    @Override
    public void execute() {
        verifyCommandContext();
        troopsSingleTypeAutoBuyMaxValue();
    }

    public void troopsSingleTypeAutoBuyMaxValue() {
        Village village = villageContainer.getVillageById(commandContext.getVillageId());
        if (village == null) {
            logger.debug(String.format("Village isn't exist: %s", commandContext.getVillageId()));
            return;
        }
        String villageId = village.getVillageId();
        RecruitmentEntity recruitmentEntity = getCommandContext().getEntity();
        try {
            TroopAlias.RequestTroopAlias alias = recruitmentEntity.getAlias();
            Nation nation = recruitmentEntity.getNation();
            UnitBuilding unitBuilding = nation.getNationTroopByAlias(alias);
            Map.Entry<Slot, Building> entry = villageContainer.getVillageById(villageId).getBuildingSlotContainer().getFirstEntryByMatchedValue(unitBuilding.getUnitBuildingIds());

            if (entry == null || entry.getKey() == null || entry.getValue() == null) {
                logger.info("Can't recruit unit (alias: " + alias + " nation: " + nation + " villageId:" + villageId + "). Reason: required special building.");
                return;
            }
            Building building = entry.getValue();
            BuildingTabs.BuildingTabValue tab = BuildingTabs.DefaultTab.MANAGEMENT;
            if (TroopAlias.RequestTroopAlias.T10 == alias || TroopAlias.RequestTroopAlias.T9 == alias) {
                tab = BuildingTabs.SettlerTab.BUY;
                if (building.getLevel().getNumber() < CHIEFTAIN_OR_SETTLER_BUILDING_MIN_LEVEL_REQUIRED) {
                    logger.info("Can't recruit chieftain or settler (alias: " + alias + " nation: " + nation + " villageId:" + villageId + "). Reason: building level less than required(10).");
                    return;
                }
            }
            BuildingEntity entity = new BuildingEntity(building, entry.getKey(), village.getVillageId());
            TravianRequest villageRequest = new TravianBuildingRequest(entity, tab);
            ResponseEntity<String> response = httpExecutor.execute(villageRequest);

            String responseBody = response.getBody();
            if (StringUtils.isBlank(responseBody)) {
                return;
            }

            String c = DefaultParamParser.findInput("c", responseBody);//z replaced by c
            String a = DefaultParamParser.findInput("a", responseBody);
            String s = DefaultParamParser.findInput("s", responseBody);
            Integer buildingId = entry.getValue().getId();
            TravianRecruitmentRequest request = new TravianRecruitmentRequest(c, a, s,
                    recruitmentEntity.toBuilder().slotId(Integer.toString(entry.getKey().getId())).buildingId(buildingId).build(),
                    tab);

            response = httpExecutor.execute(request);
            responseBody = response.getBody();
            if (StringUtils.isBlank(responseBody)) {
                return;
            }
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    @Override
    public void setCommandContext(CommandContext<RecruitmentEntity> commandContext) {
        this.commandContext = commandContext;
    }

    @Override
    public CommandContext<RecruitmentEntity> getCommandContext() {
        return commandContext;
    }
}
