package by.travian.common.command;

import by.travian.bean.container.TravianPlusContainer;
import by.travian.common.context.CommandContext;
import by.travian.configuration.TravianConfiguration;
import by.travian.executor.HttpExecutor;
import by.travian.request.TravianDefaultRequest;
import by.travian.request.TravianPlusRequest;
import by.travian.request.TravianRequest;
import by.travian.util.DefaultParamParser;
import by.travian.util.ResponseParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.ResponseEntity;

//TODO: Re-work it!. Sample of complicated command (+GetBaseAuthTokenCommand)
public class TravianPlusTrackCommand extends VoidCommand {
    public final static Logger logger = LoggerFactory.getLogger(TravianPlusTrackCommand.class);
    private final static String THREAD_NAME = "TravianPlus";

    private final HttpExecutor httpExecutor;
    private final ResponseParser responseParser;
    private final TravianPlusContainer travianPlusContainer;
    private final TravianConfiguration travianConfiguration;

    private CommandContext commandContext;

    public TravianPlusTrackCommand(HttpExecutor httpExecutor, ResponseParser responseParser, TravianPlusContainer travianPlusContainer, TravianConfiguration travianConfiguration) {
        this.httpExecutor = httpExecutor;
        this.responseParser = responseParser;
        this.travianPlusContainer = travianPlusContainer;
        this.travianConfiguration = travianConfiguration;
    }

    private Thread createTravianPlusThread() {
        Thread monitorTravianPlusStatus = new Thread(travianPlusContainerUpdate());
        monitorTravianPlusStatus.setName(THREAD_NAME);
        monitorTravianPlusStatus.setDaemon(true);
        return monitorTravianPlusStatus;
    }

    private Runnable travianPlusContainerUpdate() {
        return new Runnable() {
            @Override
            public void run() {
                while (true) {
                    String token = getToken();
                    if (token != null) {
                        TravianPlusRequest travianPlusRequest = new TravianPlusRequest(token);
                        ResponseEntity<String> response = httpExecutor.execute(travianPlusRequest);

                        String responseBody = response.getBody();
                        if (StringUtils.isBlank(responseBody)) {
                            logger.debug("Response body is empty.");
                            return;
                        }
                        try {
                            responseBody = new JSONObject(responseBody).get("html").toString();
                            responseParser.updateTravianPlus(travianPlusContainer, responseBody);
                            logger.debug("Travian plus active: " + travianPlusContainer.isTravianPlusActive());
                        } catch (JSONException e) {
                            logger.debug("", e);
                        }
                    } else {
                        logger.error("Can't receive a token.");
                    }
                    try {
                        if (travianPlusContainer.isTravianPlusActive()) {
                            Thread.sleep(travianPlusContainer.getTimestamp() - System.currentTimeMillis());
                        } else {
                            travianConfiguration.getTravianPlusTrackTimeUnit().sleep(travianConfiguration.getTravianPlusTrackTimeout());
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    //TODO: replace by GetBaseAuthTokenCommand
    private String getToken() {
        TravianRequest villageRequest = new TravianDefaultRequest();
        ResponseEntity<String> response = httpExecutor.execute(villageRequest);

        String body = response.getBody();
        if (StringUtils.isBlank(body)) {
            logger.debug("Response body is empty.");
            return null;
        }

        String token = DefaultParamParser.findToken(body);
        return token;
    }

    @Override
    public void execute() {
        createTravianPlusThread().start();
    }

    @Override
    public void setCommandContext(CommandContext commandContext) {

    }

    @Override
    public CommandContext getCommandContext() {
        return null;
    }
}
