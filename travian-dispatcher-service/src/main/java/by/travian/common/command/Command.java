package by.travian.common.command;

import by.travian.bean.entity.Entity;
import by.travian.common.context.CommandContext;
import by.travian.error.TravianException;

public interface Command<T extends Entity, R> extends CommandResult<R> {

    void execute();

    void setCommandContext(CommandContext<T> commandContext);

    default void verifyCommandContext() {
        if (getCommandContext() == null) {
            TravianException exception = new TravianException("Command context must be initialized.");
            getCommandContext().setFailedCommand(true, exception);
            throw exception;
        }
    }

    CommandContext<T> getCommandContext();

}
