package by.travian.common.processor;

import by.travian.common.command.Command;
import by.travian.common.context.ProcessorContext;

public interface ExecutorProcessor<T extends Command> {

    void inbound(ProcessorContext<T> context) throws Exception;

    void process(ProcessorContext<T> context) throws Exception;

    void outbound(ProcessorContext<T> context) throws Exception;

    void onFault(ProcessorContext<T> context, Exception ex) throws Exception;

    void setSuccessor(ExecutorProcessor<T> successor);

    ExecutorProcessor<T> getSuccessor();
}
