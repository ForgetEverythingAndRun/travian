package by.travian.common.processor;

/**
 * Required for dynamic properties update
 */
public interface CfgListener {

    void cfgChanged();

    void init();

}
