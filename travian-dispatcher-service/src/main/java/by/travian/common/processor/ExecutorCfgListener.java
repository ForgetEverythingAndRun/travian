package by.travian.common.processor;

public class ExecutorCfgListener implements CfgListener {

    //private ConfigurationService configurationService; //Read all existing properties.
    private boolean isAuthRequired;

    @Override
    public void cfgChanged() {
        init();
    }

    @Override
    public void init() {

    }

    public boolean isAuthRequired() {
        return isAuthRequired;
    }

}
