package by.travian.common.processor;

import by.travian.common.command.Command;

import java.util.List;

public class ProcessorChainFactory {
    /**
     *
     * @param individualProcessors
     * @return
     * @throws InstantiationException
     * @Description:
     * list of processors converted into chain where each element points to the next
     */
    public ExecutorProcessor<Command> createChain(List<ExecutorProcessor<Command>> individualProcessors) throws InstantiationException {
        if (individualProcessors == null  || individualProcessors.isEmpty()) {
            throw new InstantiationException("At least one Processor expected.");
        }

        ExecutorProcessor<Command> result = individualProcessors.get(0);
        ExecutorProcessor<Command> tail = result;
        if (individualProcessors.size() > 1) {
            for (int i = 1; i < individualProcessors.size(); i++) {
                ExecutorProcessor<Command> next = individualProcessors.get(i);
                tail.setSuccessor(next);
                tail = next;
            }
        }

        return result;
    }
}
