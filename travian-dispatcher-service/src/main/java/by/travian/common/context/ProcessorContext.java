package by.travian.common.context;


import by.travian.common.command.Command;
import by.travian.common.executor.CommandPhase;

import java.util.List;

/* Test functionality */
@Deprecated
public class ProcessorContext<T extends Command> {

    //private T command;

    private List<T> commands;

    private CommandPhase currentPhase = CommandPhase.NOT_STARTED;

    private TravianCommandContext travianCommandContext;


    /*public T getCommand() {
        return command;
    }*/

    public List<T> getCommands() {
        return commands;
    }

    /*void setCommand(T command) {
        this.command = command;
    }*/

    void setCommands(List<T> commands) {
        this.commands = commands;
    }

    public CommandPhase currentPhase() {
        return currentPhase;
    }

    void setCurrentPhase(CommandPhase currentPhase) {
        this.currentPhase = currentPhase;
    }

    public TravianCommandContext getTravianCommandContext() {
        return travianCommandContext;
    }

    public void setTravianCommandContext(TravianCommandContext travianCommandContext) {
        this.travianCommandContext = travianCommandContext;
    }
}