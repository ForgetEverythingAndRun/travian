package by.travian.common.context;

import by.travian.bean.entity.Entity;
import by.travian.common.executor.CommandPhase;

public class TravianCommandContext<T extends Entity> implements CommandContext<T> {

    private String villageId;
    private T entity;
    private boolean isFailedCommand;
    private Exception exception;

    public TravianCommandContext(T entity) {
        this.entity = entity;
    }

    @Override
    public T getEntity() {
        return entity;
    }


    @Override
    public boolean isFailedCommand() {
        return isFailedCommand;
    }

    @Override
    public void setFailedCommand(boolean failedCommand) {
        isFailedCommand = failedCommand;
    }

    @Override
    public Exception getException() {
        return exception;
    }

    @Override
    public void setException(Exception exception) {
        this.exception = exception;
    }
}
