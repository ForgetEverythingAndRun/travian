package by.travian.common.context;

import by.travian.bean.entity.Entity;

import java.util.Optional;

public interface CommandContext<T extends Entity> {

    default String getVillageId() {
        return Optional.ofNullable(getEntity()).map(e -> e.getVillageId()).get();
    }

    T getEntity();

    boolean isFailedCommand();

    void setFailedCommand(boolean failedCommand);

    void setException(Exception exception);

    Exception getException();

    default void setFailedCommand(boolean failedCommand, Exception exception) {
        setFailedCommand(failedCommand);
        setException(exception);
    }

}
