package by.travian.common.context;

import by.travian.bean.container.MovementContainer;
import by.travian.bean.container.TravianPlusContainer;
import by.travian.bean.container.VillageContainer;
import by.travian.bean.task.handler.ContextHandler;
import by.travian.configuration.TravianConfiguration;

import java.util.concurrent.atomic.AtomicBoolean;

public interface TravianApplicationContext {
    AtomicBoolean isAuthenticated();

    TravianConfiguration getTravianConfiguration();

    VillageContainer getVillageContainer();

    TravianPlusContainer getTravianPlusContainer();

    MovementContainer getMovementContainer();

    //HeroAdventureContainer getHeroAdventureContainer();

    ContextHandler getContextHandler();

}
