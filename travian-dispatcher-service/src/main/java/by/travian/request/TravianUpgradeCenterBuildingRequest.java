package by.travian.request;

import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.building.BuildingTabs;
import by.travian.constant.TravianConstant;
import org.springframework.web.util.UriComponentsBuilder;

public class TravianUpgradeCenterBuildingRequest<T extends BuildingTabs.BuildingTabValue> extends TravianBuildingRequest<BuildingTabs.BuildingTabValue> {
    private String queryParamC;

    public TravianUpgradeCenterBuildingRequest(BuildingEntity entity, String queryParamC) {
        super(entity, BuildingTabs.createManagementTabByCenterBuildingId(entity.getId()));
        this.queryParamC = queryParamC;
    }

    @Override
    public String uri() {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.VILLAGE_CENTER_PATH)
                .queryParam(TravianConstant.Common.VILLAGE_ID_URL_PARAM, getEntity().getVillageId())
                .queryParam("c", queryParamC);

        if (getEntity().isNewCenterBuilding()) {
            builder.queryParam("a", getEntity().getId());
            builder.queryParam("id", getEntity().getSlotId());
        } else {
            builder.queryParam("a", getEntity().getSlotId());
            if (tab != null) {
                builder.queryParam(tab.getTabQueryParamKey(), tab.getValue());
            }
        }
        return builder.toUriString();
    }
}

