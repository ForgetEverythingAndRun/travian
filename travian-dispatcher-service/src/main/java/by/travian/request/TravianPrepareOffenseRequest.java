package by.travian.request;

import by.travian.bean.entity.OffenseEntity;
import by.travian.bean.entity.building.BuildingTabs;
import by.travian.constant.TravianConstant;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

public class TravianPrepareOffenseRequest extends AbstractTravianRequest<OffenseEntity, MultiValueMap<String, String>> {
    private BuildingTabs.RallyPointTab tab = BuildingTabs.RallyPointTab.SEND;
    protected final static int RALLY_POINT_BUILDING_SLOT_ID = 39;

    public TravianPrepareOffenseRequest(OffenseEntity entity) {
        super(entity);
    }

    @Override
    public String uri() {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.BUILDING_PATH)
                .queryParam(tab.getTabQueryParamKey(), tab.getValue())
                .queryParam(TravianConstant.Common.SLOT_ID_URL_PARAM, RALLY_POINT_BUILDING_SLOT_ID)
                .queryParam(TravianConstant.Common.VILLAGE_ID_URL_PARAM, entity.getVillageId());
        return builder.toUriString();
    }

    @Override
    public HttpMethod method() {
        return HttpMethod.GET;
    }

    @Override
    public MultiValueMap<String, String> transform() {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        return map;
    }

}
