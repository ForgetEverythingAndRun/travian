package by.travian.request;

import by.travian.bean.entity.RecruitmentEntity;
import by.travian.bean.entity.building.BuildingTabs;
import by.travian.constant.TravianConstant;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *     Sample: request params
 *     id: 30
 *     z: 3e9d39
 *     a: 2
 *     s: 1
 *     did: 30783
 *     t4: 1
 *     s1: ok
 */
public class TravianRecruitmentRequest<T extends BuildingTabs.BuildingTabValue> extends AbstractTravianRequest<RecruitmentEntity,MultiValueMap<String, String>> {
    private String c;
    private String a = "2";
    private String s = "1";
    private String s1 = "ok";

    protected T tab;

    public TravianRecruitmentRequest(String c, String a, String s, RecruitmentEntity entity, T tab) {
        super(entity);
        this.c = c;
        this.a = a;
        this.s = s;
        this.tab = tab;
        //A settler unit is required this info: BuildingTabs.SettlerTab.BUY (e.g. &s=1)
        //Other unit's buildings doesn't required tab info
    }

    @Override
    public String uri() {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.BUILDING_PATH)
                .queryParam(TravianConstant.Common.VILLAGE_ID_URL_PARAM, entity.getVillageId())
                .queryParam(TravianConstant.Common.SLOT_ID_URL_PARAM, entity.getSlotId())
                .queryParam(TravianConstant.Common.BUILDING_ID_URL_PARAM, entity.getBuildingId());
        if (tab != null) {
            builder.queryParam(tab.getTabQueryParamKey(), tab.getValue());
        }
        return builder.toUriString();
    }

    @Override
    public HttpMethod method() {
        return HttpMethod.POST;
    }

    @Override
    public MultiValueMap<String, String> transform() {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("c", this.c);//param z was replaced by c
        map.add("a", this.a);
        map.add("s", this.s);
        map.add("did", entity.getVillageId());
        map.add(entity.getAlias().toString(), entity.getBuyCount().toString());
        map.add("s1", this.s1);
        return map;
    }

}
