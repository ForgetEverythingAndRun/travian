package by.travian.request;

import by.travian.bean.entity.HeroAdventureEntity;
import by.travian.constant.TravianConstant;
import org.springframework.web.util.UriComponentsBuilder;

public class TravianHeroAdventureStatusRequest extends TravianDefaultRequest<HeroAdventureEntity> {
    private String QUERY_TAB_NAME_KEY = "t";
    private String QUERY_TAB_NAME_VALUE = "3";


    @Override
    public String uri() {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.HERO_ADVENTURE_STATUS_PATH)
                .queryParam(QUERY_TAB_NAME_KEY, QUERY_TAB_NAME_VALUE);
        return builder.toUriString();
    }
}
