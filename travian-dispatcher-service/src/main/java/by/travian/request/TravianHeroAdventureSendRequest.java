package by.travian.request;

import by.travian.bean.entity.HeroAdventureEntity;
import by.travian.constant.TravianConstant;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

public class TravianHeroAdventureSendRequest extends TravianDefaultRequest<HeroAdventureEntity> {
    private String QUERY_FROM_KEY = "from";
    private String QUERY_FROM_VALUE = "list";
    private String QUERY_KID_KEY = "kid";
    private String ACTION_KEY = "action";
    private String ACTION_VALUE = "sendHeroToAdventure";

    public TravianHeroAdventureSendRequest(HeroAdventureEntity entity) {
        super(entity);
    }

    @Override
    public String uri() {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.HERO_ADVENTURE_SEND_PATH)
                .queryParam(ACTION_KEY, QUERY_FROM_VALUE)
                .queryParam(QUERY_KID_KEY, getEntity().getKid());
        return builder.toUriString();
    }

    @Override
    public MultiValueMap<String, String> transform() {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add(ACTION_KEY, ACTION_VALUE);
        return map;
    }

    @Override
    public HttpMethod method() {
        return HttpMethod.POST;
    }
}
