package by.travian.request;

import by.travian.bean.entity.Entity;
import by.travian.constant.TravianConstant;
import by.travian.service.CookieManager;
import by.travian.util.HttpHelper;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.util.UriComponentsBuilder;


public class TravianPlusRequest extends AbstractTravianRequest<Entity, String> {
    protected String token;

    public TravianPlusRequest(String token) {
        super(null);
        this.token = token;
    }

    @Override
    public HttpHeaders headers(CookieManager cookieManager) {
        String cookies = cookieManager.getCookies(uri());
        HttpHelper.HeaderBuilder headerBuilder = new HttpHelper.HeaderBuilder().cookies(cookies)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        headerBuilder.add("Authorization", "Bearer " + token);
        headerBuilder.add("X-Requested-with", "XMLHttpRequest");
        headerBuilder.add("X-Version", "847.8");
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.TRAVIAN_PLUS_PATH);
        headerBuilder.referer(builder.toUriString());

        return headerBuilder.build();
    }

    @Override
    public String uri() {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.TRAVIAN_PLUS_PATH);
        return builder.toUriString();
    }

    @Override
    public String transform() {
        /*Expected body(json): {"goldProductId":"","goldProductLocation":"","location":"","activeTab":"pros","formData":{}} */
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("goldProductId", "");
            jsonObject.put("goldProductLocation", "");
            jsonObject.put("location", "");
            jsonObject.put("activeTab", "pros");
            jsonObject.put("formData", new JSONObject());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    @Override
    public HttpMethod method() {
        return HttpMethod.POST;
    }

}
