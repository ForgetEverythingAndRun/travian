package by.travian.request;

import by.travian.bean.entity.MarketEntity;
import by.travian.constant.TravianConstant;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

public class TravianMarketRequest extends TravianPrepareMarketRequest {

    public TravianMarketRequest(MarketEntity entity, String token) {
        super(entity, token);
    }

    @Override
    public String uri() {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.MARKET_PATH);
        return builder.toUriString();
    }

    @Override
    public String transform() {
        /*Expected body(json): {"t":"5","id":"35","a":"1232","sz":"1955","kid":"96021","c":"c8f9a6","x2":"1","r1":"1","r2":"2","r3":"3","r4":"4"}*/
        //Send available amount of resources in case of deficit
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("a", entity.getVillageId());
            jsonObject.put("c", entity.getC());
            jsonObject.put("id", Integer.toString(entity.getSlotId()));
            jsonObject.put("kid", entity.getKid());
            jsonObject.put("r1", Integer.toString(entity.getWood()));
            jsonObject.put("r2", Integer.toString(entity.getClay()));
            jsonObject.put("r3", Integer.toString(entity.getIron()));
            jsonObject.put("r4", Integer.toString(entity.getCrop()));
            jsonObject.put("sz", entity.getSz());
            jsonObject.put(tab.getTabQueryParamKey(), tab.getValue()); //tab
            jsonObject.put("x2", Integer.toString(entity.getX2())); //travian plus allow to send resources x2
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public HttpMethod method() {
        return HttpMethod.POST;
    }
}
