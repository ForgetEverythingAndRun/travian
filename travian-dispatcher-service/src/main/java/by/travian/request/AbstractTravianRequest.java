package by.travian.request;

import by.travian.bean.entity.Entity;
import by.travian.service.CookieManager;
import by.travian.util.HttpHelper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

public abstract class AbstractTravianRequest<E extends Entity,R> implements TravianRequest<E,R> {
    protected E entity;

    public AbstractTravianRequest(E entity) {
        this.entity = entity;
    }

    @Override
    public HttpHeaders headers(CookieManager cookieManager) {
        String cookies = cookieManager.getCookies(uri());
        return new HttpHelper.HeaderBuilder().cookies(cookies).build();
    }

    @Override
    public HttpEntity<R> entity(CookieManager cookieManager) {
        return new HttpEntity<>(transform(), headers(cookieManager));
    }
    @Override
    public E getEntity() {
        return this.entity;
    }

    @Override
    public HttpMethod method() {
        return HttpMethod.GET;
    }

}
