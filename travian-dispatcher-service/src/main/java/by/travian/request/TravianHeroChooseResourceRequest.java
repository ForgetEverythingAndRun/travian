package by.travian.request;

import by.travian.bean.entity.HeroResourceProductionEntity;
import by.travian.constant.TravianConstant;
import by.travian.service.CookieManager;
import by.travian.util.HttpHelper;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.util.UriComponentsBuilder;

public class TravianHeroChooseResourceRequest extends AbstractTravianRequest<HeroResourceProductionEntity, String> {
    private String QUERY_RESOURCE_KEY = "resource";
    private String QUERY_ATTACK_BEHAVIOR_KEY = "attackBehaviour";
    private String token;

    public TravianHeroChooseResourceRequest(String token, HeroResourceProductionEntity entity) {
        super(entity);
        this.token = token;
    }

    @Override
    public HttpHeaders headers(CookieManager cookieManager) {
        String cookies = cookieManager.getCookies(uri());
        HttpHelper.HeaderBuilder headerBuilder = new HttpHelper.HeaderBuilder().cookies(cookies)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        headerBuilder.add("Authorization", "Bearer " + token);
        headerBuilder.add("X-Requested-with", "XMLHttpRequest");
        headerBuilder.add("X-Version", "847.8");
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.HERO_RESOURCE_PRODUCTION_PATH);
        headerBuilder.referer(builder.toUriString());

        return headerBuilder.build();
    }


    @Override
    public String uri() {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.HERO_RESOURCE_PRODUCTION_PATH);
        return builder.toUriString();
    }

    @Override
    public String transform() {
        /*Expected body(json): {"resource":"2","attackBehaviour":"hide"} */
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(QUERY_RESOURCE_KEY, getEntity().getResource());
            jsonObject.put(QUERY_ATTACK_BEHAVIOR_KEY, getEntity().getResource());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public HttpMethod method() {
        return HttpMethod.POST;
    }

}
