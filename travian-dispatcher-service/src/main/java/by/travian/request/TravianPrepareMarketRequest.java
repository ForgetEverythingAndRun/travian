package by.travian.request;

import by.travian.bean.entity.MarketEntity;
import by.travian.bean.entity.building.BuildingTabs;
import by.travian.constant.TravianConstant;
import by.travian.service.CookieManager;
import by.travian.util.HttpHelper;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.util.UriComponentsBuilder;

public class TravianPrepareMarketRequest extends AbstractTravianRequest<MarketEntity, String> {
    protected String token;
    protected BuildingTabs.MarketTab tab = BuildingTabs.MarketTab.SEND;

    public TravianPrepareMarketRequest(MarketEntity entity, String token) {
        super(entity);
        this.token = token;
    }

    @Override
    public HttpHeaders headers(CookieManager cookieManager) {
        String cookies = cookieManager.getCookies(uri());
        HttpHelper.HeaderBuilder headerBuilder = new HttpHelper.HeaderBuilder().cookies(cookies)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        headerBuilder.add("Authorization", "Bearer " + token);
        headerBuilder.add("X-Requested-with", "XMLHttpRequest");
        headerBuilder.add("X-Version", "847.8");
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.BUILDING_PATH)
                .queryParam(tab.getTabQueryParamKey(), tab.getValue())
                .queryParam(TravianConstant.Common.VILLAGE_ID_URL_PARAM, getEntity().getVillageId())
                .queryParam(TravianConstant.Common.SLOT_ID_URL_PARAM, getEntity().getSlotId());
        headerBuilder.referer(builder.toUriString());

        return headerBuilder.build();
    }

    @Override
    public String uri() {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.MARKET_PATH);
        return builder.toUriString();
    }

    @Override
    public String transform() {
        /*Expected body(json): {"r1":"1","r2":"2","r3":"3","r4":"4","dname":"","x":"-19","y":"-39","id":"35","t":"5","x2":1} */
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("r1", Integer.toString(entity.getWood()));
            jsonObject.put("r2", Integer.toString(entity.getClay()));
            jsonObject.put("r3", Integer.toString(entity.getIron()));
            jsonObject.put("r4", Integer.toString(entity.getCrop()));
            jsonObject.put("dname", entity.getDname());
            jsonObject.put("x", Integer.toString(entity.getTarget().getX()));
            jsonObject.put("y", Integer.toString(entity.getTarget().getY()));
            jsonObject.put("id", Integer.toString(entity.getSlotId()));
            jsonObject.put(tab.getTabQueryParamKey(), tab.getValue()); //tab
            jsonObject.put("x2", Integer.toString(entity.getX2())); //travian plus allow to send resources x2
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    @Override
    public HttpMethod method() {
        return HttpMethod.POST;
    }


}
