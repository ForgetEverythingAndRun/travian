package by.travian.request;

import by.travian.bean.entity.OffenseEntity;
import by.travian.bean.entity.building.BuildingTabs;
import by.travian.bean.troop.TroopAlias;
import by.travian.constant.TravianConstant;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

public class TravianChooseOffenseRequest extends AbstractTravianRequest<OffenseEntity, MultiValueMap<String, String>> {
    private BuildingTabs.RallyPointTab tab = BuildingTabs.RallyPointTab.SEND;
    protected final static int RALLY_POINT_BUILDING_SLOT_ID = 39;

    public TravianChooseOffenseRequest(OffenseEntity entity) {
        super(entity);
    }

    @Override
    public String uri() {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.BUILDING_PATH)
                .queryParam(tab.getTabQueryParamKey(), tab.getValue())
                .queryParam(TravianConstant.Common.SLOT_ID_URL_PARAM, RALLY_POINT_BUILDING_SLOT_ID)
                .queryParam(TravianConstant.Common.VILLAGE_ID_URL_PARAM, entity.getVillageId());
        return builder.toUriString();
    }

    @Override
    public HttpMethod method() {
        return HttpMethod.POST;
    }

    @Override
    public MultiValueMap<String, String> transform() {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add(TravianConstant.OffenseSend.TIMESTAMP_ATTACK_KEY, entity.getTimestamp());
        map.add(TravianConstant.OffenseSend.TIMESTAMP_CHECKSUM_ATTACK_KEY, entity.getChecksum());
        map.add(TravianConstant.OffenseSend.B_KEY, TravianConstant.OffenseSend.B_VALUE);
        map.add(TravianConstant.OffenseSend.CURRENT_DID, entity.getVillageId());

        for (TroopAlias.RequestTroopAlias alias : TroopAlias.RequestTroopAlias.values()) {
            String addWrapper = "troops[0][" + alias.toString() + "]";
            map.add(addWrapper, Integer.toString(entity.getTroopsContainer().countByAlias(alias)));
        }
        map.add(TravianConstant.OffenseSend.DNAME_KEY, TravianConstant.OffenseSend.EMPTY);
        map.add(TravianConstant.OffenseSend.X_KEY, Integer.toString(entity.getTarget().getX()));
        map.add(TravianConstant.OffenseSend.Y_KEY, Integer.toString(entity.getTarget().getY()));
        map.add(TravianConstant.OffenseSend.C_KEY, entity.getOffenseType().toString());
        map.add(TravianConstant.OffenseSend.S1_KEY, TravianConstant.OffenseSend.S1_VALUE);
        return map;
    }

}
