package by.travian.request;

import by.travian.bean.entity.Entity;
import by.travian.service.CookieManager;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.util.MultiValueMap;

public interface TravianRequest<E extends Entity, T> {
    String uri();

    HttpMethod method();

    T transform();

    HttpHeaders headers(CookieManager cookieManager);

    HttpEntity<T> entity(CookieManager cookieManager);

    E getEntity();
}
