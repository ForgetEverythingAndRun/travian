package by.travian.request;

import by.travian.bean.entity.LoginEntity;
import by.travian.constant.TravianConstant;
import by.travian.service.CookieManager;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

public class TravianLoginRequest extends AbstractTravianRequest<LoginEntity, MultiValueMap<String, String>> {
    public TravianLoginRequest(LoginEntity entity) {
        super(entity);
    }

    @Override
    public String uri() {
        String URI = TravianConstant.Login.LOGIN_PATH;
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Login.LOGIN_PATH);
        return builder.toUriString();
    }

    @Override
    public HttpMethod method() {
        return HttpMethod.POST;
    }

    @Override
    public MultiValueMap<String, String> transform() {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add(TravianConstant.Login.NAME_KEY, String.valueOf(entity.getLogin()));
        map.add(TravianConstant.Login.PASSWORD_KEY, String.valueOf(entity.getPassword()));
        map.add(TravianConstant.Login.ENTER_BUTTON_KEY, String.valueOf(TravianConstant.Login.ENTER_BUTTON_VALUE));
        map.add(TravianConstant.Login.SCREEN_WIDTH_KEY, String.valueOf(TravianConstant.Login.SCREEN_WIDTH_VALUE));
        map.add(TravianConstant.Login.TIMESTAMP_LOGIN_KEY, String.valueOf(TravianConstant.Login.TIMESTAMP_LOGIN_VALUE));
        map.add(TravianConstant.Login.LOW_RES_KEY, TravianConstant.Login.LOW_RES_VALUE);
        return map;
    }


}
