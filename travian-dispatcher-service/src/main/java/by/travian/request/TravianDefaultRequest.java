package by.travian.request;

import by.travian.bean.entity.Entity;
import by.travian.constant.TravianConstant;
import by.travian.service.CookieManager;
import org.springframework.http.HttpEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

public class TravianDefaultRequest<E extends Entity> extends AbstractTravianRequest<E, MultiValueMap<String, String>> {

    public TravianDefaultRequest() {
        super(null);
    }

    protected TravianDefaultRequest(E entity) {
        super(entity);
    }

    @Override
    public String uri() {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.VILLAGE_MAIN_PAGE_PATH);
        return builder.toUriString();
    }


    @Override
    public HttpEntity<MultiValueMap<String, String>> entity(CookieManager cookieManager) {
        return new HttpEntity<>(transform(), headers(cookieManager));
    }

    @Override
    public MultiValueMap<String, String> transform() {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        return map;
    }

}