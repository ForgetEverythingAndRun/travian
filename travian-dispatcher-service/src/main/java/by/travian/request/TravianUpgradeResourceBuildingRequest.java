package by.travian.request;

import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.building.BuildingTabs;
import by.travian.constant.TravianConstant;
import org.springframework.web.util.UriComponentsBuilder;

public class TravianUpgradeResourceBuildingRequest<T extends BuildingTabs.BuildingTabValue> extends TravianBuildingRequest<BuildingTabs.BuildingTabValue> {
    private String queryParamC;

    public TravianUpgradeResourceBuildingRequest(BuildingEntity entity, String queryParamC) {
        super(entity, null);
        this.queryParamC = queryParamC;
    }

    @Override
    public String uri() {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.VILLAGE_MAIN_PAGE_PATH)
                .queryParam(TravianConstant.Common.VILLAGE_ID_URL_PARAM, getEntity().getVillageId())
                .queryParam("a", getEntity().getSlotId())
                .queryParam("c", queryParamC);
        if (tab != null) {
            builder.queryParam(tab.getTabQueryParamKey(), tab.getValue());
        }
        return builder.toUriString();
    }

}
