package by.travian.request;

import by.travian.bean.entity.BuildingEntity;
import by.travian.bean.entity.building.BuildingTabs;
import by.travian.constant.TravianConstant;
import by.travian.service.CookieManager;
import org.springframework.http.HttpEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

public class TravianBuildingRequest<T extends BuildingTabs.BuildingTabValue> extends AbstractTravianRequest<BuildingEntity, MultiValueMap<String, String>> {
    protected T tab;

    public TravianBuildingRequest(BuildingEntity entity, T tab) {
        super(entity);
        this.tab = tab;
    }


    @Override
    public HttpEntity<MultiValueMap<String, String>> entity(CookieManager cookieManager) {
        return new HttpEntity<>(transform(), headers(cookieManager));
    }

    @Override
    public MultiValueMap<String, String> transform() {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        return map;
    }

    protected UriComponentsBuilder createBuilder() {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.BUILDING_PATH)
                .queryParam(TravianConstant.Common.VILLAGE_ID_URL_PARAM, getEntity().getVillageId())
                .queryParam(TravianConstant.Common.SLOT_ID_URL_PARAM, getEntity().getSlotId());
        if (!getEntity().isNewCenterBuilding()) {
            builder.queryParam(TravianConstant.Common.BUILDING_ID_URL_PARAM, getEntity().getId());
            if (tab != null) {
                builder.queryParam(tab.getTabQueryParamKey(), tab.getValue());
            }
        }
        return builder;
    }

    @Override
    public String uri() {
        UriComponentsBuilder builder = createBuilder();
        if (tab != null) {
            builder.queryParam(tab.getTabQueryParamKey(), tab.getValue());
        }
        return builder.toUriString();
    }
}