package by.travian.request;

import by.travian.bean.entity.village.Village;
import by.travian.constant.TravianConstant;
import org.springframework.web.util.UriComponentsBuilder;

public class TravianVillageRequest extends TravianDefaultRequest<Village> {

    public TravianVillageRequest(Village village) {
        super(village);
    }

    @Override
    public String uri() {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.VILLAGE_MAIN_PAGE_PATH)
                .queryParam(TravianConstant.Common.VILLAGE_ID_URL_PARAM, getEntity().getVillageId());
        return builder.toUriString();
    }
}