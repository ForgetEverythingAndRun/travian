package by.travian.request;

import by.travian.bean.entity.village.Village;
import by.travian.constant.TravianConstant;
import org.springframework.web.util.UriComponentsBuilder;

public class TravianVillageCenterRequest extends TravianDefaultRequest<Village> {

    public TravianVillageCenterRequest(Village village) {
        super(village);
    }

    @Override
    public String uri() {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(TravianConstant.Common.HTTPS_SCHEME)
                .host(TravianConstant.Common.HOST)
                .path(TravianConstant.Common.VILLAGE_CENTER_PATH)
                .queryParam(TravianConstant.Common.VILLAGE_ID_URL_PARAM, getEntity().getVillageId());
        return builder.toUriString();
    }
}